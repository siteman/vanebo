<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
WPConfig::define( 'DB_NAME', '-' );

/** MySQL database username */
WPConfig::define( 'DB_USER', '-' );

/** MySQL database password */
WPConfig::define( 'DB_PASSWORD', '-' );

/** MySQL hostname */
WPConfig::define( 'DB_HOST', '-' );

/** Database Charset to use in creating database tables. */
WPConfig::define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
WPConfig::define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
WPConfig::define( 'AUTH_KEY',         '-' );
WPConfig::define( 'SECURE_AUTH_KEY',  '-' );
WPConfig::define( 'LOGGED_IN_KEY',    '-' );
WPConfig::define( 'NONCE_KEY',        '-' );
WPConfig::define( 'AUTH_SALT',        '-' );
WPConfig::define( 'SECURE_AUTH_SALT', '-' );
WPConfig::define( 'LOGGED_IN_SALT',   '-' );
WPConfig::define( 'NONCE_SALT',       '-' );