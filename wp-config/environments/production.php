<?php

WPConfig::define('WP_CACHE', true);

WPConfig::define('WP_DEBUG', false);

ini_set('display_errors', '0');

WPConfig::define('WPCF7_AUTOP', false);

WPConfig::define('DISALLOW_FILE_EDIT', false);

WPConfig::define('WP_AUTO_UPDATE_CORE', false);

WPConfig::define('DISALLOW_FILE_MODS', true);

WPConfig::define('DISABLE_WP_CRON', false);