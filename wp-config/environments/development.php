<?php

WPConfig::define('WP_DEBUG', true);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

WPConfig::define('WPCF7_AUTOP', false);

WPConfig::define('QM_DISABLE_ERROR_HANDLER', false);

WPConfig::define('WP_DISABLE_FATAL_ERROR_HANDLER', true);