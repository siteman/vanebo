<?php

class WPConfig {
    /**
     * Environment
     *
     * @var string
     */
    private $env;

    /**
     * List of constants to define
     *
     * @var array
     */
    private static $defines = [];

    /**
     * Path for config files
     *
     * @var string
     */
    private $directoryPath = null;

    /**
     * Set directory path with config files
     *
     * @param string $directoryPath
     * @return \WPConfig
     */
    public function setDirectory(string $directoryPath): self
    {
        $this->directoryPath = $directoryPath;

        return $this;
    }

    /**
     * Set environment
     *
     * @param string $env
     * @return \WPConfig
     */
    public function setEnv(string $env): self
    {
        $this->env = $env;

        return $this;
    }

    /**
     * Set defines
     *
     * @param string $key
     * @param $value
     */
    public static function define(string $key, $value): void
    {
        self::$defines[$key] = $value;
    }

    /**
     * Load config files
     */
    private function loadFiles(): void
    {
        require $this->directoryPath.'env.php';
        require $this->directoryPath . $this->env .'.php';
    }

    /**
     * Set constants
     */
    public function load()
    {
        $this->loadFiles();

        foreach(self::$defines as $key => $value) {
            define($key, $value);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getTablePrefix(): string
    {
        return self::$defines['TABLE_PREFIX'] ?? 'wp_';
    }
}

$appConfig = (new WPConfig())
    ->setEnv(WP_ENV)
    ->setDirectory(__DIR__ . DIRECTORY_SEPARATOR . 'environments' . DIRECTORY_SEPARATOR)
    ->load();

$table_prefix = $appConfig->getTablePrefix();