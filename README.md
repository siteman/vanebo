# GS - Theme

## Requirements:
- php 7.4
- composer 2
- node 16 
- WP CLI

## Installation:
1. Download repository:
```
git clone git@bitbucket.org:siteman/godt-sagt-theme-page.git
```

2. Configure your wordpress config wp-config/environments/env.php
3. Install **ACF Pro**, (if is not installed) this plugin is required to start working with the theme.
4. Enable Theme called "Gs Starter Theme"
5. Change names for theme
- Change name for theme directory
- Edit ``{theme directory}/resources/style.css``
- Edit ``{theme directory}/config/application.php``

## Blocks:
### Creating blocks

1. Create a new class in directory ``{theme directory}/src/Blocks``
2. Create a new template for the block ``{theme directory}/resources/block-text.php``
3. Register block in: ``{theme directory}/config/content.php``
4. Create new fields for block in ACF Pro, remember to add a prefix before each field, prefix has to be the same as slug.

## Post type:
### Creating new post type

1. Create a new class in ``{theme_directory}/src/Models/Posts``
2. Register post type in ``{theme directory}/config/content.php``

## Thumbnails:
All thumbnails for the theme should be registered in ``{theme directory}/config/thumbnails.php``

## Composers:
Providing variables to templates in easy way.

## Providers
The place where are register actions/filters for wordpress hooks.