import axios from 'axios';

export default class FragmentCache {
  public static async getFragment(item: string): Promise<string> {
    return await axios.get('', {
      params: {
        items: [item],
        action: 'get_fragment_cache'
      }
    }).then(r => r.data.fragments[item]);
  }
}