export default class Loader {
  private readonly el: HTMLElement;
  private loaderEl: HTMLElement;
  private isVisible = false;

  constructor(elem: HTMLElement, instantly = false) {
    this.el = elem;

    this.create();

    if (instantly) {
      this.show();
    }
  }

  private create() {
    this.loaderEl = this.el.querySelector('.loader');
    if (this.loaderEl) {
      return;
    }

    this.loaderEl = document.createElement('DIV');
    this.loaderEl.classList.add('loader');
    this.loaderEl.innerHTML = '<div></div><div></div><div></div>';
    this.el.append(this.loaderEl);
  }

  public getIsVisible(): boolean {
    return this.isVisible;
  }

  public show(): void {
    if (this.isVisible) {
      return;
    }

    this.el.classList.add('is-loading');
    this.loaderEl.classList.add('loader--visible');
    this.isVisible = true;
  }

  public hide(): void {
    if (!this.isVisible) {
      return;
    }

    this.el.classList.remove('is-loading');
    this.loaderEl.classList.remove('loader--visible');
    this.isVisible = false;
  }
}