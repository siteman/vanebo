import App from "../app";

declare let grecaptcha: any;

export default class Recaptcha {
  public static script: HTMLScriptElement = null;

  public static async getToken(): Promise<string> {
    return new Promise<string>((resolve) => {
      const getToken = () => {
        grecaptcha.ready(() => {
          grecaptcha.execute(App.getData('recaptcha_key')).then((token: string) => {
            resolve(token);
          });
        });
      };

      if (typeof grecaptcha === 'undefined' && Recaptcha.script === null) {
        Recaptcha.script = document.createElement('script');
        Recaptcha.script.src = 'https://www.google.com/recaptcha/api.js?render=' + App.getData('recaptcha_key');
        Recaptcha.script.onload = () => {
          getToken();
        };
        document.body.append(Recaptcha.script);
      } else {
        getToken();
      }
    });
  }
}