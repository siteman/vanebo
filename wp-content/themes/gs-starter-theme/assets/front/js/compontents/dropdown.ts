import {TimelineLite, Power4} from 'gsap';
import * as delegate from 'delegate';

export default class Dropdown {
    public readonly elem: HTMLElement;
    public readonly elemLink: HTMLElement;
    public maxHeight = 'auto';
    private tl: TimelineLite;

    constructor(target: HTMLElement, elemLink: HTMLElement = null) {
        this.elem = target;
        this.elemLink = elemLink;
        this.tl = new TimelineLite();
    }

    public getElemLink(): HTMLElement {
        return this.elemLink || document.getElementById(this.elem.getAttribute('aria-labelledby'));
    }

    static factory() {
        delegate('[data-toggle]', 'click', (e) => {
            const target = e.delegateTarget as HTMLElement;
            const dropdown = new Dropdown(document.querySelector(target.dataset.toggle));
            dropdown.toggle();
        });

        document.addEventListener('click', (event: MouseEvent) => {
            const target = event.target as Element;
            const dropdownCurrentElem = target.closest('.dropdown');

            // Close all dropdowns
            document.querySelectorAll('.dropdown.is-expanded').forEach((dropdownElem: Element) => {
                if (dropdownElem !== dropdownCurrentElem) {
                    const dropdown = new Dropdown(dropdownElem as HTMLElement);
                    dropdown.hide();
                }
            });
        });
    }

    show(): void {
        this.tl.set(this.elem, {
            visibility: 'visible',
            height: '0px',
            display: 'block',
          }).to(this.elem, 0.3, {
            height: 'auto',
            maxHeight: this.maxHeight,
            ease: Power4.easeInOut,
            onComplete: () => {
                this.elem.classList.add('is-expanded');
                this.elem.setAttribute('aria-expanded', 'true');

                this.getElemLink().classList.add('is-expanded');
            }
        });
    }

    hide(): void {

        this.tl.to(this.elem, 0.3, {
            height: '0px',
            ease: Power4.easeInOut,
        }).set(this.elem, {
            visibility: 'hidden',
            display: 'none',
            onComplete: () => {
                this.elem.classList.remove('is-expanded');
                this.elem.setAttribute('aria-expanded', 'false');
                this.getElemLink().classList.remove('is-expanded');

                this.elem.dispatchEvent(new CustomEvent('dropdown-hidden', { bubbles: true }));
            }
        });
    }

    toggle(): void {
        this.isExpanded() ? this.hide() : this.show();
    }

    public isExpanded(): boolean {
        return this.elem.classList.contains('is-expanded');
    }
}