export default class ViewPort {
  static breakpoints: Record<string, number>;

  static setBreakpoints(breakpoints: Record<string, number>): void {
    ViewPort.breakpoints = breakpoints;
  }

  static getBreakpoint(breakpoint: string): number {
    return ViewPort.breakpoints[breakpoint];
  }

  static isUp(breakpoint?: string): boolean {
    return window.matchMedia('(min-width: ' + ViewPort.breakpoints[breakpoint] + 'px)').matches;
  }

  static isDown(breakpoint?: string): boolean {
    return window.matchMedia('(max-width: ' + ViewPort.breakpoints[breakpoint] + 'px)').matches;
  }
}