import { first, get, has } from 'lodash';

import Vue from "vue";

export default Vue.extend({
  data: function() {
    return {
      success: false,
      errors: []
    }
  },
  methods: {
    error(id): string {
      const errors = get(this.errors, id);

      if (errors) {
        return first(errors);
      } else {
        return '';
      }
    },
    hasError(id): boolean {
      return has(this.errors, id);
    }
  }
});