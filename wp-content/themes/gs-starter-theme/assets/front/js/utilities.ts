export default class Utilities {
  static debounce = (callback, time: number) => {
    let interval;
    return (...args) => {
      clearTimeout(interval);
      interval = setTimeout(() => {
        interval = null;
        callback(...args);
      }, time);
    };
  };

  static objectToFormData = function(obj, form, namespace) {

    const fd = form || new FormData();
    let formKey;

    for(const property in obj) {
      if(obj.hasOwnProperty(property)) {
        if(namespace) {
          formKey = namespace + '[' + property + ']';
        } else {
          formKey = property;
        }
        if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
          Utilities.objectToFormData(obj[property], fd, property);
        } else {
          fd.append(formKey, obj[property]);
        }
      }
    }

    return fd;
  };

  static getCookie = (cname: string) => {
    const name = cname + "=";
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }

  static setCookie(cname: string, value: string, expires: Date) {
    document.cookie = `${cname}=${value}; expires=${expires.toUTCString()}; path=/;`;
  }
}