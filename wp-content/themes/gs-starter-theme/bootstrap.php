<?php

/**
 * Load composer vendors
 */
if (!file_exists($composer = __DIR__.'/vendor/autoload.php')) {
    die('You have to run in the command line "composer install"');
} else {
    require_once $composer;
    require_once __DIR__ . '/vendor/autoload.php';
}

if (class_exists('\PP\Foundation\Application')) {
    /**
     * Create application instance
     */
    \PP\Foundation\Application::getInstance()
        ->registerTheme(\PPTheme\Theme::class, __DIR__);
} elseif (false == is_admin()) {
    die('This theme require "PP Core" plugin!');
}