<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;

class BlockTextWrapper extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_text_wrapper';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'text_wrapper',
            'title' => __('Text Wrapper', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-text-wrapper.php',
        ];
    }

    public function with(): array
    {
        return [
            'content' => $this->get('content'),
            'background_color' => $this->get('background_color'),
            'text_color' => $this->get('text_color'),
        ];
    }
}