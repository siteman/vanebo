<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockMap extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_map';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'map',
            'title' => __('Map', _THEME_DOMAIN_),
            'supports' => [
                    'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-map.php',
        ];
    }

    public function with(): array
    {
        $data = [
            'title' => $this->get('title'),
        ];
        return $data;
    }
}