<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockPartnerLogos extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_partner_logos';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'text',
            'title' => __('Partner logos', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-partner-logos.php',
        ];
    }

    public function with(): array
    {
        $data = [
            'type' => $this->get('type'),
        ];

        if (!empty($this->get('single_image'))) {
            $data['single_image'] = new Image($this->get('single_image'), 'img-1400x150');
        }

        if (!empty($this->get('partner_logos'))) {
            $partners = [];
            foreach ($this->get('partner_logos') as $key => $partner) {
                $partners[$key]['logo'] = new Image($partner['block_partner_logos_logo'], 'img-150x70');
                $partners[$key]['link'] = $partner['block_partner_logos_link'];
            }
            $data['carousel_logos'] = $partners;

        }
        return $data;
    }
}