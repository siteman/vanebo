<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockTimeline extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_timeline';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'timeline',
            'title' => __('Timeline', _THEME_DOMAIN_),
            'supports' => [
                    'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-timeline.php',
        ];
    }

    public function with(): array
    {
        $data  = [
                'title' => $this->get('title'),
                'content' => $this->get('content'),
        ];
        return $data;
    }
}