<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockShowHeader extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_show_header';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'text',
            'title' => __('Show Header', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-show-header.php',
        ];
    }

    public function with(): array
    {
        $data = [
            'text' => $this->get('text'),
            'type_h' => $this->get('type'),
            'font' => $this->get('font'),
        ];
        if(!empty($data['type_h'])){
            $data['type_h'] = 'h2';
        }
        return $data;
    }
}