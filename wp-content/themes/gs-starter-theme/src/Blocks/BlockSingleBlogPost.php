<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockSingleBlogPost extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_single_blog_post';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'text',
            'title' => __('Single Blog Post', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-single-blog-post.php',
        ];
    }

    public function with(): array
    {

        $data  = [
            'facebook' => $this->get('facebook'),
            'twitter' => $this->get('twitter'),
            'google_plus' => $this->get('google_plus'),
            'github' => $this->get('github'),
            'pinterest' => $this->get('pinterest'),
            'linkedin' => $this->get('linkedin'),
            'instagram' => $this->get('instagram'),
            'youtube' => $this->get('youtube'),
        ];

        // the query
        $query_args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'orderby' => 'publish_date',
                'order' => 'DESC',
                'posts_per_page'=> 1,
        );

        $latest_post = new \WP_Query( $query_args );

        $data['latest_post'] = $latest_post;
        return $data;
    }
}