<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockSlider extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_slider';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'slider',
            'title' => __('Slider', _THEME_DOMAIN_),
            'supports' => [
                    'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-slider.php',
        ];
    }

    public function with(): array
    {
        return [

        ];
    }
}