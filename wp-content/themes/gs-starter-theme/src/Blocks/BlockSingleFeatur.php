<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockSingleFeatur extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_single_feature';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'text',
            'title' => __('Single feature', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-single-feature.php',
        ];
    }

    public function with(): array
    {
        $data  = [
            'image_alignment' => $this->get('image_alignment'),
            'header_text' => $this->get('header'),
            'header_text2' => $this->get('header_2'),
            'content_text' => $this->get('content'),
            'content_text2' => $this->get('content_2'),
            'show_button' => $this->get('show_button'),
            'show_informations' => $this->get('show_informations'),
            'button' => $this->get('button'),
            'layout_background' => $this->get('layout_background'),
        ];
        if(!empty($this->get('image'))){
            $data['image'] = new Image($this->get('image'), 'img-450x300');
        }
        return $data;
    }
}