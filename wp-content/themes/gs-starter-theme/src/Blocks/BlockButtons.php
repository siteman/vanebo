<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockButtons extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_buttons';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'buttons',
            'title' => __('Buttons', _THEME_DOMAIN_),
            'supports' => [
                    'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-buttons.php',
        ];
    }

    public function with(): array
    {

        return[

        ];
    }
}