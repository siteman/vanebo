<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockCallToAction extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_call_to_action';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'text',
            'title' => __('Call to action', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-call-to-action.php',
        ];
    }

    public function with(): array
    {
        $data = [
            'title' => $this->get('title'),
            'columns' => $this->get('columns'),
            'column_1' => $this->get('column_1'),
            'column_2' => $this->get('column_2'),
            'show_button' => $this->get('show_button'),
            'button_link' => $this->get('button_link'),
            'background_type' => $this->get('background_type'),
        ];

        return $data;
    }
}