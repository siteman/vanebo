<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockShowPosts extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_show_posts';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'text',
            'title' => __('Show Posts', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-show-posts.php',
        ];
    }

    public function with(): array
    {
        $data = [
            'header' => $this->get('header'),
            'number_of_posts' => $this->get('number_of_posts'),
        ];

        // the query
        $query_args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'orderby' => 'publish_date',
                'order' => 'DESC',
                'posts_per_page'=> intval($this->get('number_of_posts')) -1,
        );

        $latest_posts = new \WP_Query( $query_args );

        $data['latest_posts'] = $latest_posts;
        return $data;
    }
}