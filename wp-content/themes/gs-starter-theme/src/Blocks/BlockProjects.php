<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;

class BlockProjects extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_projects';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'projects',
            'title' => __('Projects', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-projects.php',
        ];
    }

    public function with(): array
    {

		$data  = [
			'title' => $this->get('title'),
            'background_color' => $this->get('background_color')
		];
        
		return $data;
    }
}