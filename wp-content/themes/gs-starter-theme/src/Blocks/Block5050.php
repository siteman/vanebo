<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;

class Block5050 extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_50_50';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'text',
            'title' => __('Block 50/50', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-50-50.php',
        ];
    }

    public function with(): array
    {
		$data  = [
			'image_align' => $this->get('image_align'),
			'title' => $this->get('title'),
			'content' => $this->get('content'),
			'image' => $this->get('image'),
			'button' => $this->get('button'),
			'parameters' => $this->get('parameters'),
            'background_color' => $this->get('background_color'),
            'button_layout' => $this->get('button_layout'),
		];
        return $data;
    }
}