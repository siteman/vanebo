<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;

class BlockReferencesCircles extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_references_circles';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'references_circles',
            'title' => __('References Classes', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-references-circles.php',
        ];
    }

    public function with(): array
    {
		$data  = [
			'title' => $this->get('title'),
		];
		return $data;
    }
}