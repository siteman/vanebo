<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockShowEmployees extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_show_employees';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'text',
            'title' => __('Show Employees', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-show-employees.php',
        ];
    }

    public function with(): array
    {
        $data = [
            'header' => $this->get('header'),
            'number_of_employees' => $this->get('number_of_employees'),
        ];

        // the query
        $query_args = array(
                'post_type' => 'employees',
                'post_status' => 'publish',
                'orderby' => 'publish_date',
                'order' => 'DESC',
                'posts_per_page'=> intval($this->get('number_of_employees')) -1,
        );

        $latest_employees = new \WP_Query( $query_args );

        $data['latest_employees'] = $latest_employees;
        return $data;
    }
}