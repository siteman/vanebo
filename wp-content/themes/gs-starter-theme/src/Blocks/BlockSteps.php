<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockSteps extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_steps';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'steps',
            'title' => __('Steps', _THEME_DOMAIN_),
            'supports' => [
                    'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-steps.php',
        ];
    }

    public function with(): array
    {
        $data  = [
                'show_arrows' => $this->get('show_arrows'),
                'show_step_numbers' => $this->get('show_step_numbers'),
                'arrow_icon' => $this->get('arrow_icon'),
                'title_checkbox' => $this->get('title_checkbox'),
                'title' => $this->get('title'),
                'button_checkbox' => $this->get('button_checkbox'),
                'button' => $this->get('button'),
        ];
        return $data;
    }
}