<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;

class BlockText extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_text';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'text',
            'title' => __('Text', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-text.php',
        ];
    }

    public function with(): array
    {

		$data  = [
			'title' => $this->get('title'),
			'content' => $this->get('content'),
			'buttons' => $this->get('buttons'),
			'button_align' => $this->get('button_align'),
			'button_layout' => $this->get('button_layout'),
			'layout' => $this->get('layout'),
			'background_image' => $this->get('background_image'),
			'content_width' => $this->get('content_width'),
			'content_align' => $this->get('content_align'),
			'h1_title' => $this->get('h1_title'),
			'show_circle' => $this->get('show_circle'),
            'post_id' => $this->getPostId()
		];
        return $data;
    }
}