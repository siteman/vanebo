<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;

class BlockAccordion extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_accordion';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'accordion',
            'title' => __('Accordion', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-accordion.php',
        ];
    }

    public function with(): array
    {
        return [
            
        ];
    }
}