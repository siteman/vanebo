<?php

namespace PPTheme\Blocks;

use Illuminate\Http\Response;
use PP\AbstractBlock;
use PP\Image;

class BlockNews extends AbstractBlock {
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string {
        return 'block_news';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array {
        return [
            'name' => 'news',
            'title' => __('News', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-news.php',
        ];
    }

    public function with(): array {
        $data  = [
            'type' => $this->get('type'),
            'title' => $this->get('title'),
            'number_of_newest_posts' => $this->get('number_of_newest_posts'),
            'number_of_skipped_news' => $this->get('number_of_skipped_news'),
            'posts_number' => $this->get('posts_number'),
        ];
        return $data;
    }

    public function LoadMore() {
        $elements = $_GET['elements'];
        $page = $_GET['page'];
        $skiped = $_GET['skiped'];
        $offset = ($elements * $page) + $skiped;
        $args = [
            'numberposts'   => $elements,
            'offset'        => $offset,
            'orderby'       => 'post_date',
            'order'         => 'DESC',
        ];
        $posts = wp_get_recent_posts($args);

        $html = '';
        foreach ($posts as $post) {
            $html .=
                '<article class="block-all-news-container__article">' .
                '<p>' .
                '<time datetime=' . get_the_date('d F y', $post['ID']) . '>' . get_the_date('d F y', $post['ID']) . '</time>' .
                '</p>' .
                '<a href=' . get_post_permalink($post['ID']) . '> ' .
                '<h3>' . $post['post_title'] . '</h3>' .
                '</a>' .
                '</article>';
        }



        return Response::create([
            'success' => true,
            'posts' => $posts,
            'html' => $html,
        ]);
    }
}
