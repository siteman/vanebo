<?php

namespace PPTheme\Blocks;

use PP\AbstractBlock;
use PP\Image;

class BlockFeatures extends AbstractBlock
{
    /**
     * Return the block slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return 'block_features';
    }

    /**
     * Return an args for registering block
     *
     * @return array
     */
    public function getArgs(): array
    {
        return [
            'name' => 'text',
            'title' => __('Features', _THEME_DOMAIN_),
            'supports' => [
                'align' => ['full']
            ],
            'align' => 'full',
            'render_template' => 'blocks/block-features.php',
        ];
    }

    public function with(): array
    {
        $data  = [
            'main_header' => $this->get('main_header'),
            'columns' => $this->get('columns'),
            'background_color' => $this->get('background_color')

        ];

        if(!empty($data['columns'])){
            foreach ($data['columns'] as $col_key => $column){
                if(!empty($column['image'])){
                    $data['columns'][$col_key]['pp_image'] = new Image($column['image'], 'full');
                }
            }
        }
        return $data;
    }
}