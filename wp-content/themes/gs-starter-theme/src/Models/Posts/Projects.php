<?php

namespace PPTheme\Models\Posts;

use Illuminate\Support\Arr;
use PP\Image;
use PP\Models\AbstractPostType;
use PP\Picture;

class Projects extends AbstractPostType
{
    /**
     * Return slug for post type
     *
     * @return string
     */
    public static function getPostType(): string
    {
        return 'projects';
    }

    /**
     * Have to return post type args
     *
     * List of allowed arguments: https://codex.wordpress.org/Function_Reference/register_post_type
     * List dashboard icons: https://developer.wordpress.org/resource/dashicons
     *
     * @return array
     */
    public static function getArgs(): array
    {
        return [
            'labels'              => [
                'name'               => _x('Projects', 'post type general name', _THEME_DOMAIN_),
                'singular_name'      => _x('Projects', 'post type singular name', _THEME_DOMAIN_),
                'menu_name'          => _x('Projects', 'admin menu', _THEME_DOMAIN_),
                'name_admin_bar'     => _x('Projects', 'add new on admin bar', _THEME_DOMAIN_),
                'add_new'            => _x('Add new', 'add new', _THEME_DOMAIN_),
                'add_new_item'       => __('Add new project', _THEME_DOMAIN_),
                'new_item'           => __('New element', _THEME_DOMAIN_),
                'edit_item'          => __('Edit project', _THEME_DOMAIN_),
                'view_item'          => __('View project', _THEME_DOMAIN_),
                'all_items'          => __('All projects', _THEME_DOMAIN_),
                'search_items'       => __('Search project', _THEME_DOMAIN_),
                'parent_item_colon'  => __('Project parent', _THEME_DOMAIN_),
                'not_found'          => __('Project not found.', _THEME_DOMAIN_),
                'not_found_in_trash' => __('Project not found in trash.', _THEME_DOMAIN_)
            ],
            'public'              => true,
            'publicly_queryable'  => true,
            'exclude_from_search' => true,
            'show_ui'             => true,
            'show_in_nav_menus'   => true,
			'show_in_rest' 		  => true,
			'map_meta_cap'		  => true,
            'query_var'           => false,
            'capability_type'     => 'post',
            'has_archive'         => true,
            'hierarchical'        => false,
            'rewrite'             => [
                    'slug' => 'prosjekter',
            ],
            'menu_icon'           => 'dashicons-groups',
            'supports'            => ['revisions', 'title', 'thumbnail']
        ];
    }


    public function getHeader(): ?string
    {
        return $this->getMeta('header');
    }

    public function getDescription(): ?string
    {
        return $this->getMeta('description');
    }
    public function getSubtitle(): ?string
    {
        return $this->getMeta('subtitle');
    }
    public function getGallery(): ?array
    {
        return $this->getMeta('gallery');
    }

    public function getCTATitle(): ?string
    {
        return $this->getMeta('block_call_to_action_title');
    }
    public function getCTAColumns(): ?string
    {
        return $this->getMeta('block_call_to_action_columns');
    }
    public function getCTAColumn1(): ?string
    {
        return $this->getMeta('block_call_to_action_column_1');
    }
    public function getCTAColumn2(): ?string
    {
        return $this->getMeta('block_call_to_action_column_2');
    }
    public function getCTAShowButton(): ?bool
    {
        return $this->getMeta('block_call_to_action_show_button');
    }
    public function getCTALink(): ?array
    {
        return $this->getMeta('block_call_to_action_button_link');
    }
    public function getCTABackgroundType(): ?string
    {
        return $this->getMeta('block_call_to_action_background_type');
    }


    public function getButtons(): ?array
    {
        $buttons = $this->getMeta('buttons');

        if (empty($buttons)) {
            return null;
        }

        return $buttons;
    }

    public function getBackgroundPicture(): ?Picture
    {
        $background = $this->getMeta('background');

        if (empty($background)) {
            return null;
        }

        return (new Picture($background))->setSizes([
            'lg' => 'slider-1250x450',
            'md' => 'slider-768x450',
            'sm' => 'slider-480x400'
        ])->setAlt($this->getHeader());
    }
}