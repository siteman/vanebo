<?php

namespace PPTheme\Models\Posts;

use Illuminate\Support\Arr;
use PP\Image;
use PP\Models\AbstractPostType;
use PP\Picture;

class Slider extends AbstractPostType
{
    /**
     * Return slug for post type
     *
     * @return string
     */
    public static function getPostType(): string
    {
        return 'slider';
    }

    /**
     * Have to return post type args
     *
     * List of allowed arguments: https://codex.wordpress.org/Function_Reference/register_post_type
     * List dashboard icons: https://developer.wordpress.org/resource/dashicons
     *
     * @return array
     */
    public static function getArgs(): array
    {
        return [
            'labels'              => [
                'name'               => _x('Slider', 'post type general name', _THEME_DOMAIN_),
                'singular_name'      => _x('Slider', 'post type singular name', _THEME_DOMAIN_),
                'menu_name'          => _x('Slider', 'admin menu', _THEME_DOMAIN_),
                'name_admin_bar'     => _x('Slider', 'add new on admin bar', _THEME_DOMAIN_),
                'add_new'            => _x('Dodaj nowy', 'add new', _THEME_DOMAIN_),
                'add_new_item'       => __('Dodaj nowy slider', _THEME_DOMAIN_),
                'new_item'           => __('Nowy element', _THEME_DOMAIN_),
                'edit_item'          => __('Edytuje slider', _THEME_DOMAIN_),
                'view_item'          => __('Zobacz slider', _THEME_DOMAIN_),
                'all_items'          => __('Wszystkie slidery', _THEME_DOMAIN_),
                'search_items'       => __('Szukaj slideru', _THEME_DOMAIN_),
                'parent_item_colon'  => __('Rodzic slidera', _THEME_DOMAIN_),
                'not_found'          => __('Nie znaleziono slidera.', _THEME_DOMAIN_),
                'not_found_in_trash' => __('Nie znaleziono slidera w koszu.', _THEME_DOMAIN_)
            ],
            'public'              => false,
            'publicly_queryable'  => false,
            'exclude_from_search' => true,
            'show_ui'             => true,
            'show_in_nav_menus'   => true,
            'query_var'           => false,
            'capability_type'     => 'post',
            'has_archive'         => true,
            'hierarchical'        => false,
            'rewrite'             => false,
            'menu_icon'           => 'dashicons-format-gallery',
            'supports'            => ['revisions', 'title']
        ];
    }

    public function getHeader(): ?string
    {
        return $this->getMeta('header');
    }

    public function getDescription(): ?string
    {
        return $this->getMeta('description');
    }

    public function getButtons(): ?array
    {
        $buttons = $this->getMeta('buttons');

        if (empty($buttons)) {
            return null;
        }

        return $buttons;
    }

    public function getBackgroundPicture(): ?Picture
    {
        $background = $this->getMeta('background');

        if (empty($background)) {
            return null;
        }

        return (new Picture($background))->setSizes([
            'lg' => 'slider-1250x450',
            'md' => 'slider-768x450',
            'sm' => 'slider-480x400'
        ])->setAlt($this->getHeader());
    }
}