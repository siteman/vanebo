<?php

namespace PPTheme\Models\Options;

use PP\Image;
use PP\Models\AbstractOption;

class Footer extends AbstractOption
{
    public function getLogo(): ?Image
    {
        $logoId =  $this->get('options_footer_logo_id');

        return empty($logoId) ? null : new Image($logoId);
    }

    public function getName(): ?string
    {
        return $this->get('options_footer_name');
    }

    public function getEmail(): ?string
    {
        return $this->get('options_footer_email');
    }

    public function getPhone(): ?string
    {
        return $this->get('options_footer_phone');
    }

    public function getCaption(): ?string
    {
        return $this->get('options_footer_caption');
    }

    public function getColor(): ?string
    {
        return $this->get('options_footer_background_color');
    }

    public function getFacebook()
    {
        return $this->get('options_footer_facebook');
    }

    public function getTwitter(): ?string
    {
        return $this->get('options_footer_twitter');
    }

    public function getGooglePlus(): ?string
    {
        return $this->get('options_footer_google_plus');
    }

    public function getGithub(): ?string
    {
        return $this->get('options_footer_github');
    }

    public function getPinterest(): ?string
    {
        return $this->get('options_footer_pinterest');
    }

    public function getLinkedin(): ?string
    {
        return $this->get('options_footer_linkedin');
    }

    public function getInstagram(): ?string
    {
        return $this->get('options_footer_instagram');
    }

    public function getYoutube(): ?string
    {
        return $this->get('options_footer_youtube');
    }

    public function getLogoMester()
    {
        return $this->get('options_footer_mester');
    }

    public function getLogoSentral()
    {
        return $this->get('options_footer_sentral');
    }

    public function getCol1(): ?string
    {
        return $this->get('options_footer_col_1');
    }

    public function getCol4(): ?string
    {
        return $this->get('options_footer_col_4');
    }

}