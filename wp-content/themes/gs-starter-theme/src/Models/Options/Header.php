<?php

namespace PPTheme\Models\Options;

use PP\Image;
use PP\Models\AbstractOption;

class Header extends AbstractOption
{
    public function getLogo(): ?Image
    {
        $logoId =  $this->get('options_header_logo_id');
        return empty($logoId) ? null : new Image($logoId);
    }
    public function getScriptCodeForHead(): ?string
    {
        $code_for_head =  $this->get('options_header_script_code_for_head');
        return empty($code_for_head) ? '' : $code_for_head;
    }
    public function getScriptCodeForBody(): ?string
    {
        $code_for_body =  $this->get('options_header_script_code_for_body');
        return empty($code_for_body) ? '' : $code_for_body;
    }
}