<?php

namespace PPTheme\Composers;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use PP\AbstractViewComposer;
use PP\Image;
use PP\Models\Options;
use PP\Walkers\BootstrapNavWalker;
use PPTheme\Models\Options\BlockNewsletter;
use PPTheme\Models\Options\Footer;
use PPTheme\Models\Options\SocialMedia;

class FooterComposer extends AbstractViewComposer
{
    /**
     * Return list of views where composer should be registered
     *
     * @return array
     */
    public function getViews(): array
    {
        return [
            'partials/footer.php'
        ];
    }

   /**
     * Return list of variables provides to template
     *
     * @return array
     */
    public function with(): array
    {
        $footer = new Footer();

        return [
            'home_url' => home_url(),
            'sitename' => get_bloginfo(),
            'logo' => $footer->getLogo(),
            'name' => $footer->getName(),
            'email' => $footer->getEmail(),
            'phone' => $footer->getPhone(),
            'caption' => $footer->getCaption(),
            'bgcolor' => $footer->getColor(),
            'facebook' => $footer->getFacebook(),
            'twitter' => $footer->getTwitter(),
            'google_plus' => $footer->getGooglePlus(),
            'github' => $footer->getGithub(),
            'pinterest' => $footer->getPinterest(),
            'linkedin' => $footer->getLinkedin(),
            'instagram' => $footer->getInstagram(),
            'youtube' => $footer->getYoutube(),
            'mester' => $footer->getLogoMester(),
            'sentral' => $footer->getLogoSentral(),
            'col1' => $footer->getCol1(),
            'col4' => $footer->getCol4(),
            'nav' => $this->getNav()
        ];
    }

    public function getNav(): ?string
    {
        return wp_nav_menu([
            'theme_location'  => 'footer_nav',
            'echo'            => false,
            'menu_class'      => 'nav',
            'depth'           => 1,
            'container'       => 'nav',
            'container_class' => 'site-footer__nav',
            'items_wrap'      => '<ul class="%2$s" role="menu">%3$s</ul>',
            'fallback_cb'     => 'PP\Walkers\BootstrapNavWalker::fallback',
            'walker'          => new BootstrapNavWalker(),
        ]);
    }
}