<?php

namespace PPTheme\Composers;

use PP\AbstractViewComposer;
use PP\Models\AbstractPostType;

class ThemeComposer extends AbstractViewComposer
{
    /**
     * Return list of views where composer should be registered
     *
     * @return array
     */
    public function getViews(): array
    {
        return [
            'partials/header.php',
        ];
    }

    /**
     * Return list of variables provides to template
     *
     * @return array
     */
    public function with(): array
    {
        return [
            'theme_header' => $this->getThemeHeader(),
            'theme_body' => $this->getThemeBody()
        ];
    }

    public function getThemeHeader(): string
    {
        /** @var \PP\Models\Posts\Page $page */
        $page = \PP\Facades\Posts::factoryByGlobalPost();
        $themeHeader = $page->getMeta('options_theme_header');

        return !empty($themeHeader) ? $themeHeader : 'primary';
    }

    public function getThemeBody(): string
    {
        /** @var \PP\Models\Posts\Page $page */
        $page = \PP\Facades\Posts::factoryByGlobalPost();
        $themeBody = $page->getMeta('options_theme_body');

        return !empty($themeBody) ? $themeBody : 'primary';
    }
}