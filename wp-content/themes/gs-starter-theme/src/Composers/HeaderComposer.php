<?php

namespace PPTheme\Composers;

use PP\AbstractViewComposer;
use PP\Facades\Router;
use PP\Walkers\BootstrapNavWalker;
use PPTheme\Models\Options\Header;

class HeaderComposer extends AbstractViewComposer
{
    /**
     * Return list of views where composer should be registered
     *
     * @return array
     */
    public function getViews(): array
    {
        return [
            'partials/header.php',
        ];
    }

    /**
     * Return list of variables provides to template
     *
     * @return array
     */
    public function with(): array
    {
        $header = new Header();

        return [
            'home_url' => esc_url(home_url()),
            'sitename' => get_bloginfo(),
            'logo' => $header->getLogo(),
            'has_nav' => has_nav_menu('header_nav'),
            'nav' => $this->getNav(),
            'code_for_body' => $header->getScriptCodeForBody(),
            'code_for_head' => $header->getScriptCodeForHead(),
        ];
    }

    public function getNav(): ?string
    {
        return wp_nav_menu([
            'theme_location'  => 'header_nav',
            'echo'            => false,
            'menu_class'      => 'nav',
            'depth'           => 1,
            'container'       => 'nav',
            'container_class' => 'main-menu__nav',
            'items_wrap'      => '<ul class="%2$s" role="menu">%3$s</ul>',
            'fallback_cb'     => 'PP\Walkers\BootstrapNavWalker::fallback',
            'walker'          => new BootstrapNavWalker(),
        ]);
    }
}