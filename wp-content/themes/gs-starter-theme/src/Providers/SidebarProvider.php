<?php

namespace PPTheme\Providers;

class SidebarProvider
{
    public function register()
    {
        $this->registerSidebars();
    }

    public function registerSidebars()
    {
        register_sidebar([
            'name' => __('Footer sidebar', _THEME_DOMAIN_),
            'id' => 'footer',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="widget__title">',
            'after_title'   => '</h2>',
        ]);
    }
}