<?php

namespace PPTheme\Providers;

class AcfServiceProvider
{
    public function register()
    {
        $this->registerOptionPages();
    }

    public function registerOptionPages()
    {
        if (!class_exists('\ACF')) {
            return;
        }

        $parent = acf_add_options_page([
            'page_title'  => __('Theme General Settings'),
            'menu_title'  => __('Theme Settings'),
            'menu_slug' => 'acf-page-options',
            'autoload' => true,
        ]);

        acf_add_options_sub_page([
            'page_title' => __('Header', _THEME_DOMAIN_),
            'parent_slug' => $parent['menu_slug'],
            'autoload' => true,
        ]);

        acf_add_options_sub_page([
            'page_title' => __('Footer', _THEME_DOMAIN_),
            'parent_slug' => $parent['menu_slug'],
            'autoload' => true,
        ]);
    }
}