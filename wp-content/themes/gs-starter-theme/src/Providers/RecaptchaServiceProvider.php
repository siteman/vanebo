<?php

namespace PPTheme\Providers;

use PP\Foundation\Application;
use PP\Supports\Cons;

class RecaptchaServiceProvider
{
    public function register()
    {
        Application::getInstance()->get('assets')->addJsData([
            'recaptcha_key' => Cons::get('RECAPTCHA_SITE_KEY')
        ]);
    }
}