<?php

namespace PPTheme\Providers;

use Illuminate\Support\Facades\Request;
use PP\Facades\Theme;

class FragmentCacheProvider
{
    public function register()
    {
        add_action('wp_ajax_get_fragment_cache', [$this, 'hookActionFragmentCache']);
        add_action('wp_ajax_nopriv_get_fragment_cache', [$this, 'hookActionFragmentCache']);
    }

    public function hookActionFragmentCache()
    {
        $requestedFragments = esc_sql(Request::get('items'));
        $fragments = [];

        foreach ($requestedFragments as $requestedFragment) {
            $path = Theme::get('config')->get('cache.fragment_cache.'.$requestedFragment);

            ob_start();
            pp_template_include($path);
            $fragments[$requestedFragment] = ob_get_clean();
        }

        pp_response()->json([
            'fragments' => $fragments
        ])->send();
        die();
    }
}