<?php

namespace PPTheme\Providers;

use Illuminate\Support\Arr;

class GalleryProvider
{
    public function register()
    {
        add_filter( 'use_default_gallery_style', '__return_false');

        add_filter('shortcode_atts_gallery', [$this, 'hookAttsGallery']);
    }

    public function hookAttsGallery($args)
    {
        if (Arr::get($args, 'size') === 'thumbnail') {
            Arr::set($args, 'size', 'gallery-370x370');
        }

        return $args;
    }
}