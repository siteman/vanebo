<?php

namespace PPTheme;

use Illuminate\Support\Arr;
use PP\Foundation\Application;
use PP\Foundation\Theme as CoreTheme;

class Theme extends CoreTheme
{
    public function registerSupports(): void
    {
        // Enable woocommerce support
        add_theme_support( 'woocommerce' );

        // Remove wooocommerce styles
        add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

        // Enable plugins to manage the document title
        // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
        add_theme_support('title-tag');

        // Add post thumbnails
        // http://codex.wordpress.org/Post_Thumbnails
        // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
        // http://codex.wordpress.org/Function_Reference/add_image_size
        add_theme_support('post-thumbnails');

        // Add HTML5 markup for captions
        // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
        add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery']);

        // Allow wide for gutenberg
        add_theme_support('align-wide');
    }

    public function imagesPath()
    {
        return $this->baseUrl('assets/images');
    }
}