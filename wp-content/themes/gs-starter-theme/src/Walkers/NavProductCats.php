<?php

namespace PPTheme\Walkers;

use PPTheme\Models\Taxonomies\ProductCat;

class NavProductCats
{
    /**
     * @var mixed|\PP\Collections\WPTermCollections|null
     */
    private $cats = null;

    /**
     * @var ProductCat|null
     */
    private $active = null;

    /**
     * NavProductCats constructor.
     */
    public function __construct()
    {
        $this->cats = ProductCat::getAll([
            'hide_empty' => true,
            'orderby' => 'name',
            'order'   => 'ASC',
            'exclude' => [ProductCat::getDefaultId()]
        ]);

        $this->active = false;

        if (is_product_category()) {
            $this->active = new ProductCat();
        }
    }

    /**
     * Return active category id
     *
     * @return int|null
     */
    public function getActiveId(): ?int
    {
        if (!$this->active) {
            return null;
        }

        return $this->active->getId();
    }

    /**
     * Check if any of child or given category is active category
     *
     * @param \PPTheme\Models\Taxonomies\ProductCat $term
     * @return bool
     */
    public function isActiveDeep(ProductCat $term): bool
    {
        $children = $this->getChildren($term->getId());

        if ($term->getId() === $this->getActiveId()) {
            return true;
        }

        if (!empty($children)) {
            foreach ($children as $child) {
                if ($this->isActiveDeep($child)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Render row html
     *
     * @param \PPTheme\Models\Taxonomies\ProductCat $term
     * @param int $deep
     * @param bool $show
     * @return string
     */
    public function renderRow(ProductCat $term, int $deep = 0, bool $show = false)
    {
        $active = $this->getActiveId() === $term->getId();

        $liClasses = ['shop-cats-list__nav-item'];
        if ($active) {
            $liClasses[] = 'shop-cats-list__nav-item--active';
        }

        $html = '<li class="'.implode(' ', $liClasses).'">
                    <a href="'.$term->getLink().'" title="'.$term->getTitle().'">'.$term->getTitle().'</a>';

        $children = $this->getChildren($term->getId());
        if (!empty($children) && $this->isActiveDeep($term)) {
            $deep++;
            $html .= '<ul class="shop-cats-list__nav-list shop-cats-list__nav-list--children">';
            foreach ($children as $child) {
                $html .= $this->renderRow($child, $deep);
            }
            $html .= '</ul>';
        }

        $html .= '</li>';

        return $html;
    }

    /**
     * Return array of children categories by parent id
     *
     * @param int $parent
     * @return array
     */
    public function getChildren(int $parent = 0): array
    {
        return array_filter($this->cats->getTerms(), function($cat) use ($parent) {
            /** @var $cat ProductCat */
            return $parent === $cat->getParentId();
        });
    }

    /**
     * Render list of categories to display on front
     *
     * @return string
     */
    public function render(): string
    {
        $html = '<ul class="dropdown--mobile shop-cats-list__nav-list">';

        foreach ($this->getChildren(0) as $term) {
            $html .= $this->renderRow($term);
        }

        $html .= '</ul>';

        return $html;
    }
}