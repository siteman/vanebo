<?php

use Illuminate\Support\Arr;

if (! function_exists('get_page_by_template'))
{
    function get_page_by_template(string $template) {
        $args = [
            'post_type'   => 'page',
            'post_status' => 'publish',
            'number'      => 1,
            'nopaging'    => true,
            'meta_key'    => '_wp_page_template',
            'meta_value'  => 'templates/' . trim($template, '.php') . '.php'
        ];
        $pages = get_posts($args);
        return head($pages);
    }
}

if (! function_exists('get_permalink_by_template'))
{
    function get_permalink_by_template(string $template, $slug = '') {
        $page = get_page_by_template($template);
        if (empty($page)) {
            return null;
        }
        return trim(get_permalink($page) . $slug, '/');
    }
}

if (! function_exists('str_cut_words'))
{
    function str_cut_words(?string $text = null, int $limit = 0, string $more = '...') {
        if ($text === null) {
            return '';
        }

        if ($limit === 0) {
            return $text;
        }

        if (strlen($text) > $limit) {
            return substr($text, 0, strpos($text, ' ', $limit)) . $more;
        }

        return $text;
    }
}

if (! function_exists('get_template_svg_file'))
{
    function get_template_svg_file($path) {
        $absolutePath = \PP\Facades\Theme::basePath($path);

        if (file_exists($absolutePath)) {
            return file_get_contents($absolutePath);
        }

        return null;
    }
}