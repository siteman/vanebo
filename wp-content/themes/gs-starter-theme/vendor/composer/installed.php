<?php return array(
    'root' => array(
        'pretty_version' => 'dev-develop',
        'version' => 'dev-develop',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'dff105d80a190e757ea8b565c6295c28f44ae1bb',
        'name' => 'perfectlyproject/wp-template-boilerplate',
        'dev' => true,
    ),
    'versions' => array(
        'perfectlyproject/wp-template-boilerplate' => array(
            'pretty_version' => 'dev-develop',
            'version' => 'dev-develop',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'dff105d80a190e757ea8b565c6295c28f44ae1bb',
            'dev_requirement' => false,
        ),
    ),
);
