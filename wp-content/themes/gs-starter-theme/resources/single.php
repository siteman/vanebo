<?php

if (!defined('ABSPATH')) {
    exit(); // Exit if accessed directly.
}

/** @var integer $post_id */
/** @var \PP\Models\Posts\Page $page */
$page = \PP\Facades\Posts::factoryByGlobalPost();

get_header();
?>
  <div class="single-article">
    <?php


            echo '<a href="'.get_post_type_archive_link( 'post' ).'" class="block-text__back"><    Tilbake</a>';
        ?>
    <?php if (
        isset($page->getPost()->post_title) &&
        !empty($page->getPost()->post_title)
    ): ?>
      <div class="single-article__image">
        <?php echo get_the_post_thumbnail($page->getPost()->ID, 'full'); ?>
      </div>
    <?php endif; ?>
    <?php if (
        isset($page->getPost()->post_title) &&
        !empty($page->getPost()->post_title)
    ): ?>
      <div class="single-article__date">
        <?php echo $page->getPost()->post_date; ?>
      </div>
    <?php endif; ?>
    <?php if (
        isset($page->getPost()->post_title) &&
        !empty($page->getPost()->post_title)
    ): ?>
      <h1 class="single-article__title">
        <?php echo $page->getPost()->post_title; ?>
      </h1>
    <?php endif; ?>
    <?php if (
        isset($page->getPost()->post_title) &&
        !empty($page->getPost()->post_title)
    ): ?>
      <div class="single-article__content">
        <?php echo $page->getContent(true); ?>
    </div>
    <?php endif; ?>
    
  </div>
<?php get_footer();
