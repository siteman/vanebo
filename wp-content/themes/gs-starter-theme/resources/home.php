<?php
$background = '';
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

$posts = \PP\Collections\WPQueryCollections::factoryByGlobal();

get_header();
$background_url = 'https://arcticnuvsvaag.no/wp-content/uploads/2021/11/DJI_0809-HDR-1.png';

if($background_url){
    $background = "background-image: url('".$background_url."')";
}
?>

    <div class="block-text layout6"  style="<?php echo $background; ?>" >
        <div class="block-text__row" >
            <h1 class="title">
                Tutaj jest tytul
            </h1>
            <div class="content">
                Tutaj jest tresc bloku
            </div>
        </div>
    </div>
    <div class="container">
        <?php (new \PP\Libs\Breadcrumbs\Breadcrumbs())->display(); ?>

        <?php pp_template_include('partials/loop/post', [
            'query' => $posts
        ]); ?>

        <?php $posts->pagination()->display(); ?>
    </div>
<?php

get_footer();