<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();

if (get_option('page_on_front')) {
    $page = new \PP\Models\Posts\Page(get_option('page_on_front'));
    $page->setupPostData();
    echo $page->getContent(true);
}

get_footer();