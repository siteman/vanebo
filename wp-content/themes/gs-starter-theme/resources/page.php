<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}


/** @var \PP\Models\Posts\Page $page */
$page = \PP\Facades\Posts::factoryByGlobalPost();

//            $page = get_post($post_id);

//Back button to parent page
//            if( $page && isset($page->post_parent) && !empty($page->post_parent) && $page->post_parent != 0  )
//                echo '<a href="'.get_permalink($page->post_parent).'" class="block-text__back"><    Tilbake</a>';

get_header();
?>
<div class="page__content">
    <?php echo $page->getContent(true); ?>
</div>
<?php

get_footer();