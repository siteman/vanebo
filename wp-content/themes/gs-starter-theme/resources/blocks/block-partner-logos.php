<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var \PP\Image $single_image */
/** @var  string $type */
/** @var  array $carousel_logos */

?>
<div class="block-partner-logos type_<?php echo $type ?>">
    <?php if ($type == 'single_image' && !empty($single_image)): ?>
        <div class="block-partner-logos__single_image">
            <?php echo $single_image->display(); ?>
        </div>
    <?php endif; ?>

    <?php if ($type == 'carousel' && !empty($carousel_logos)): ?>
    <div class="block-partner-logos__carousel">
        <?php  foreach ($carousel_logos as $logo): ?>
            <?php if(!empty($logo['link'])):?>
                <div class="block-partner-logos__carousel_single_logo">
                    <a href="<?php echo $logo['link']['url']; ?>" target="<?php echo $logo['link']['target']; ?>" alt="<?php echo $logo['link']['title']; ?>">
                        <?php echo $logo['logo']->display(); ?>
                    </a>
                </div>
            <?php else: ?>
                <div class="block-partner-logos__carousel_single_logo">
                    <?php echo $logo['logo']->display(); ?>
                </div>
            <?php endif;?>
        <?php endforeach;?>
    </div>
<?php endif; ?>

</div>
<div class="glide">
  <div data-glide-el="track" class="glide__track">
    <ul class="glide__slides">
      <li class="glide__slide">1</li>
      <li class="glide__slide">2</li>
      <li class="glide__slide">3</li>
    </ul>
  </div>
  <div data-glide-el="controls">
    <button data-glide-dir="<<">Start</button>
    <button data-glide-dir=">>">End</button>
    </div>
</div>