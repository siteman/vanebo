<?php

if ( ! defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var array $columns */
/** @var  string $main_header */
/** @var string $background_color */

?>
<div class="block-features <?php echo $background_color; ?>">
    <?php if (!empty($main_header)): ?>
        <h2 class="block-features__main-header"><?php echo $main_header?></h2>
    <?php endif;?>
    <?php if (!empty($columns)): ?>
        <div class="block-features__columns">
        <?php foreach ($columns as $column): ?>
            <div class="block-features__column">
                <?php if (!empty($column['pp_image'])): ?>
                    <div class="block-features__column-image">
                        <?php echo  $column['pp_image']->display();?>
                    </div>
                <?php endif; ?>
                <?php if (!empty($column['header'])): ?>
                    <h3 class="block-features__column-header">
                        <?php echo  $column['header']?>
                    </h3>
                <?php endif; ?>
                <?php if (!empty($column['text'])): ?>
                    <p class="block-features__column-text">
                        <?php echo  $column['text']?>
                    </p>
                <?php endif; ?>
                <?php if ($column['show_buttonlink'] === true && !empty($column['link'])): ?>
                    <div class="block-single-feature__buttons">
                        <a href="<?php echo $column['link']['url']; ?>" class="button button--secondary" target="<?php echo $column['link']['target']; ?>"><?php echo $column['link']['title']; ?></a>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach;?>
        </div>
    <?php endif;?>
</div>
