<?php

use PP\Image;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var  string $content */
/** @var  string $title */
/** @var  Image $image */

?>
<div class="block_timeline">
    <div class="block_timeline__row">
        <div class="block_timeline__row__title"> <?php echo $title; ?></div>
        <div class="block_timeline__row__description"> <?php echo $content; ?></div>
        <div class="block_timeline__row__box">

            <div class="glide-timeline glide">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                    <?php 
                        if (have_rows('block_timeline_steps')):
                            while (have_rows('block_timeline_steps')) : the_row(); ?>
                        <li class="glide__slide">
                            <?php 
                                $year = get_sub_field('year');
                                $step_content = get_sub_field('content');
                            ?>
                            <div class="step" style="">
                                <div class="year"><?php echo $year ?></div>
                                <div class="title"><?php echo $step_content ?></div>
                            </div>
                        </li>
                    <?php  
                            endwhile;
                        endif;
                    ?>
                </ul>
            </div>
            <div class="glide__arrows" data-glide-el="controls">
                    <button class="glide__arrow glide__arrow--left" data-glide-dir="&lt;"><i class="fas fa-chevron-left"></i> </button>
                    <button class="glide__arrow glide__arrow--right" data-glide-dir="&gt;"><i class="fas fa-chevron-right"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>