<?php
$pins = [];
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>
<div class="block_map">
    <div class="block_map__header"><?php echo $title ?></div>
    <div class="block_map__content">
        <div class="block_map__points">
        <?php
        if (have_rows('block_map_points')):
            while (have_rows('block_map_points')) : the_row();
                        $city = get_sub_field('city');
                        $street = get_sub_field('street');
                        $country = get_sub_field('country');
                        $additional_info = get_sub_field('additional_info');
                        $latitude = get_sub_field('latitude');
                        $longitude = get_sub_field('longitude');

                        $cord = '{lat: '.$latitude.',lng: '.$longitude .'}';
                        if(!empty($latitude) && !empty($longitude))
                            $pins[] = $cord;
            ?>
            <div class="point">
                <div class="point__city"><?php echo $city ?></div>
                <div><?php echo $street ?></div>
                <div><?php echo $additional_info ?></div>
                <div><?php echo $country ?></div>
                <button onclick="map.setCenter(<?php echo $cord?>)"> See location</button>
            </div>

            <?php
            endwhile;
        endif;

        ?>

        </div>
        <script type="text/javascript">
            let map;
            function initMap() {
                map = new google.maps.Map(document.getElementById("map"), {
                    center: { lat: 59.180490, lng: 10.210610 },
                    zoom: 15,
                });
                <?php if(!empty($pins)): ?>
                     <?php foreach($pins as $key => $pin):?>
                        marker = new google.maps.Marker({
                            position: <?php echo $pin ?>,
                            map: map,
                        });
                    <?php endforeach; ?>
                <?php endif;?>

            }

        </script>

        <div class="map">l
            <div id="map"></div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo WP_GOOGLE_MAP_API; ?>&callback=initMap&libraries=&v=weekly&channel=2" async></script>
