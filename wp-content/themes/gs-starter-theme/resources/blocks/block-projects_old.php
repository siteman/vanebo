<?php

/** @var  string $title */

if ( ! defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}?>
<?php
if (have_rows('block_projects')):?>
    <div class="block_project">
        <div class="block_project__row">
            <div class="block_project__row__title"><?php echo $title; ?></div>
            <div class="block_project__row__box">
            <?php
            while (have_rows('block_projects')) : the_row();
                $title2 = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $button = get_sub_field('button');?>

                <div class="project">
                    <div>
                        <div class="project__title"><?php echo $title2; ?></div>
                        <div class="project__subtitle"><?php echo $subtitle ?></div>
                    </div>
					<?php
                        $target = null;
                        if($button['target'] != '0')
                            $target = 'target="'.$button['target'].'"';
					?>
                    <div class="project__button"><a href="<?php echo $button['url'] ?>" <?php echo $target; ?>><?php echo $button['title'] ?> </a></div>
                </div>
            <?php endwhile;?>
            </div>
        </div>
    </div>
    <?php
endif;
?>



