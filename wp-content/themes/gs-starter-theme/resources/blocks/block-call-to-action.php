<?php

if (!defined('ABSPATH')) {
    exit(); // Exit if accessed directly.
}
/** @var  string $title */
/** @var  string $columns */
/** @var  string $column_1 */
/** @var  string $column_2 */
/** @var  boolean $show_button */
/** @var  array $button_link */
/** @var  string $background_type */
?>
<div class="block-call-to-action <?php echo $background_type; ?>">

    <?php if (!empty($title)): ?>
        <div class="block-call-to-action__title"><?php echo $title; ?></div>
    <?php endif; ?>
    <?php if (!empty($column_1)): ?>
        <div class="block-call-to-action__text"><p><?php echo $column_1; ?></p></div>
    <?php endif; ?>
    <?php if (!empty($column_2) && $columns == '2'): ?>
        <div class="block-call-to-action__text"><p><?php echo $column_2; ?></p></div>
    <?php endif; ?>
    <?php if ($show_button === true && !empty($button_link)): ?>
        <div class="block-call-to-action__buttons orange button center">
            <a href="<?php echo $button_link[
                'url'
            ]; ?>" class="button button--secondary" target="<?php echo $button_link[
    'target'
]; ?>"><?php echo $button_link['title']; ?></a>
        </div>
    <?php endif; ?>
</div>


</div>
