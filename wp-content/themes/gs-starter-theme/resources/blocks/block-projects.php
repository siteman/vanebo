<?php

/** @var  string $title */
/** @var string $background_color */

if ( ! defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}?>
<?php
if (have_rows('block_projects')):?>
    <div class="block_project <?php echo $background_color; ?>">
        <div class="block_project__row">
            <h2 class="block_project__row__title"><?php echo $title; ?></h2>
            <div class="block_project__row__box">
            <?php
            while (have_rows('block_projects')) : the_row();

                $project = get_sub_field('project');
                $subtitle = get_post_meta($project->ID , 'subtitle', true);
                $title = get_the_title($project->ID , 'title', true);

                ?>

                <div class="single-project">
                    <?php echo get_the_post_thumbnail($project->ID,'full'); ?>
                    <div class="single-project__title-wrapper">
                        <h3 class="single-project__title"><?php echo $project->title ?></h3>
                        <h3 class="single-project__title"><?php echo $title ?></h3>
                        <div class="single-project__subtitle"><?php echo $subtitle ?></div>
                        <div class="single-project__button"><a href="<?php echo get_permalink($project->ID) ?>"> <?php echo __('Les mer') ?> </a></div>
                    </div>

                </div>
            <?php endwhile;?>
            </div>
        </div>
    </div>
    <?php
endif;
?>



