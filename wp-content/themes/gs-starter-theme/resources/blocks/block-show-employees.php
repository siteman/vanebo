<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var  string $header */
/** @var  object $latest_employees */


?>

<?php
 
if (have_rows('block_show_employees_repeater')):?>
    <div class="block_employees be-bg-<?php echo $layout?>">
		<div class="block_employees__row__title"><?php echo $header; ?></div>
		<div class="block_employees__row__description"><?php echo $description; ?></div>
		<div class="block_employees__row__box">
        <?php
        while (have_rows('block_show_employees_repeater')) : the_row(); ?>
        <div class="employee">
            <?php $employee = get_sub_field('employee'); 
            $image = get_field('employees_image', $employee->ID);
            $size = 'img-370x465'; // (thumbnail, medium, large, full or custom size)
            if( $image ) {
                echo '<div class="employee__image">'.wp_get_attachment_image( $image, $size ).'</div>';
            } ?>
            <div class="employee__title"><?php echo $employee->post_title; ?></div>
            <?php if (get_field('employees_position', $employee->ID)):?>
             <div class="employee__position"><span><?php _e('Title:'); ?></span><?php echo get_field('employees_position', $employee->ID); ?></div>
            <?php endif; ?>
            <?php if (get_field('employees_education', $employee->ID)):?>
                <div class="employee__education"><span><?php _e('Education:'); ?></span><?php echo get_field('employees_education', $employee->ID); ?></div>
            <?php endif; ?>
            <?php if (get_field('employees_email', $employee->ID)):?>
                <div class="employee__email">
                    <span><?php _e('Email:'); ?></span>
                    <a href="mailto:<?php echo get_field('employees_email', $employee->ID); ?>">
                        <?php 
                            $email = explode("@pure", get_field('employees_email', $employee->ID));
                            if( !empty($email[1]) ){
                                echo $email[0].'@pure<br/>'.$email[1];
                            }
                            else {
                                echo get_field('employees_email', $employee->ID);
                            }
                        ?>
                    </a>
            </div>
            <?php endif; ?>
            <?php if (get_field('employees_mobile', $employee->ID)):?>
                <div class="employee__phone"><span><?php _e('Phone number:'); ?></span><a href="tel:<?php echo get_field('employees_mobile', $employee->ID); ?>"><?php echo get_field('employees_mobile', $employee->ID); ?></a></div>
            <?php endif; ?>
        </div>
        <?php endwhile;?>
    </div>
    <?php
endif;
?>