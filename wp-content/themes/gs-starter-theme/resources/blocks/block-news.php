<?php

use PP\Image;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var  string $type */
/** @var  integer $number_of_newest_posts */
/** @var  integer $number_of_skipped_news */
/** @var  integer $posts_number */
/** @var  string $title */

$ajaxurl = admin_url('admin-ajax.php');
?>
<div class="block-news">
    <div class="block-news__container">
        <?php if (!empty($type) && $type == 'type1') : ?>
        <div class="block-news__title">
            <h2><?= $title; ?></h2>
        </div>
        <?php endif; ?>

    <!-- Block for Newest Blogs -->
    <?php if (!empty($type) && $type == 'type1') :
        $args = [
            // 'numberposts'   => 1,
            'numberposts'   => $number_of_newest_posts,
            'post_status'   => 'publish',
            'post_type'     => 'post',
            'offset'        => 0,
            'orderby'       => 'post_date',
            'order'         => 'DESC',
        ];
        $posts = wp_get_recent_posts($args); ?>

        <div class="block-news__grid-container" id="block-news-section">
            <?php foreach ($posts as $post) : ?>
                <article class="block-news__single">
                    <a href="<?= esc_html_e(the_permalink($post['ID']), 'gs-starter-theme'); ?>" class="block-news__single--image">
                        <?= get_the_post_thumbnail($post['ID'], 'full'); ?>
                    </a>
                    <header class="block-news__single--header">
                        <p class="block-news__single--date">
                            <time datetime="<?= get_the_date('d F y H:i:s'); ?>"><?= get_the_date('d F y H:i:s'); ?></time>
                        </p>
                        <a href="<?= esc_html_e(the_permalink($post['ID']), 'gs-starter-theme'); ?>">
                            <h3 class="block-news__single--title"><?= esc_html_e($post['post_title'], 'gs-starter-theme'); ?></h3>
                        </a>
                        <p class="block-news__single--excerpt"><?= esc_html_e(get_the_excerpt($post['ID']), 'gs-starter-theme'); ?></p>
                    </header>
                </article>
            <?php endforeach; ?>
        </div>

    <!-- Block for all posts -->
    <?php elseif (!empty($type) && $type == 'type2') : ?>
        <h2 class="block-all-news-title"><?= $title; ?></h2>
        <div class="block-all-news-container">
            <?php
            $args = [
                'numberposts'   => $posts_number,
                'offset'        => $number_of_skipped_news,
                'orderby'       => 'post_date',
                'order'         => 'DESC',
            ];
            $posts = wp_get_recent_posts($args);
            foreach ($posts as $post) : ?>
                <article class="block-all-news-container__article">
                    <header class="block-all-news-container__header">
                        <p class="block-all-news-container__date">
                            <time datetime="<?= get_the_date('d F y'); ?>"><?= get_the_date('d F y'); ?></time>
                        </p>
                        <a href="<?php echo get_post_permalink($post['ID']); ?>">
                            <h3 class="block-all-news-container__title"><?php echo $post['post_title']; ?></h3>
                        </a>
                    </header>
                </article>
            <?php endforeach; ?>
        </div>

    <?php endif; ?>
        <div class="block-all-news-button">
            <a  class="btn btn--dark-blue u-px-12 u-py-3 u-fw-bold u-mt-15" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">Vis flere aktuelt saker</a>
        </div>
    </div>   
</div>
