<?php

use PP\Image;

if (!defined('ABSPATH')) {
    exit(); // Exit if accessed directly.
}

/** @var  boolean $show_step_numbers */
/** @var  boolean $show_arrows */
/** @var  boolean $title_checkbox */
/** @var  string $title */
/** @var  boolean $button_checkbox */
/** @var  array $button */
/** @var  Image $arrow_icon */
?>
    <div class="block-steps">
    <?php if ($title_checkbox && !empty($title)): ?>
        <h2 class="block-steps__title"> <?php echo $title; ?></h2>
    <?php endif; ?>
        <div class="block-steps__row">
            <?php if (have_rows('block_steps')):
                $number = 1;
                while (have_rows('block_steps')):

                    the_row();
                    $image = get_sub_field('image');
                    $title = get_sub_field('title');
                    $subtitle = get_sub_field('subtitle');
                    $button_checkbox = get_sub_field('button_checkbox');
                    $button = get_sub_field('button');
                    ?>
                    
                    <div class="step">
                        <div class="step__image"><?php echo wp_get_attachment_image(
                            $image['id'],
                            'full'
                        ); ?> </div>
                        <h3 class="step__title"> 
                            <?php if($button_checkbox && !empty($button['url']) ): ?>
                                <a href="<?php echo $button['url']; ?>"><?php echo $title; ?> <i class="fa fa-light fa-chevron-right"></i></a>
                            <?php else: ?>
                                <?php echo $title; ?> 
                            <?php endif; ?>
                        </h3>
                        
                        <?php if (
                            $show_arrows && !empty($arrow_icon)
                        ): ?>
                            <div class="step__arrow">
                                <?php echo wp_get_attachment_image(
                                    $arrow_icon['id'],
                                    'thumbnail'
                                ); ?>
                            </div>
                        <?php endif; ?>
                        <div class="subtitle"> <?php echo $subtitle; ?> </div>
                    </div>

                    <?php $number++;
                endwhile;
            endif; ?>
        </div>
    <?php if ($button_checkbox && !empty($button['title'])): ?>
        <div class="button">
            <button url="<?php echo $button['url']; ?>"> <?php echo $button[
    'title'
]; ?></button>
        </div>
    <?php endif;
?></div>