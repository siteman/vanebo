<?php

use PP\Image;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var string $type */
/** @var Image $image */
/** @var string $layout */
/** @var string $layout_1_title */
/** @var string $layout_1_subtitle */
/** @var string $layout_1_button */
/** @var string $layout_2_title */
/** @var string $layout_2_subtitle */
/** @var string $layout_2_button */
/** @var string $youtube_id_embed */
/** @var boolean $show_youtube_buttons */

?>
<div class="block_slider">
    <div class="glide-slider glide">
        <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
                <?php
                if (have_rows('block-slider')):
                    while (have_rows('block-slider')) : the_row();


                        $type = get_sub_field('block_slider_type');
                        $image = get_sub_field('block_slider_image');
                        $layout = get_sub_field('block_slider_layout');
                        $layout_1_title = get_sub_field('block_slider_layout_1_title');
                        $layout_1_subtitle = get_sub_field('block_slider_layout_1_subtitle');
                        $layout_1_button = get_sub_field('block_slider_layout_1_button');
                        $layout_2_title = get_sub_field('block_slider_layout_2_title');
                        $layout_2_subtitle = get_sub_field('block_slider_layout_2_subtitle');
                        $layout_2_button = get_sub_field('block_slider_layout_2_button');
                        $youtube_id_embed = get_sub_field('block_slider_youtube_id_embed');
                        $show_youtube_buttons = get_sub_field('block_slider_show_youtube_buttons');

    ?> <li class="glide__slide"><?php
                        if (!empty($type) && isset($type)) {
                            if ($type == 'image') {
                                if ($layout == 'layout_1') {
                                    ?>
                                    <div class="layout_1 ">
                                        <?php if (isset($image['id']) && !empty($image['id'])): ?>
                                            <div class="image">
                                                <?php echo wp_get_attachment_image($image['id'], 'full') ?>
                                            </div>
                                        <?php endif ?>
                                        <?php if (isset($layout_1_title) && !empty($layout_1_title)): ?>
                                            <div class="title">
                                                <span><?php echo $layout_1_title ?></span>
                                            </div>
                                        <?php endif ?>
                                        <?php if (isset($layout_1_subtitle) && !empty($layout_1_subtitle)): ?>
                                            <div class="subtitle">
                                                <span><?php echo $layout_1_subtitle ?></span>
                                            </div>
                                        <?php endif ?>
                                        <?php if (isset($layout_1_button['url']) && !empty($layout_1_button['url']) && isset($layout_1_button['title']) && !empty($layout_1_button['title'])): ?>
                                            <div class="button">
                                                <button src="<?php echo $layout_1_button['url'] ?>"><?php echo $layout_1_button['title'] ?></button>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                    <?php
                                } elseif ($layout == 'layout_2') {
                                    ?>
                                    <div class="layout_2 ">
                                        <?php if (isset($image['id']) && !empty($image['id'])): ?>
                                            <div class="image">
                                                <?php echo wp_get_attachment_image($image['id'], 'full') ?>
                                            </div>
                                        <?php endif ?>
                                        <?php if (isset($layout_2_title) && !empty($layout_2_title)): ?>
                                            <div class="title">
                                                <span><?php echo $layout_2_title ?></span>
                                            </div>
                                        <?php endif ?>
                                        <?php if (isset($layout_2_subtitle) && !empty($layout_2_subtitle)): ?>
                                            <div class="subtitle">
                                                <span><?php echo $layout_2_subtitle ?></span>
                                            </div>
                                        <?php endif ?>
                                        <?php if (isset($layout_2_button['url']) && !empty($layout_2_button['url']) && isset($layout_2_button['title']) && !empty($layout_2_button['title'])): ?>
                                            <div class="button">
                                                <button src="<?php echo $layout_2_button['url'] ?>"><?php echo $layout_2_button['title'] ?></button>
                                            </div>
                                        <?php endif ?>
                                    </div>


                                    <?php
                                }

                            } elseif ($type == 'video' && isset($youtube_id_embed) && !empty($youtube_id_embed)) {

                                $styles = '';

                                $params = '?autoplay=1';
                                $params .= '&modestbranding=1';
                                $params .= '&loop=1';
                                $params .= '&disablekb=1';
                                $params .= '&autohide=1';
                                $params .= '&iv_load_policy=3';
                                $params .= '&cc_load_policy=1';
                                $params .= '&mute=1';
                                $params .= '&playlist=' . $youtube_id_embed;
                                $params .= '&rel=0';

                                if (isset($show_youtube_buttons) && $show_youtube_buttons == true) {
                                    $params .= '&controls=2';
                                } else {
                                    $params .= '&controls=0';
                                    $styles = "pointer-events:none;";
                                }
                                ?>
                                <div class="ytplyaer" style="<?php echo $styles ?>">
                                    <iframe id="ytplayer" type="text/html" width="100%" height="460px"
                                            src="//www.youtube.com/embed/<?php echo $youtube_id_embed; ?><?php echo $params; ?>"
                                            allow="autoplay; encrypted-media" webkitallowfullscreen="" allowfullscreen=""
                                            frameborder="0"></iframe>
                                </div>
                                <?php
                            }

                        }
                        ?> </li><?php
                    endwhile;
                endif;


                ?>
        </ul>
        </div>
        <div class="glide__arrows" data-glide-el="controls">
            <button class="glide__arrow glide__arrow--left" data-glide-dir="&lt;"><i class="fas fa-chevron-left"></i> </button>
            <button class="glide__arrow glide__arrow--right" data-glide-dir="&gt;"><i class="fas fa-chevron-right"></i></button>
        </div>
    </div>
</div>
