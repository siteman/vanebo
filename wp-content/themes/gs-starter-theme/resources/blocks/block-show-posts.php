<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var  string $header */
/** @var  object $latest_posts */


?>
<div class="block-show-posts">
    <?php if(!empty($header)):?>
        <h2 class="block-show-posts__header"><?php echo $header;?></h2>
    <?php endif;?>
    <?php
    // The Loop
    if ( $latest_posts->have_posts() ) { ?>
        <div class="block-show-posts__wrapper">
        <?php
        while ( $latest_posts->have_posts() ) {
        $latest_posts->the_post();
        ?>
              <div class="block-show-posts__single-post">
                   <?php if ( has_post_thumbnail() ) { ?>
                        <div class="block-show-posts__image" >
                            <a href="<?php the_permalink(); ?>" class="lightbox" title="<?php the_title(); ?>" rel="bookmark">
                                <?php the_post_thumbnail( 'img-300x300' ); ?>
                            </a>
                        </div>
                    <?php } ?>
                  <h3 class="block-show-posts__title">
                      <a class="block-show-posts__title-link href="<?php the_permalink(); ?>" class="lightbox" title="<?php the_title(); ?>" rel="bookmark">
                        <?php the_title(); ?>
                      </a>
                  </h3>

              </div>

        <?php } ?>
        </div>
    <?php }
    else { ?>
        <div class="block-show-posts__noposts"><?php echo __('No posts to show', _THEME_DOMAIN_)?></div>
    <?php }
    /* Restore original Post Data */
    wp_reset_postdata();

    ?>

</div>
