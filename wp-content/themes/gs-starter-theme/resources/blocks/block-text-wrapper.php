<?php

if (!defined('ABSPATH')) {
    exit(); // Exit if accessed directly.
}

/** @var  string $content */
/** @var  string $background_color */
/** @var  string $text_color */

?>

<div class="block-text-box" style="<?= $background_color ? 'background-color:' . $background_color . ';' : null; ?> <?= $text_color ? 'color:' . $text_color . ';' : null; ?>">
    <div class="content">
        <?= $content; ?>
    </div>
</div>