<?php

if ( ! defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var string $title */
/** @var string $content */
/** @var string $buttons */
/** @var string $button_align */
/** @var string $button_layout */
/** @var string $layout */
/** @var string $h1_title */
/** @var Image $background_image */
/** @var string $content_width */
/** @var string $content_align */
/** @var integer $post_id */

$background = '';

if(!empty($background_image['url']) && ($layout == "layout1" || $layout == "layout4" || $layout == "layout5" || $layout == "layout6")){
	$background = "background-image: url('".$background_image['url']."')";
}

?>
<div class="block-text <?php echo $layout; ?>" style="<?php echo $background ?>" >
    <div class="block-text__row" >
        <?php
            $page = get_post($post_id);

            //Back button to parent page
            if( $page && isset($page->post_parent) && !empty($page->post_parent) && $page->post_parent != 0  )
                echo '<a href="'.get_permalink($page->post_parent).'" class="block-text__back"><    Tilbake</a>';
        ?>
        <?php if(!empty($title)): ?>
            <?php if($h1_title == true): ?>
                <h1 class="title <?php echo $content_width; ?>">
                    <?php echo $title; ?>
                </h1>
            <?php else:?>
                <h2 class="title <?php echo $content_width; ?>">
                    <?php echo $title; ?>
                </h2>
            <?php endif;?>
        <?php endif;?>


        <?php if(!empty($content)): ?>
            <div class="content <?php echo $content_width; ?>">
                <?php echo $content; ?>
            </div>
        <?php endif; ?>

        <?php
		if (have_rows('block_text_buttons')): ?>
            <div class="button <?php echo $button_align ?> <?php echo $button_layout ?>">
		    <?php while (have_rows('block_text_buttons')) : the_row();
				$button = get_sub_field('button');?>
					<?php if(!empty($button['url'] && !empty($button['title']))): ?>
                    <?php
                        $target = null;
                        if($button['target'] != '0')
                            $target = 'target="'.$button['target'].'"';
                    ?>
                        <a href="<?php echo $button['url']?>" <?php echo $target?>> <?php echo $button['title']?> </a>
					<?php endif ?>
            <?php
		    endwhile; ?>
            </div>
        <?php endif; ?>

    </div>

</div>