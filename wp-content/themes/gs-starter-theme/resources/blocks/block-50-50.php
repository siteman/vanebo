<?php

if ( ! defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
/** @var string $title */
/** @var string $image_align */
/** @var string $content */
/** @var Image $image */
/** @var array $button */
/** @var array $parameters */
/** @var string $background_color */
/** @var string $button_layout */

?>

<div class="block-50-50-wrapper <?php echo $background_color; ?>">

    <div class="block-50-50 <?php echo $image_align;?>" >

        <div class="block-50-50__image">
            <?php if(!empty($image)):?>
                <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
            <?php endif; ?>
        </div>
        <div class="block-50-50__text-content">
			<?php if(isset($title) && !empty($title)): ?>
                <h2 class="block-50-50__title">
                    <?php echo $title;?>
                </h2>
			<?php endif;?>
            <?php if(isset($content) && !empty($content)): ?>
                <div class="block-50-50__content">
                    <?php echo $content;?>
                </div>
			<?php endif;?>
            <?php if(isset($parameters) && !empty($parameters)):?>
                <?php foreach($parameters as $parameter): ?>
                    <div class="block-50-50__icon">
                        <?php echo $parameter['icon'];?>
                        <?php echo $parameter['parameter'];?>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <?php if(isset($button) && !empty($button)): ?>
                <div class="block-50-50__button button <?php echo $button_layout; ?>">
					<?php if(!empty($button['url'] && !empty($button['title']))): ?>
						<?php
						$target = null;
						if($button['target'] != '0')
							$target = 'target="'.$button['target'].'"';
						?>
                        <a href="<?php echo $button['url']?>" <?php echo $target?>> <?php echo $button['title']?> </a>
					<?php endif ?>
                </div>
			<?php endif;?>
        </div>

    </div>

</div>