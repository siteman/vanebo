<?php

if ( ! defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var \PP\Image $image */
/** @var  string $image_alignment */
/** @var  string $header_text */
/** @var  string $header_text2 */
/** @var  string $content_text */
/** @var  string $content_text2 */
/** @var  boolean $show_button */
/** @var  boolean $show_informations */
/** @var  array $button  */

?>
<div class="bsf-bg-<?php echo $layout_background?>">
    <div class="block-single-feature align-<?php echo $image_alignment?>" data-align="<?php echo $image_alignment?>">
        <?php if($image_alignment == 'text'):?>
            <div class="block-single-feature__text">
                <div class="block-single-feature__desc">
                    <?php if (!empty($header_text2)): ?>
                        <div class="block-single-feature__header">
                            <?php echo $header_text2; ?>
                        </div>
                    <?php endif; ?>
					<?php if (!empty($content_text2)): ?>
                        <div class="block-single-feature__content" data-align="<?php echo $image_alignment?>">
							<?php echo $content_text2;?>
                        </div>
					<?php endif; ?>
                </div>
            </div>


        <?php else:?>
            <?php if (!empty($image)): ?>
            <div class="block-single-feature__media <?php echo ((!empty($header_text)) ? '' : 'no-padding-top' );?>" data-align="<?php echo $image_alignment?>">
                <?php echo  $image->display();?>
            </div>
            <?php endif; ?>
        <?php endif; ?>
        <div class="block-single-feature__text">
            <div class="block-single-feature__desc">
                <?php if (!empty($header_text)): ?>
                    <div class="block-single-feature__header">
                        <?php echo $header_text; ?>
                    </div>
                <?php endif; ?>
                <?php if (!empty($content_text)): ?>
                    <div class="block-single-feature__content">
                        <?php echo $content_text; ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php if ($show_button === true && !empty($button)): ?>
                <div class="block-single-feature__buttons">
					<?php
                        $target = null;
                        if($button['target'] != '0')
                            $target = 'target="'.$button['target'].'"';
					?>
                    <a href="<?php echo $button['url']; ?>" class="button button--secondary" <?php echo $target; ?>><?php echo $button['title']; ?></a>
                </div>
            <?php endif; ?>

			<?php if ($show_informations === true):?>
                <div class="block-single-feature__informations"><?php
				if (have_rows('block_single_feature_informations')):
					while (have_rows('block_single_feature_informations')) : the_row();
						$icon = get_sub_field('icon');
						$text = get_sub_field('text');
						?>
                        <div class="information">
                            <div class="information__icon"><?php echo $icon; ?></div>
                            <div class="information__text"><?php echo $text; ?></div>
                        </div>
					<?php endwhile;
				endif;?>
                </div><?php
			endif;?>
        </div>

    </div>
</div>
