<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var  string $header */
/** @var  object $latest_post */

?>
<div class="block-single-blog-post">
    
    <?php   if ( $latest_post->have_posts() ) : ?>
        <div class="block-single-blog-post__wrapper">
            <?php while ( $latest_post->have_posts() ) :
                $latest_post->the_post();
            ?>
                <h3 class="block-single-blog-post__title">
                    <?php the_title(); ?>
                </h3>
                    <?php if ( has_post_thumbnail() ) : ?>
                        <div class="block-single-blog-post__image" >                     
                                <?php the_post_thumbnail( 'medium_large' ); ?>
                        </div>
                    <?php endif; ?>
                <div class="block-single-blog-post__content">
                    <?php the_content(); ?>
                </div>

                <div class="block-single-blog-post__media col-sm">
                    <ul>
                    <?php if(!empty($facebook)) : ?>
                        <li><a href="<?php echo $facebook;?>" target="_blank"><span class="fab fa-facebook fa-2x icon"></span></a></li>
                    <?php endif; ?>
                    <?php if(!empty($twitter)) : ?>
                        <li><a href="<?php echo $twitter;?>" target="_blank"><span class="fab fa-twitter fa-2x icon"></span></a></li>
                    <?php endif; ?>
                    <?php if(!empty($google_plus)) : ?>
                        <li><a href="<?php echo $google_plus;?>" target="_blank"><span class="fab fa-google-plus fa-2x icon"></span></a></li>
                    <?php endif; ?>
                    <?php if(!empty($github)) : ?>
                        <li><a href="<?php echo $github;?>" target="_blank"><span class="fab fa-github fa-2x icon"></span></a></li>
                    <?php endif; ?>
                    <?php if(!empty($pinterest)) : ?>
                        <li><a href="<?php echo $pinterest;?>" target="_blank"><span class="fab fa-pinterest fa-2x icon"></span></a></li>
                    <?php endif; ?>
                    <?php if(!empty($linkedin)) : ?>
                        <li><a href="<?php echo $linkedin;?>" target="_blank"><span class="fab fa-linkedin fa-2x icon"></span></a></li>
                    <?php endif; ?>
                    <?php if(!empty($instagram)) : ?>
                        <li><a href="<?php echo $instagram;?>" target="_blank"><span class="fab fa-instagram fa-2x icon"></span></a></li>
                    <?php endif; ?>
                    <?php if(!empty($youtube)) : ?>
                        <li><a href="<?php echo $youtube;?>" target="_blank"><span class="fab fa-youtube fa-2x icon"></span></a></li>
                    <?php endif; ?>
                    </ul>
                </div>
            <?php endwhile; ?>
        </div>
    <?php else : ?>
        <div class="block-single-blog-post__noposts"><?php echo __('No posts to show', _THEME_DOMAIN_)?></div>
    <?php endif;

    /* Restore original Post Data */
    wp_reset_postdata();

    ?>

</div>
