<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var  string $text */
/** @var  string $type_h */
/** @var  string $font */


?>

<?php if(!empty($text)): ?>
    <div class="block-show-header">
        <<?php echo $type_h;?> class="font-<?php echo $font?>">
            <?php echo $text;?>
        </<?php echo $type_h?>>
    </div>
<?php endif; ?>
