<?php

use PP\Image;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
//var_dump(have_rows('buttons'));
?>


<div class="buttons">
    <?php

    if (have_rows('buttons')):
        while (have_rows('buttons')) : the_row();
            $button = get_sub_field('button');
            $layout = get_sub_field('layout');
            ?>
            <div class="<?php echo $layout ?>">
                <button src="<?php echo $button['url'] ?>"> <?php echo $button['title'] ?></button>
            </div>

            <?php

        endwhile;
    endif;
    ?>
</div>
