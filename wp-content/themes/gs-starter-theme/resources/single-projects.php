<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/** @var \PP\Models\Posts\Page $page */
$page = \PP\Facades\Posts::factoryByGlobalPost();

get_header();
$gallery = $page->getGallery();

?>
<div class="single-project">
    <?php
        echo '<a href="'.get_post_type_archive_link( 'page' ).'" class="single-project__back"><    Tilbake</a>';
    ?>
    <br><br><br>
    <div class="single-project__content">
        <div class="single-project__content-left">
            <?php if (
                isset($page->getPost()->post_title) &&
                !empty($page->getPost()->post_title)
            ): ?>
            <h1 class="single-project__title">
                <?php echo $page->getPost()->post_title; ?>
            </h1>
            <?php endif; ?>

            <?php if (
                isset($page->getPost()->post_title) &&
                !empty($page->getPost()->post_title)
            ): ?>
            <p class="single-project__subtitle">
                <?php echo $page->getSubtitle(); ?>
            </p>
            <?php endif; ?>
        </div>

        <div class="single-project__content-right">
            <?php if (
                isset($page->getPost()->post_title) &&
                !empty($page->getPost()->post_title)
            ): ?>
            <div class="single-project__image">
                <?php echo $page->getFeatureThumbnail('full'); ?>
            </div>
            <?php endif; ?>

        </div>
    </div>
    
    <div class="single-project__gallery">
      <?php echo $page->getContent(true); ?>
      
      <?php if(!empty($gallery) && is_array($gallery)):?>
            <?php foreach($gallery as $photo):?>
                <div class="single-project__gallery-single-item">
                    <img src="<?php echo($photo['sizes']['large']); ?>">
                </div>
            <?php endforeach; ?>
      <?php endif;?>
    </div>
</div>

<div class="<?php echo $page->getCTABackgroundType(); ?> block-call-to-action">
    <h2 class="block-call-to-action__title"><?php echo $page->getCTATitle(); ?></h2>

    <div class="block-call-to-action__text">
        <?php if($page->getCTAColumns() == 1): ?>
            <?php echo $page->getCTAColumn1(); ?>
        <?php elseif($page->getCTAColumns() == 2):?>
            <?php echo $page->getCTAColumn1(); ?>
            <?php echo $page->getCTAColumn2(); ?>
        <?php endif; ?>
    </div>

    <?php if($page->getCTAShowButton()): ?>
        <div class="block-call-to-action__buttons orange button center">
            <a href="<?php echo $page->getCTALink()['url'];?>" class="button button--secondary"><?php echo $page->getCTALink()['title'] ; ?></a>
        </div>
    <?php endif;?>

</div>

<?php

get_footer();