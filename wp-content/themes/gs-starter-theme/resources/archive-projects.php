<?php
$background = '';
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (get_post_type() === 'product') {
    global $wp_query;
    $query = new \PP\Collections\WPQueryProductCollections($wp_query);
} else {
    global $wp_query;
    $query = new \PP\Collections\WPQueryCollections($wp_query);
}

get_header();

$background_url = 'https://arcticnuvsvaag.no/wp-content/uploads/2021/11/DJI_0809-HDR-1.png';

if($background_url){
    $background = "background-image: url('".$background_url."')";
}

?>
    <div class="block-text layout6"  style="<?php echo $background; ?>" >
        <div class="block-text__row" >
            <h1 class="title">
               Alle prosjekter
            </h1>
            <div class="content">
                <p>Lorem pisiorum orem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo</p>

            </div>
        </div>
    </div>

    <div class="archive__header">
        <h1><?php echo get_the_archive_title(); ?></h1>
    </div>

    <?php pp_template_include('loops/' . get_post_type() . '/grid', [
        'query' => $query
    ]); ?>

    <?php $query->pagination()->display(); ?>


<?php

get_footer();