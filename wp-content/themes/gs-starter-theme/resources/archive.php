<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (get_post_type() === 'product') {
    global $wp_query;
    $query = new \PP\Collections\WPQueryProductCollections($wp_query);
} else {
    global $wp_query;
    $query = new \PP\Collections\WPQueryCollections($wp_query);
}

get_header();

?>

    <div class="archive__header">
        <h1><?php echo get_the_archive_title(); ?></h1>
    </div>

    <?php pp_template_include('loops/' . get_post_type() . '/grid', [
        'query' => $query
    ]); ?>

    <?php $query->pagination()->display(); ?>

<?php

get_footer();