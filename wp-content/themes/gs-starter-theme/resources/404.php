<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();

?>
<div class="page-404">
    <h1><?php _e('404', _THEME_DOMAIN_); ?>
        <span><?php _e('Page not found', _THEME_DOMAIN_); ?></span>
    </h1>
    <div class="button">
        <a href="<?php echo home_url(); ?>" class="button button--secondary"><?php _e('Tilbake til hjemmeside', _THEME_DOMAIN_); ?></a>
    </div>
</div>
<?php

get_footer();