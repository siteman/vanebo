<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (empty($query)) {
  return;
}

/** @var $query \PP\Collections\WPQueryCollections */

$products = $query->getPosts();

?>
<?php if (empty($products)): ?>
    <div class="loop__empty loop-products__empty">
        <?php _e('No posts to display', _THEME_DOMAIN_); ?>
    </div>
<?php else: ?>
    <div class="projects__grid">
        <div class="projects__grid--wrapper">
            <?php collect($products)->each(function(\PPTheme\Models\Posts\Projects $project) { ?>
                <div class="projects__grid--wrapper-single">
                    <?php pp_template_include('items/projects/grid', [
                        'project' => $project
                    ]); ?>
                </div>
            <?php }); ?>
        </div>
    </div>
<?php endif; ?>