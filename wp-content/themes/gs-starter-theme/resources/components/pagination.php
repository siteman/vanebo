<?php

/** @var $pager \PPTheme\Libraries\Paginator\IPager */
/** @var $range int */

$count = (int) $pager->getTotalPages();
$currentPage  = (int) $pager->getCurrentPage();
$ceil  = ceil($range / 2);

if ($count <= 1) {
    return false;
}

if (!$currentPage) {
    $currentPage = 1;
}

if ($count > $range) {
    if ($currentPage <= $range) {
        $min = 1;
        $max = $range + 1;
    } elseif ($currentPage >= ($count - $ceil)) {
        $min = $count - $range;
        $max = $count;
    } elseif ($currentPage >= $range && $currentPage < ($count - $ceil)) {
        $min = $currentPage - $ceil;
        $max = $currentPage + $ceil;
    }
} else {
    $min = 1;
    $max = $count;
}

?>
<nav role="navigation" aria-label="<?php _e('Pagination Navigation', _THEME_DOMAIN_); ?>" class="pagination">
    <?php if ($pager->getHasPrevious()): ?>
        <div class="pagination__previous">
            <a class="btn btn--primary" href="<?php echo $pager->getPageLink($currentPage - 1); ?>">
                <?php _e('Older', _THEME_DOMAIN_); ?>
            </a>
        </div>
    <?php endif; ?>
    <ul class="pagination__pages">
        <?php if ($min !== 1): ?>
            <li class="pagination__item">
                <a href="<?php echo $pager->getPageLink(1); ?>"
                   class="pagination__page-link"
                   aria-label="<?php printf(__('Go to page %d', _THEME_DOMAIN_), 1)?>"
                ><?php echo 1; ?></a>
            </li>
            <li class="pagination__page-separator">...</li>
        <?php endif; ?>
        <?php for ($page = $min; $page <= $max; $page++):
            $pageClasses = ['pagination__page-link'];

            if ($page === $currentPage) {
                $pageClasses[] = 'pagination__page-link--current';
            }
            ?>
            <li class="pagination__item">
                <a href="<?php echo $pager->getPageLink($page); ?>"
                   class="<?php echo implode(' ', $pageClasses); ?>"
                   <?php if ($page === $currentPage): ?>aria-current="true"<?php endif; ?>
                   aria-label="<?php printf(__('Go to page %d', _THEME_DOMAIN_), $page)?>"
                ><?php echo $page; ?></a>
            </li>
        <?php endfor; ?>
        <?php if ($max !== $count): ?>
            <li class="pagination__page-separator">...</li>
            <li class="pagination__item">
                <a href="<?php echo $pager->getPageLink($count); ?>"
                   data-page="<?php echo $page; ?>"
                   class="pagination__page-link"
                   aria-label="<?php printf(__('Go to page %d', _THEME_DOMAIN_), $count)?>"
                ><?php echo $count; ?></a>
            </li>
        <?php endif; ?>
    </ul>
    <?php if ($pager->getHasNext()): ?>
        <div class="pagination__next">
            <a class="btn btn--primary" href="<?php echo $pager->getPageLink($currentPage + 1); ?>">
                <?php _e('Newer', _THEME_DOMAIN_); ?>
            </a>
        </div>
    <?php endif; ?>
</nav>