<?php

/** @var $home_url string */
/** @var $logo \PP\Image */
/** @var $name string */
/** @var $email string */
/** @var $phone string */
/** @var $caption string */
/** @var $bgcolor string */
/** @var $col4 string */

if ( ! defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>
</main>
<footer id="site-footer" class="site-footer row" style="background-color:<?php echo $bgcolor ?>">
  <div class="site-footer__row">
    <div class="site-footer__row--left">
      <div class="site-footer__row--contact col-sm">
        <?php echo $col1; ?>
      </div>
      
      <div class="site-footer__row--nav">
        <h4>Snarveier</h4>
        <?php echo $nav; ?>
      </div>

      <div class="site-footer__row--logos">
        <div class="site-footer__row--logos-wrapper">
          <?php if(!empty($mester)) : ?>
            <img src="<?php echo $mester['url'] ?>">          
          <?php endif; ?>
          <?php if(!empty($sentral)) : ?>
            <img src="<?php echo $sentral['url'] ?>">          
          <?php endif; ?>
        </div>
      </div>
    </div>

    <div class="site-footer__row--right">
      <div class="site-footer__row--stall-horgen col-sm">
        <?php echo $col4; ?>

        <ul>
          <?php if(!empty($facebook)) : ?>
            <li><a href="<?php echo $facebook;?>" target="_blank"><span class="fab fa-facebook-square fa-2x icon"></span></a></li>
          <?php endif; ?>
        </ul>
      </div>
    </div>

  </div>
</footer>
<?php pp_template_include('partials/footer/base'); ?>