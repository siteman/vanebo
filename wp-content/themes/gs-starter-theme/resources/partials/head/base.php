<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no maximum-scale=1 minimum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="theme-color" content="#ffffff">

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/apple-icon-57x57.png'); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/apple-icon-60x60.png'); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/apple-icon-72x72.png'); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/apple-icon-76x76.png'); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/apple-icon-114x114.png'); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/apple-icon-120x120.png'); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/apple-icon-144x144.png'); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/apple-icon-152x152.png'); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/apple-icon-180x180.png'); ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/android-icon-192x192.png'); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/favicon-32x32.png'); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/favicon-96x96.png'); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/favicon-16x16.png'); ?>">
    <link rel="manifest" href="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon/manifest.json'); ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo \PP\Facades\Theme::baseUrl('assets/favicon//ms-icon-144x144.png'); ?>">
    <link rel='stylesheet' id='acf-input-font-awesome_library-css'  href='https://use.fontawesome.com/releases/v5.15.4/css/all.css?ver=5.8' media='all' />
    <meta name="theme-color" content="#ffffff">
    <meta name="google-site-verification" content="5P9BPcsYpiTzHOJJRzxXcTaQfXC0RZAg5I3MWPNsfNs" />
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Teko:wght@400;600&display=swap');
    </style>
    <?php wp_head(); ?>
    <?php echo $args['code_for_head']; ?>
</head>
<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebSite">
    <?php echo $args['code_for_body']; ?>
    <meta itemprop="url" content="<?php echo home_url(); ?>"/>
