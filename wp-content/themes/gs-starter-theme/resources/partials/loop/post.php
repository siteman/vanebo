<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (empty($query)) {
    return;
}

/** @var $query \PP\Collections\WPQueryCollections */

$products = $query->getPosts();

?>
<?php if (empty($products)): ?>
    <div class="loop__empty loop-products__empty">
        <?php _e('No posts to display', _THEME_DOMAIN_); ?>
    </div>
<?php else: ?>
    <div class="grid">
        <div class="grid__wrapper">
            <?php collect($products)->each(function(\PP\Models\Posts\Post $post) { ?>
                <?php pp_template_include('items/posts/grid', [
                        'post' => $post
                ]); ?>
            <?php }); ?>
        </div>
    </div>
<?php endif; ?>