<?php

/** @var $home_url string */
/** @var $logo \PP\Image */
/** @var $sitename string */
/** @var $wishlist_url string */
/** @var $search_url string */
/** @var $search_query string */
/** @var $nav string */
/** @var $code_for_head string */
/** @var $code_for_body string */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}
$args = [
	'code_for_head' => $code_for_head,
	'code_for_body' => $code_for_body,
];
get_template_part('partials/head/base', 'scripts', $args);

?>
<header class="site-header">
    <div class="site-header__wrapper">
        <div class="site-header__logo">
            <a href="<?php echo $home_url; ?>" class="site-header__logo" title="<?php _e('Home page', _THEME_DOMAIN_); ?>">
				<?php echo $logo instanceof \PP\Image ? $logo->display() : $sitename; ?>
            </a>
        </div>

        <div class="site-header__nav" >
            <button class="menu"><i class="fas fa-bars"></i></button>
			<?php echo $nav; ?>
        </div>
    </div>
</header>
<script>
$( "button.menu" ).click(function() {
    $(".site-header__nav").toggleClass( "mobile-active");
});
</script>
<main id="main-content" <?php post_class(); ?>>