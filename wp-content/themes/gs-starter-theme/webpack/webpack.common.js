const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const WpAssets = require('./lib/assets');
const CopyPlugin = require("copy-webpack-plugin");
const StylelintPlugin = require('stylelint-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const config = {
  paths: {
    src: path.resolve(__dirname, '../assets'),
    dist: path.resolve(__dirname, '../public')
  },
};

module.exports = env => {
  let fileNames = env.production === true ? '[name].[hash].[ext]' : '[name].[ext]';

  return {
    entry: {
      front: [
        './assets/front/js/app.ts',
        './assets/front/scss/app.scss',
      ],
      admin: [
        './assets/admin/js/app.ts',
        './assets/admin/scss/app.scss',
      ],
      blocks: [
        //'./assets/blocks/js/*.ts',
        './assets/blocks/scss/blocks.scss',
      ]
    },
    output: {
      path: config.paths.dist,
      filename: '[name].js'
    },
    plugins: [
      new ProgressBarPlugin(),
      new CopyPlugin({
        patterns: [
          {from: './assets/front/img', to: 'img'}
        ]
      }),
      new MiniCssExtractPlugin({
        filename: env.production === true ? '[id].[hash].css' : '[name].css'
      }),
      new StylelintPlugin(),
      new VueLoaderPlugin(),
      new WpAssets(),
      //new LiveReloadPlugin(),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
      })
    ],
    resolve: {
      extensions: ['.js', '.ts', '.tsx', '.vue'],
      alias: {
        'vue$': 'vue/dist/vue.esm.js'
      }
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          enforce: 'pre',
          exclude: /(node_modules|bower_components)/,
          use: [
            {
              loader: 'eslint-loader'
            },
          ]
        }, {
          test: /\.vue$/,
          exclude: /(node_modules|bower_components)/,
          use: 'vue-loader',
        }, {
          test: /\.(tsx?|jsx?)$/,
          exclude: /node_modules\/(?!(dom7|swiper)\/).*/,
          use: 'babel-loader',
        }, {
          test: /\.(ttf|eot|woff(2)?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          use: {
            loader: 'file-loader',
            options: {
              name: fileNames,
              outputPath: 'fonts/',
            },
          },
        }, {
          test: /\.(gif|png|jpe?g|svg)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: fileNames,
                outputPath: 'img/',
              },
            }, {
              loader: 'image-webpack-loader',
              options: {
                mozjpeg: {
                  enabled: true,
                  progressive: true,
                  quality: 70,
                },
                optipng: {
                  enabled: false,
                },
                pngquant: {
                  quality: '65-90',
                  speed: 4,
                },
                gifsicle: {
                  interlaced: false,
                },
              },
            },
          ],
        }, {
          test: /\.(sa|sc|c)ss$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
            }, {
              loader: 'css-loader',
            }, {
              loader: 'postcss-loader',
              options: {
                postcssOptions: {
                  plugins: [
                    [
                      'autoprefixer'
                    ]
                  ],
                }
              },
            }, {
              loader: 'sass-loader'
            }],
        },
      ],
    },
  }
};