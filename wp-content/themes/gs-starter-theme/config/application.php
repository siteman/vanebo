<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

return [

    /**
     * Theme name
     */
    'name' => 'gs-starter-theme',

    /**
     * Theme version
     */
    'version' => '1.0.0',

    /**
     * Author name
     */
    'author' => 'Godtsagt',

    /**
     * Theme uri
     */
    'theme_uri' => 'https://godtsagt.no/',

    /**
     * Text domain which will be available as const _THEME_DOMAIN_
     */
    'text_domain' => 'gs-starter-theme',

    /**
     * Theme slug
     */
    'slug' => 'gs-starter-theme',

    /**
     * Navs
     */
    'navs' => [
        'header_nav' => __('Header', 'gs-starter-theme'),
        'footer_nav' => __('Footer', 'gs-starter-theme'),
    ],

    /**
     * Custom providers
     */
    'providers' => [
        \PPTheme\Providers\RecaptchaServiceProvider::class,
        \PPTheme\Providers\AcfServiceProvider::class,
        \PPTheme\Providers\GalleryProvider::class,
    ],

    /**
     * List composers
     */
    'composers' => [
        \PPTheme\Composers\FooterComposer::class,
        \PPTheme\Composers\HeaderComposer::class,
        \PPTheme\Composers\ThemeComposer::class
    ]
];