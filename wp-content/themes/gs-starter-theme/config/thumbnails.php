<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

return [

    /**
     * Thumbnail sizes
     */
    'sizes' => [
        'img-1920x1080' => [
            'width' => 1920,
            'height' => 1080,
            'crop' => true
        ],
        'img-450x300' => [
            'width' => 450,
            'height' => 300,
            'crop' => true
        ],
        'img-1400x150' => [
            'width' => 1400,
            'height' => 150,
            'crop' => true
        ],
        'img-1250x500' => [
            'width' => 1250,
            'height' => 500,
            'crop' => true
        ],
        'img-300x300' => [
                'width' => 300,
                'height' => 300,
                'crop' => true
        ],
    ],

    /**
     * List of sizes for Picture's objects
     */
    'srcset' => [
        'lg' => '(min-width: 1200px)',
        'md' => '(min-width: 768px)',
        'sm' => \PP\Picture::DEFAULT_MEDIA
    ]
];