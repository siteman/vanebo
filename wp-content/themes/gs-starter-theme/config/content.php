<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

return [

    /**
     * List post types used in theme
     */
    'post_types' => [
//        \PPTheme\Models\Posts\Slider::class,
        \PPTheme\Models\Posts\Projects::class,
//        \PPTheme\Models\Posts\Employees::class,
    ],

    /**
     * List taxonomies used in theme
     */
    'taxonomies' => [
    ],

    'blocks' => [
        \PPTheme\Blocks\BlockText::class,
        \PPTheme\Blocks\BlockReferencesCircles::class,
        \PPTheme\Blocks\BlockButtons::class,
        \PPTheme\Blocks\BlockSteps::class,
        \PPTheme\Blocks\BlockSlider::class,
        \PPTheme\Blocks\BlockAccordion::class,
        \PPTheme\Blocks\BlockSingleFeatur::class,
        \PPTheme\Blocks\BlockPartnerLogos::class,
        \PPTheme\Blocks\BlockCallToAction::class,
        \PPTheme\Blocks\BlockShowPosts::class,
        \PPTheme\Blocks\BlockShowHeader::class,
        \PPTheme\Blocks\BlockSingleBlogPost::class,
        \PPTheme\Blocks\BlockFeatures::class,
        \PPTheme\Blocks\BlockTimeline::class,
		\PPTheme\Blocks\BlockMap::class,
		\PPTheme\Blocks\BlockShowEmployees::class,
		\PPTheme\Blocks\BlockNews::class,
        \PPTheme\Blocks\Block5050::class,
        \PPTheme\Blocks\BlockProjects::class,
        \PPTheme\Blocks\BlockTextWrapper::class,
    ],

    /**
     *
     */

    'shortcodes' => [
        //PPTheme\Shortcodes\ExampleShortcode::class
    ]
];