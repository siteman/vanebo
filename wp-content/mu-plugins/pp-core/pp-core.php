<?php
/**
 * Plugin Name: PP Core
 * Plugin URI: https://perfectlyproject.pl/
 * Description: Core plugin for themes and plugins for stable, easier and faster developers work.
 * Author: OnTheGoSystems
 * Author URI: http://www.perfectlyproject.pl/
 * Version: 1.0.0
 * Plugin Slug: pp-core
 *
 * @package PP
 */

/**
 * Load composer vendors
 */
if (!file_exists($composer = __DIR__.'/../../../vendor/autoload.php')) {
    die('We could not find the composer autoload, You have to run in the command line "composer install"');
} else {
    require_once $composer;
    require_once __DIR__.'/vendor/autoload.php';
}

\PP\Foundation\Application::load(__DIR__);