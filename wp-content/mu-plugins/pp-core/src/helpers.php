<?php

use Illuminate\Support\Arr;
use Illuminate\Contracts\Routing\ResponseFactory;
use PP\Foundation\Application;

if (!function_exists('esc_phone')) {
    /**
     * Remove special chars for phone
     *
     * @param $phone
     *
     * @return mixed
     */
    function esc_phone($phone)
    {
        return str_replace([' ', '-'], '', $phone);
    }
}

if (!function_exists('wp_get_collection_posts')) {
    /**
     * Return collection by wp query args
     *
     * @param $args
     *
     * @return \PP\Collections\WPQueryCollections
     */
    function wp_get_collection_posts($args): \PP\Collections\WPQueryCollections
    {
        return new \PP\Collections\WPQueryCollections(new WP_Query($args));
    }
}

if (!function_exists('pp_template_include')) {
    /**
     * Include template part with data
     *
     * @param $template
     * @param array $data
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    function pp_template_include($template, $data = [])
    {
        extract(Application::getInstance()->get('composer_manager')->resolve($template.'.php'));
        extract($data);

        include Application::getInstance()->get('theme')->resourcesPath($template.'.php');
    }
}

if (!function_exists('pp_response')) {
    /**
     * Return a new response from the application.
     *
     * @param  \Illuminate\View\View|string|array|null  $content
     * @param  int  $status
     * @param  array  $headers
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    function pp_response($content = '', $status = 200, array $headers = [])
    {
        $factory = Application::getInstance()->get(ResponseFactory::class);

        if (func_num_args() === 0) {
            return $factory;
        }

        return $factory->make($content, $status, $headers);
    }
}

if (!function_exists('plugin_template')) {
    /**
     * Include template part with data from plugin
     *
     * @param string $plugin
     * @param string $template
     * @param array $data
     */
    function plugin_template(string $plugin, string $template, array $data = [])
    {
        (new \PP\Template())->setPluginPath($plugin, $template)->setParams($data)->display();
    }
}

if (!function_exists('trim_text')) {
    /**
     * Trims text to a certain number of characters.
     *
     * @param $text
     * @param int $length
     * @param string $more
     * @return string
     */
    function trim_text($text, $length = 0, $more = '...')
    {
        $text = wp_strip_all_tags($text);

        if (mb_strlen($text) <= $length) {
            return $text;
        }

        $formatedTextCuts = [];
        $explodedText = explode(' ', $text);

        foreach ($explodedText as $text) {
            $formatedTextCuts[] = $text;
            $formatedText = implode(' ', $formatedTextCuts);

            if (mb_strlen($formatedText) >= $length) {
                break;
            }
        }

        return $formatedText . $more;
    }
}