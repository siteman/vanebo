<?php

namespace PP\Foundation;

abstract class Plugin extends ApplicationPart
{
    /**
     * Plugin constructor.
     *
     * @param \PP\Foundation\Application $app
     * @param string $path
     */
    public function __construct(Application $app, string $path)
    {
        parent::__construct($app, $path);
    }

    /**
     * Return the slug for the plugin manager
     *
     * @return string
     */
    abstract public function getSlug(): string;
}