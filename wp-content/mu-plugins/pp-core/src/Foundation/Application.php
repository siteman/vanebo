<?php

namespace PP\Foundation;

use Illuminate\Config\Repository;
use Illuminate\Container\Container;
use Illuminate\Events\EventServiceProvider;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Routing\RoutingServiceProvider;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Str;
use Illuminate\Translation\TranslationServiceProvider;
use Illuminate\Validation\ValidationServiceProvider;
use Illuminate\View\ViewServiceProvider;
use PP\Managers\PluginManager;
use PP\Providers\AcfProvider;
use PP\Providers\AssetsProvider;
use PP\Providers\CleanWpProvider;
use PP\Providers\ComposerThemeProvider;
use PP\Providers\ContentProvider;
use PP\Providers\DatabaseServiceProvider;
use PP\Providers\ShortcodeProvider;

class Application extends Container
{
    /**
     * The Perfectly plugin version.
     *
     * @var string
     */
    const VERSION = '1.0.0';

    /**
     * All of the registered service providers.
     *
     * @var \Illuminate\Support\ServiceProvider[]
     */
    protected $serviceProviders = [];

    /**
     * The names of the loaded service providers.
     *
     * @var array
     */
    protected $loadedProviders = [];

    /**
     * Application constructor.
     *
     * @param string $path
     */
    protected function __construct(string $path)
    {
        $this->setBasePath($path);
        $this->setDefaultPaths();
        $this->setDefaultUris();

        $this->loadHelpers();
        $this->registerBaseBindings();
        $this->registerBaseProviders();
        $this->registerCoreContainerAliases();
        $this->bootstrapFacades();
    }

    public function registerCoreContainerAliases()
    {
        foreach ([
             'app'                  => [self::class, \Illuminate\Contracts\Container\Container::class, \Illuminate\Contracts\Foundation\Application::class, \Psr\Container\ContainerInterface::class],
             'view'                 => [\Illuminate\View\Factory::class, \Illuminate\Contracts\View\Factory::class],
         ] as $key => $aliases) {
            foreach ($aliases as $alias) {
                $this->alias($key, $alias);
            }
        }
    }

    public function bootstrapFacades()
    {
        Facade::clearResolvedInstances();

        Facade::setFacadeApplication($this);
    }

    /**
     * Load helper functions
     */
    public function loadHelpers(): void
    {
        require_once __DIR__.'/../helpers.php';
    }

    /**
     * Create main instance
     *
     * @param string $path
     * @return \PP\Foundation\Application
     */
    public static function load(string $path): self
    {
        if (is_null(static::$instance)) {
            return new self($path);
        }
    }

    /**
     * Register default bindings
     */
    public function registerBaseBindings(): void
    {
        static::setInstance($this);

        $this->instance('config', new Repository([
            'app' => [
                'locale' => 'nb',
                'fallback_locale' => 'nb',
            ],
            'view' => [
                'paths' => []
            ]
        ]));
    }

    /**
     * Register theme
     *
     * @param string $theme
     */
    public function registerTheme(string $theme, string $path): void
    {
        $theme = new $theme($this, $path);
        $theme->boot();

        $this->instance('theme', $theme);
    }

    /**
     * Register a plugin
     *
     * @param string $plugin
     * @param string $path
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function registerPlugin(string $plugin, string $path): void
    {
        $this->get('plugins')->register($plugin, $path);
    }

    /**
     * Register base providers
     */
    public function registerBaseProviders(): void
    {
        $this->instance('plugins', new PluginManager($this));
        $this->instance('request', \Illuminate\Http\Request::capture());
        $this->instance('env', WP_DEBUG === false ? 'production' : 'develop');

        $this->singleton('files', function () {
            return new Filesystem();
        });

        $this->register(EventServiceProvider::class);
        $this->register(DatabaseServiceProvider::class);
        $this->register(AssetsProvider::class);
        $this->register(CleanWpProvider::class);
        $this->register(AcfProvider::class);
        $this->register(ContentProvider::class);
        $this->register(ComposerThemeProvider::class);
        $this->register(ViewServiceProvider::class);
        $this->register(RoutingServiceProvider::class);
        $this->register(ShortcodeProvider::class);
        $this->register(TranslationServiceProvider::class);
        $this->register(ValidationServiceProvider::class);
    }

    /**
     * Register a service provider with the application.
     *
     * @param  \Illuminate\Support\ServiceProvider|string  $provider
     * @param  bool   $force
     * @return \Illuminate\Support\ServiceProvider
     */
    public function register($provider, $force = false)
    {
        if (($registered = $this->getProvider($provider)) && ! $force) {
            return $registered;
        }

        // If the given "provider" is a string, we will resolve it, passing in the
        // application instance automatically for the developer. This is simply
        // a more convenient way of specifying your service provider classes.
        if (is_string($provider)) {
            $provider = $this->resolveProvider($provider);
        }

        $provider->register();

        // If there are bindings / singletons set as properties on the provider we
        // will spin through them and register them with the application, which
        // serves as a convenience layer while registering a lot of bindings.
        if (property_exists($provider, 'bindings')) {
            foreach ($provider->bindings as $key => $value) {
                $this->bind($key, $value);
            }
        }

        if (property_exists($provider, 'singletons')) {
            foreach ($provider->singletons as $key => $value) {
                $this->singleton($key, $value);
            }
        }

        $this->markAsRegistered($provider);
    }

    /**
     * Mark the given provider as registered.
     *
     * @param  \Illuminate\Support\ServiceProvider  $provider
     * @return void
     */
    protected function markAsRegistered($provider)
    {
        $this->serviceProviders[] = $provider;

        $this->loadedProviders[get_class($provider)] = true;
    }

    /**
     * Get the registered service provider instance if it exists.
     *
     * @param  \Illuminate\Support\ServiceProvider|string  $provider
     * @return \Illuminate\Support\ServiceProvider|null
     */
    public function getProvider($provider)
    {
        return array_values($this->getProviders($provider))[0] ?? null;
    }

    /**
     * Get the registered service provider instances if any exist.
     *
     * @param  \Illuminate\Support\ServiceProvider|string  $provider
     * @return array
     */
    public function getProviders($provider)
    {
        $name = is_string($provider) ? $provider : get_class($provider);

        return Arr::where($this->serviceProviders, function ($value) use ($name) {
            return $value instanceof $name;
        });
    }

    /**
     * Boot the given service provider.
     *
     * @param  \Illuminate\Support\ServiceProvider  $provider
     * @return mixed
     */
    protected function bootProvider(ServiceProvider $provider)
    {
        if (method_exists($provider, 'boot')) {
            return $this->call([$provider, 'boot']);
        }
    }

    /**
     * Resolve a service provider instance from the class name.
     *
     * @param  string  $provider
     * @return \Illuminate\Support\ServiceProvider
     */
    public function resolveProvider($provider)
    {
        return new $provider($this);
    }

    /**
     * Set default paths paths
     *
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    protected function setDefaultPaths()
    {
        $this->instance('path.lang', $this->basePath('resources/lang'));

        $this->setCommonPath(WP_CONTENT_DIR.DIRECTORY_SEPARATOR.'pp-common')
            ->setThemePath(get_template_directory())
            ->setDatabasePath($this->commonPath('db'));
    }

    protected function setDefaultUris()
    {
        /** @todo */
    }

    /**
     * Set base path
     *
     * @param string $path
     * @return $this
     */
    public function setBasePath(string $path) : Application
    {
        $this->instance('path.base', $path);

        return $this;
    }

    /**
     * Get the path to the base plugin directory.
     *
     * @param string $path
     * @return string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function basePath(string $path = '') : string
    {
        return $this->get('path.base').DIRECTORY_SEPARATOR.$path;
    }

    /**
     * Set wp content directory path
     *
     * @param string $path
     * @return \PP\Foundation\Application
     */
    public function setCommonPath(string $path): Application
    {
        $this->instance('path.common', $path);

        return $this;
    }

    /**
     * Get the path to the base wp content directory.
     *
     * @param string $path
     * @return string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function commonPath(string $path = '') : string
    {
        return $this->get('path.common').DIRECTORY_SEPARATOR.$path;
    }

    /**
     * Set wp content directory path
     *
     * @param string $path
     * @return \PP\Foundation\Application
     */
    public function setThemePath(string $path) : Application
    {
        $this->instance('path.theme', $path);

        return $this;
    }

    /**
     * Get the path to the theme directory.
     *
     * @param string $path
     * @return string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function themePath(string $path = '') : string
    {
        return $this->get('path.theme').DIRECTORY_SEPARATOR.$path;
    }

    /**
     * Set the database path.
     *
     * @param string $path
     * @return \PP\Foundation\Application
     */
    public function setDatabasePath(string $path = '') : Application
    {
        $this->instance('path.database', $path);

        return $this;
    }

    /**
     * Get the path to the theme directory.
     *
     * @param string $path
     * @return string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function databasePath(string $path = '') : string
    {
        return $this->get('path.database').DIRECTORY_SEPARATOR.$path;
    }


    /**
     * Get or check the current application environment.
     *
     * @param  string|array  $environments
     * @return string|bool
     */
    public function environment(...$environments)
    {
        if (count($environments) > 0) {
            $patterns = is_array($environments[0]) ? $environments[0] : $environments;

            return Str::is($patterns, $this['env']);
        }

        return $this['env'];
    }
}