<?php

namespace PP\Foundation;

use Illuminate\Config\Repository;
use Illuminate\Container\Container;
use Illuminate\Support\Arr;
use Symfony\Component\Finder\Finder;

abstract class ApplicationPart extends Container
{
    /**
     * Instance to main application
     *
     * @var \PP\Foundation\Application
     */
    protected $app;

    /**
     * Theme constructor.
     *
     * @param \PP\Foundation\Application $app
     */
    public function __construct(Application $app, string $path)
    {
        $this->app = $app;
        $this->setBasePath($path);
    }

    public function boot()
    {
        $this->registerBaseBindings();
        $this->loadConfiguration();

        $this->init();
        $this->registerNavs();
        $this->registerProviders();
        $this->registerControllers();
        $this->registerContent();
    }

    /**
     * Method excluded after registered base bindings.
     */
    protected function init()
    {
        
    }

    /**
     * Register theme providers
     *
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function registerProviders(): void
    {
        collect($this->get('config')->get('application.providers'))->each(function(string $provider) {
            $this->app->register($provider);
        });
    }

    /**
     * Register theme providers
     *
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function registerControllers(): void
    {
        collect($this->get('config')->get('application.controllers'))->each(function(string $controller) {
            $this->app->get('controllers')->register($controller);
        });
    }

    /**
     * Register base bindings
     */
    public function registerBaseBindings(): void
    {
        $this->instance('config', new Repository());
    }

    /**
     * Register nav menus from config
     */
    public function registerNavs(): void
    {
        $menus = $this->get('config')->get('application.navs');

        if (!empty($menus)) {
            register_nav_menus($menus);
        }
    }

    /**
     * Register the content
     *
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function registerContent(): void
    {
        collect($this->get('config')->get('content.post_types'))->each(function($postType) {
            $this->app->get('posts')->register($postType);
        });

        collect($this->get('config')->get('content.taxonomies'))->each(function($taxonomy) {
            $this->app->get('taxonomies')->register($taxonomy);
        });

        collect($this->get('config')->get('content.blocks'))->each(function($block) {
            $this->app->get('blocks')->register($block);
        });

        collect($this->get('config')->get('content.shortcodes'))->each(function($shortcode) {
            $this->app->get('shortcodes')->register($shortcode);
        });

        collect($this->get('config')->get('routes.pages'))->each(function($page, $name) {
            $this->app->get('controllers')->registerPage($name, $page['path'], $page['callback']);
        });

        collect($this->get('config')->get('routes.ajax'))->each(function($page) {
            $this->app->get('controllers')->registerAjax($page['action'], $page['callback']);
        });
    }

    /**
     * Set base path
     *
     * @param string $path
     * @return $this
     */
    public function setBasePath(string $path): self
    {
        $this->instance('path.base', $path);

        return $this;
    }

    /**
     * Get the path to the base plugin directory.
     *
     * @param string $path
     * @return string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function basePath(string $path = '') : string
    {
        return $this->get('path.base').DIRECTORY_SEPARATOR.$path;
    }

    /**
     * Get the path to the config directory
     *
     * @param string $path
     * @return string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function configPath(string $path = '') : string
    {
        return $this->get('path.base').DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.$path;
    }

    /**
     * Get the path to the resources directory
     *
     * @param string $path
     * @return string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function resourcesPath(string $path = '') : string
    {
        return $this->get('path.base').DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.$path;
    }

    /**
     * Get the path to the assets directory
     *
     * @param string $path
     * @return string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function assetsPath(string $path = '') : string
    {
        return $this->get('path.base').DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.$path;
    }

    /**
     * Get the path to the public directory
     *
     * @param string $path
     * @return string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function publicPath(string $path = '') : string
    {
        return $this->get('path.base').DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$path;
    }

    /**
     * Load configuration
     */
    public function loadConfiguration()
    {
        foreach (Finder::create()->files()->name('*.php')->in($this->configPath()) as $file) {
            $this->get('config')->set(basename($file->getRealPath(), '.php'), require $file->getRealPath());
        }
    }
}