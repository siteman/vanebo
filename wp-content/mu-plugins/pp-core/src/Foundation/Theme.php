<?php

namespace PP\Foundation;

use Illuminate\Support\Arr;
use Symfony\Component\Finder\Finder;

abstract class Theme extends ApplicationPart
{
    /**
     * Method excluded after registered base bindings.
     *
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function init()
    {
        $this->registerConst();
        $this->registerAssets();
        $this->registerComposers();

        add_action('after_setup_theme', [$this, 'registerSupports'], 10);
    }

    public function boot()
    {
        parent::boot();

        collect($this->get('config')->get('thumbnails.sizes'))->each(function($size, $key) {
            $this->app->get('assets')->registerImageSize($key, $size['width'], $size['height'], $size['crop']);
        });
    }

    public abstract function registerSupports(): void;

    /**
     * Register composers
     *
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function registerComposers(): void
    {
        collect($this->get('config')->get('application.composers'))->each(function($composer) {
            $this->app->get('composer_manager')->register($composer);
        });
    }

    /**
     * Register theme assests
     *
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function registerAssets(): void
    {
        $assetsFile = $this->app->commonPath('theme-webpack-assets.php');

        if (!file_exists($assetsFile)) {
            return;
        }

        $assets = include $assetsFile;

        collect(Arr::get($assets, 'styles'))->each(function($style) {
            $this->app->get('assets')->registerStyle($style);
        });

        collect(Arr::get($assets, 'scripts'))->each(function($script) {
            $this->app->get('assets')->registerScript($script);
        });
    }

    /**
     * Register general const
     */
    public function registerConst(): void
    {
        define('_THEME_DOMAIN_', $this->app->get('config')->get('application.text_domain'));
    }

    /**
     * Return base url
     *
     * @param string $path
     * @return string
     */
    public function baseUrl(string $path)
    {
        return rtrim(dirname(get_stylesheet_directory_uri()), '\/').'/'.$path;
    }

    /**
     * Return public url
     */
    public function publicUrl(string $path)
    {
        return $this->baseUrl('public/'.$path);
    }
}