<?php

namespace PP\Libs\Breadcrumbs;

class Item
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var bool
     */
    public $active = false;

    /**
     * @var string
     */
    public $link = '';

    /**
     * @var int
     */
    public $position = 1;

    /**
     * @var array
     */
    public $classes = [];

    public $hide_name = false;

    public function render()
    {
        $html = '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">';

        if ($this->active === false) {
            $html .= '<a itemprop="item" href="'.$this->link.'" title="'.$this->name.'">';
        }

        $html .= '<span itemprop="name" content="'.$this->name.'" class="'.implode(' ', $this->classes).'">'.($this->hide_name === false ? $this->name : "").'</span>';

        if ($this->active === false) {
            $html .= '</a>';
        }

        $html .= '<meta itemprop="position" content="'.$this->position.'" />';


        return $html;
    }
}