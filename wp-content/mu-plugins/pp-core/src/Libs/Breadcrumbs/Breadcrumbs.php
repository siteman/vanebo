<?php

namespace PP\Libs\Breadcrumbs;

use Illuminate\Support\Arr;
use PP\Libs\Breadcrumbs\Handlers\FrontPageHandler;
use PP\Libs\Breadcrumbs\Handlers\HomeHandler;
use PP\Libs\Breadcrumbs\Handlers\PageHandler;
use PP\Libs\Breadcrumbs\Handlers\PostHandler;
use PP\Libs\Breadcrumbs\Handlers\ProductCatHandler;
use PP\Libs\Breadcrumbs\Handlers\ProductHandler;
use PP\Libs\Breadcrumbs\Handlers\SearchHandler;

class Breadcrumbs
{
    /**
     * @var \PP\Libs\Breadcrumbs\Item[]
     */
    private $items = [];

    /**
     * @var array
     */
    private $handlers = [];

    /**
     * @var bool
     */
    private $resolved = false;

    /**
     * @var array
     */
    private $currentObject = [];

    /**
     * Breadcrumbs constructor.
     */
    public function __construct()
    {
        $this->handlers = [
            ProductHandler::class,
            ProductCatHandler::class,
            FrontPageHandler::class,
            HomeHandler::class,
            SearchHandler::class,
            PageHandler::class,
            PostHandler::class,
        ];
    }

    /**
     * Resolve handlers
     */
    public function resolveGlobal(): void
    {
        if ($this->resolved) {
            return;
        }

        foreach ($this->handlers as $handler) {
            $resolvedHandler = new $handler($this);
            if ($resolvedHandler->isGlobalHandling()) {
                $resolvedHandler->handleGlobal();
                $this->resolveObjects();
                break;
            }
        }

        $this->currentObject = null;
        $this->resolved = true;
    }

    /**
     *
     */
    private function resolveObjects()
    {
        foreach ($this->handlers as $handler) {
            $resolvedHandler = new $handler($this);
            $resolvedHandler->setObject(Arr::get($this->currentObject, 0, ));
            $resolvedHandler->setParams(Arr::get($this->currentObject, 1, []));

            if ($resolvedHandler->isObjectHandling()) {
                $resolvedHandler->handleObject();
                $this->resolveObjects();
                break;
            }
        }
    }

    /**
     * Add item
     *
     * @param \PP\Libs\Breadcrumbs\Item $item
     */
    public function add(Item $item)
    {
        $this->items[] = $item;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $this->resolveGlobal();

        $html = '<ol itemscope itemtype="https://schema.org/BreadcrumbList" class="breadcrumbs">';

        $counter = 1;
        foreach (array_reverse($this->items) as $item) {
            $item->position = $counter;
            $html .= $item->render();

            $counter++;
        }

        $html .= '</ol>';

        return $html;
    }

    public function display(): void
    {
        echo $this->render();
    }

    /**
     * @param mixed $currentObject
     * @param array $args
     * @return Breadcrumbs
     */
    public function setCurrentObject($currentObject, array $args = []): self
    {
        $this->currentObject = [
            $currentObject,
            $args
        ];

        return $this;
    }
}