<?php

namespace PP\Libs\Breadcrumbs;

abstract class AbstractHandler
{
    /**
     * @var mixed
     */
    private $object = null;

    /**
     * @var array
     */
    private $params = [];

    /**
     * @var \PP\Libs\Breadcrumbs\Breadcrumbs
     */
    protected $breadcrumbs;

    /**
     * AbstractHandler constructor.
     *
     * @param \PP\Libs\Breadcrumbs\Breadcrumbs $breadcrumbs
     */
    public function __construct(Breadcrumbs $breadcrumbs)
    {
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * Add item to breadcrumbs
     *
     * @param array $params
     */
    public function add(array $params)
    {
        $item = new Item();

        foreach ($params as $key => $value) {
            $item->{$key} = $value;
        }

        $this->breadcrumbs->add($item);
    }

    /**
     * Set object
     *
     * @param $object
     * @return \PP\Libs\Breadcrumbs\AbstractHandler
     */
    public function setObject($object): self
    {
        $this->object = $object;
        return $this;
    }

    /**
     * Set object
     *
     * @param $args
     * @return \PP\Libs\Breadcrumbs\AbstractHandler
     */
    public function setParams(array $args = []): self
    {
        $this->params = $args;
        return $this;
    }

    /**
     * Return object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Return param
     *
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param $object
     * @param array $args
     */
    public function setCurrentObject($object, array $args = [])
    {
        $this->breadcrumbs->setCurrentObject($object, $args);
    }

    /**
     * Function run every
     *
     * @return void
     */
    abstract public function handleGlobal(): void;

    /**
     * Function run every
     *
     * @return void
     */
    abstract public function handleObject(): void;

    /**
     * @return mixed
     */
    abstract public function isGlobalHandling(): bool;

    /**
     * @return bool
     */
    abstract public function isObjectHandling(): bool;
}