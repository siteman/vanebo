<?php

namespace PP\Libs\Breadcrumbs\Handlers;

use PP\Libs\Breadcrumbs\AbstractHandler;

class SearchHandler extends AbstractHandler
{
    const TYPE = 'is_search';

    /**
     * Function run every
     *
     * @return void
     */
    public function handle(): void
    {
        $this->add([
            'active' => true,
            'name' => sprintf(__('Søkeresultater til: %s'), get_search_query()),
            'link' => get_search_link(get_search_query())
        ]);

        $this->setCurrentObject(FrontPageHandler::TYPE);
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleGlobal(): void
    {
        $this->handle();
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleObject(): void
    {
        $this->handle();
    }

    /**
     * @return mixed
     */
    public function isGlobalHandling(): bool
    {
        return is_search();
    }

    /**
     * @return bool
     */
    public function isObjectHandling(): bool
    {
        return self::TYPE === $this->getObject();
    }
}