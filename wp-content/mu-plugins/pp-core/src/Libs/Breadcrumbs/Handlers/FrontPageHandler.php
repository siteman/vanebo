<?php

namespace PP\Libs\Breadcrumbs\Handlers;

use PP\Libs\Breadcrumbs\AbstractHandler;
use PP\Models\Posts\Page;

class FrontPageHandler extends AbstractHandler
{
    const TYPE = 'is_front_page';

    /**
     * Function run every
     *
     * @return void
     */
    public function handle(): void
    {
        $frontPage = new Page(get_option('page_on_front'));

        $this->add([
            'classes' => ['i--home'],
            'hide_name' => true,
            'name' => $frontPage->getTitle(),
            'link' => $frontPage->getPermalink()
        ]);

        $this->setCurrentObject(null);
    }

    /**
     * @return mixed
     */
    public function isGlobalHandling(): bool
    {
        return is_front_page();
    }

    /**
     * @return bool
     */
    public function isObjectHandling(): bool
    {
        return self::TYPE === $this->getObject();
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleObject(): void
    {
        $this->handle();
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleGlobal(): void
    {
        $this->handle();
    }
}