<?php

namespace PP\Libs\Breadcrumbs\Handlers;

use PP\Libs\Breadcrumbs\AbstractHandler;
use PP\Models\Posts\Page;

class ArchiveHandler extends AbstractHandler
{
    const TYPE = 'is_archive';

    /**
     * Function run every
     *
     * @return void
     */
    public function handle(): void
    {

    }

    /**
     * @return bool
     */
    public function isGlobalHandling(): bool
    {
        return is_archive();
    }

    /**
     * @return bool
     */
    public function isObjectHandling(): bool
    {
        return self::TYPE === $this->getObject();
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleGlobal(): void
    {
        $this->handle();
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleObject(): void
    {
        $this->handle();
    }
}