<?php

namespace PP\Libs\Breadcrumbs\Handlers;

use PP\Libs\Breadcrumbs\AbstractHandler;
use PP\Models\Posts\Page;

class HomeHandler extends AbstractHandler
{
    const TYPE = 'is_home';

    /**
     * Function run every
     *
     * @return void
     */
    public function handle(): void
    {
        $frontPage = new Page(get_option('page_for_posts'));

        $this->add([
            'name' => $frontPage->getTitle(),
            'link' => $frontPage->getPermalink()
        ]);

        $this->setCurrentObject(FrontPageHandler::TYPE);
    }

    /**
     * @return mixed
     */
    public function isGlobalHandling(): bool
    {
        return is_home();
    }

    /**
     * @return bool
     */
    public function isObjectHandling(): bool
    {
        return self::TYPE === $this->getObject();
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleObject(): void
    {
        $this->handle();
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleGlobal(): void
    {
        $this->handle();
    }
}