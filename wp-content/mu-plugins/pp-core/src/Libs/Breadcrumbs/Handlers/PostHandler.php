<?php

namespace PP\Libs\Breadcrumbs\Handlers;

use PP\Libs\Breadcrumbs\AbstractHandler;
use PP\Models\Posts\Page;
use PP\Models\Posts\Post;

class PostHandler extends AbstractHandler
{
    /**
     * Function run every
     *
     * @return void
     */
    public function handle(): void
    {
        $page = $this->getObject();

        $this->add([
            'active' => $this->getObject()->getId() === get_the_ID(),
            'name' => $this->getObject()->getTitle(),
            'link' => $this->getObject()->getPermalink()
        ]);

        $parent = $page->getParent();

        if (!empty($parent)) {
            $this->setCurrentObject($parent);
        } else {
            $this->setCurrentObject(FrontPageHandler::TYPE);
        }
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleGlobal(): void
    {
        $this->setObject(new Post(get_the_ID()));
        $this->handle();
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleObject(): void
    {
        $this->handle();
    }

    /**
     * @return mixed
     */
    public function isGlobalHandling(): bool
    {
        return is_single();
    }

    /**
     * @return bool
     */
    public function isObjectHandling(): bool
    {
        return $this->getObject() instanceof Post;
    }
}