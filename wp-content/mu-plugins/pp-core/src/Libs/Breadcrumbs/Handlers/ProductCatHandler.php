<?php

namespace PP\Libs\Breadcrumbs\Handlers;

use PP\Libs\Breadcrumbs\AbstractHandler;
use PPTheme\Models\Taxonomies\ProductCat;

class ProductCatHandler extends AbstractHandler
{
    /**
     * Function run every
     *
     * @return void
     */
    public function handle(): void
    {
        $category = $this->getObject();

        $this->add([
            'active' => $this->getObject()->getId() === get_queried_object()->term_id,
            'name' => $this->getObject()->getTitle(),
            'link' => $this->getObject()->getLink()
        ]);

        $parent = $category->getParent();

        if (!empty($parent)) {
            $this->setCurrentObject($parent);
        } else {
            $this->setCurrentObject(FrontPageHandler::TYPE);
        }
    }

    /**
     * @return mixed
     */
    public function isGlobalHandling(): bool
    {
        return is_tax('product_cat');
    }

    /**
     * @return bool
     */
    public function isObjectHandling(): bool
    {
        return $this->getObject() instanceof ProductCat;
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleGlobal(): void
    {
        $this->setObject(new ProductCat(get_queried_object()->term_id));
        $this->handle();
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleObject(): void
    {
        $this->handle();
    }
}