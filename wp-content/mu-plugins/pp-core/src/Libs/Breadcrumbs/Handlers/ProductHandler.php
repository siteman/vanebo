<?php

namespace PP\Libs\Breadcrumbs\Handlers;

use PP\Libs\Breadcrumbs\AbstractHandler;
use PP\Models\Posts\Product;
use Illuminate\Support\Arr;

class ProductHandler extends AbstractHandler
{
    /**
     * Function run every
     *
     * @return void
     */
    public function handle(): void
    {
        $this->add([
            'active' => $this->getObject()->getId() === get_the_ID(),
            'name' => $this->getObject()->getTitle(),
            'link' => $this->getObject()->getPermalink()
        ]);

        $categories = $this->getObject()->getCategories([
            'orderby' => 'parent',
            'order'   => 'DESC',
        ]);

        $category = Arr::first($categories);

        if (!empty($category)) {
            $this->setCurrentObject($category);
        } else {
            $this->setCurrentObject(null);
        }
    }

    /**
     * @return mixed
     */
    public function isGlobalHandling(): bool
    {
        return is_singular('product');
    }

    /**
     * @return bool
     */
    public function isObjectHandling(): bool
    {
        return $this->getObject() instanceof Product;
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleGlobal(): void
    {
        $this->setObject(new Product(get_the_ID()));
        $this->handle();
    }

    /**
     * Function run every
     *
     * @return void
     */
    public function handleObject(): void
    {

        $this->handle();
    }
}