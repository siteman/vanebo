<?php

namespace PP;

use Illuminate\Support\Arr;

class TaxonomyManager
{

    /**
     * Array of registered post types
     *
     * @var array
     */
    private $registeredTaxonomies = [];

    /**
     * Array of post type to register
     *
     * @var array
     */
    private $taxonomies = [];

    public function __construct()
    {

        add_action('init', [$this, 'hookActionInit'], 10, 0);
    }

    /**
     * Register post type
     *
     * @param string $taxonomy
     */
    public function register(string $taxonomy): void
    {

        $this->taxonomies[] = $taxonomy;
    }

    /**
     * Return class name by post type
     *
     * @param string $taxonomy
     *
     * @return string|null
     */
    public function get(string $taxonomy): ?string
    {

        return Arr::get($this->registeredTaxonomies, $taxonomy, null);
    }

    /**
     * Factor by slug
     *
     * @param $slug
     *
     * @return mixed|null
     * @throws \Exception
     */
    //public function factoryBySlug($slug)
    //{
    //
    //    $term = \get_term_by('slug', $slug, static::getTaxonomy());
    //
    //    if (empty($term)) {
    //        return null;
    //    }
    //
    //    return self::factory(static::getTaxonomy(), $term->term_id);
    //}

    /**
     * Create instance to the post type class
     *
     * @param $taxonomy
     * @param $id
     *
     * @return mixed
     * @throws \Exception
     */
    public function factory($taxonomy, $id)
    {

        $class = Arr::get($this->registeredTaxonomies, $taxonomy, null);

        if ($class === null) {
            throw new \Exception('Term not exists');
        }

        return new $class($id);
    }

    /**
     * Register taxonomy
     */
    public function hookActionInit()
    {

        $manager = $this;

        collect($this->taxonomies)->each(function ($taxonomy) use ($manager) {

            if ($taxonomy::$needsRegister) {
                register_taxonomy($taxonomy::getTaxonomy(), $taxonomy::getPostTypes(), $taxonomy::getArgs());
            }

            $manager->registeredTaxonomies[$taxonomy::getTaxonomy()] = $taxonomy;
        });
    }
}