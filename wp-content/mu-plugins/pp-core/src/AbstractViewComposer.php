<?php

namespace PP;

use PP\Foundation\Application;

abstract class AbstractViewComposer
{
    private $template = null;

    /**
     * Return list of views where composer should be registered
     *
     * @return array
     */
    abstract public function getViews(): array;

    /**
     * Return list of variables provides to template
     *
     * @return array
     */
    abstract public function with(): array;

    /**
     * Return current view class
     */
    public function getView(): string
    {
        return $this->template;
    }

    /**
     * Set composer current view
     *
     * @param string $template
     * @return \PP\AbstractViewComposer
     */
    public function setView(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Return list of variables adding to js languages
     *
     * @return array
     */
    public function withJsLanguages(): array
    {
        return [];
    }

    /**
     * Return list of variables adding to js variables
     *
     * @return array
     */
    public function withJsVariables(): array
    {
        return [];
    }

    public function boot(): void
    {

    }

    public function resolve()
    {
        $this->boot();
        $this->setJsLanguages($this->withJsLanguages());
        $this->setJsVariables($this->withJsVariables());
    }

    /**
     * Adding javascript params
     *
     * @param array $params
     */
    private function setJsLanguages(array $params)
    {

        Application::getInstance()->get('assets')->addJsTranslation($params);
    }

    /**
     * Adding javascript params
     *
     * @param array $params
     */
    private function setJsVariables(array $params)
    {

        Application::getInstance()->get('assets')->addJsData($params);
    }
}