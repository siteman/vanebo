<?php

namespace PP\Database;

use Illuminate\Database\Query\Builder;

class Processor extends \Illuminate\Database\Query\Processors\Processor
{
    public function processInsertGetId(Builder $query, $sql, $values, $sequence = null)
    {
        $query->getConnection()->insert($sql, $values);

        $id = $query->getConnection()->getDb()->insert_id;

        return is_numeric($id) ? (int) $id : $id;
    }
}