<?php

namespace PP\Database;

use Illuminate\Database\ConnectionResolverInterface;

class DatabaseManager implements ConnectionResolverInterface
{
    /**
     * Get a database connection instance.
     *
     * @param  string|null $name
     * @return \Illuminate\Database\ConnectionInterface
     */
    public function connection($name = null)
    {
        return new Connection();
    }

    /**
     * Get the default connection name.
     *
     * @return string
     */
    public function getDefaultConnection()
    {
        // TODO: Implement getDefaultConnection() method.
    }

    /**
     * Set the default connection name.
     *
     * @param  string $name
     * @return void
     */
    public function setDefaultConnection($name)
    {
        // TODO: Implement setDefaultConnection() method.
    }

    public function getPrefixedTable(string $table): string
    {
        return $this->connection()->getTablePrefix() . $table;
    }

    /**
     * Dynamically pass methods to the default connection.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->connection()->$method(...$parameters);
    }
}