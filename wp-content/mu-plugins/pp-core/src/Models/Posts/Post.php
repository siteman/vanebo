<?php

namespace PP\Models\Posts;

use PP\Image;
use PP\Models\AbstractPostType;
use PP\Picture;

class Post extends AbstractPostType
{
    public static $needsRegister = false;

    /**
     * Return slug for post type
     *
     * @return string
     */
    public static function getPostType(): string
    {
        return 'post';
    }

    /**
     * Have to return post type args
     *
     * List of allowed arguments: https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return array
     */
    public static function getArgs(): array
    {
        return [];
    }

    public function getFeaturePicture($size = 'post-520x400'): Picture
    {
        return new Picture([
            Picture::DEFAULT_MEDIA => new Image($this->getThumbnailId(), $size)
        ]);
    }
}