<?php

namespace PP\Models\Posts;

use Illuminate\Support\Arr;
use PP\Collections\AbstractWPCollections;
use PP\Collections\WPQueryProductCollections;
use PP\Image;
use PP\Models\AbstractPostType;
use PP\Models\Taxonomies\ProductAttributeValue;
use PP\Picture;
use PP\Models\Taxonomies\ProductCategory;

class Product extends AbstractPostType
{
    public static $needsRegister = false;

    /**
     * @var \WC_Product
     */
    protected $product = null;

    /**
     * Product constructor.
     *
     * @param int $id
     * @throws \Exception
     */
    public function __construct($id = null)
    {
        parent::__construct($id);

        if ($this->getId()) {
            $this->setProduct(wc_get_product($id));
        }
    }

    public function setProduct(\WC_Product $product): void
    {
        $this->product = $product;
    }

    /**
     * Return slug for post type
     *
     * @return string
     */
    public static function getPostType(): string
    {

        return 'product';
    }

    /**
     * Have to return post type args
     *
     * List of allowed arguments: https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return array
     */
    public static function getArgs(): array
    {
        return [];
    }

    public function getPrice()
    {
        return $this->product->get_price();
    }

    public function getPriceHTML()
    {
        return $this->product->get_price_html();
    }

    public function getSku()
    {
        return $this->product->get_sku();
    }

    public function getRatingCount()
    {
        return $this->product->get_rating_count();
    }

    public function getAverageRating()
    {
        return $this->product->get_average_rating();
    }

    public function getProductType()
    {
        return $this->product->get_type();
    }

    public function isInStock()
    {
        return $this->product->is_in_stock();
    }

    public function getFeaturePicture($size = 'product-360x260'): Picture
    {
        return new Picture([
            Picture::DEFAULT_MEDIA => new Image($this->getThumbnailId(), $size)
        ]);
    }

    public function getGalleryPictures(string $size = 'product-360x260'): array
    {
        $ids = $this->product->get_gallery_image_ids();
        array_unshift($ids, $this->product->get_image_id());

        return array_map(function($id) use ($size) {
            return new Picture([
                Picture::DEFAULT_MEDIA => new Image($id, $size)
            ]);
        }, array_filter($ids));
    }

    public function getCaption()
    {
        return $this->getMeta('product_caption');
    }

    /**
     * Retrieve the post thumbnail.
     *
     * @param string $size
     *
     * @return Image
     */
    public function getFeatureThumbnail($size = 'thumbnail'): ?Image
    {
        $thumbnailId = $this->getFeatureThumbnailId();
        if (!empty($thumbnailId)) {
            return new Image($thumbnailId, $size);
        }

        $placeholderId = intval(get_option('woocommerce_placeholder_image'));
        if (!empty($placeholderId)) {
            return new Image($placeholderId, $size);
        }

        return null;
    }

    public function getCategories(array $args = []): array
    {
        return array_map(function($term) {
            return new ProductCategory($term->term_id);
        }, wc_get_product_terms($this->getId(), 'product_cat', $args));
    }

    /**
     * Create query for custom post type
     *
     * @param array $args
     *
     * @return AbstractWPCollections
     */
    public static function getQuery($args = []): AbstractWPCollections
    {
        $default = [
            'posts_per_page' => -1,
            'status' => ['publish']
        ];

        return new WPQueryProductCollections(new \WP_Query(
            array_merge($default, $args)
        ));
    }

    public function getUpSellIds(): ?WPQueryProductCollections
    {
        $ids = $this->product->get_upsell_ids();

        if (empty($ids)) {
            return null;
        }

        return new WPQueryProductCollections(new \WP_Query([
            'posts_in' => $ids
        ]));
    }

    public function getShortDescription()
    {
        return $this->product->get_short_description();
    }

    public function getStockStatus()
    {
        return $this->product->get_stock_status();
    }

    public function hasDiscount(): bool
    {
        return $this->product->is_on_sale();
    }

    public function getPercentageDiscount(): string
    {
        $discount = (1 - round($this->product->get_price() / $this->product->get_regular_price(false), 2)) * 100;
        return sprintf('-%d%%', $discount);
    }

    public function getAttribute($attributeName)
    {
        $attributeName = 'pa_' . $attributeName;

        $attributes = $this->product->get_attributes();

        $attribute = Arr::get($attributes, $attributeName, null);

        if ($attribute instanceof \WC_Product_Attribute) {
            return array_map(function($option) use ($attributeName) {
                return new ProductAttributeValue($option, $attributeName);
            }, $attribute->get_options());
        }

        return [];
    }

    public function getProductUrl(): ?string
    {
        return $this->product->get_product_url();
    }
}