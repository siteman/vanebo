<?php

namespace PP\Models\Posts;

use PP\Models\AbstractPostType;

class Page extends AbstractPostType
{
    public static $needsRegister = false;

    /**
     * Return slug for post type
     *
     * @return string
     */
    public static function getPostType(): string
    {

        return 'page';
    }

    /**
     * Have to return post type args
     *
     * List of allowed arguments: https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return array
     */
    public static function getArgs(): array
    {

        return [];
    }
}