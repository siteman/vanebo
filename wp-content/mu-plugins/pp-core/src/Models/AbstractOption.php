<?php

namespace PP\Models;

abstract class AbstractOption
{
    public function get(string $option)
    {
        return \PP\Facades\Options::get($option);
    }
}