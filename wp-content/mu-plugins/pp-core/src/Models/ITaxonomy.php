<?php

namespace PP\Models;

/**
 * Interface ITaxonomy
 * @package PPTheme\Core\Models
 */
interface ITaxonomy
{
    /**
     * Return slug taxonomy
     *
     * @return string
     */
    public static function getTaxonomy(): string;

    /**
     * List of post types
     *
     * @return array
     */
    public static function getPostTypes() : array;

    /**
     * Args to register taxonomy
     *
     * List of allowed arguments: https://codex.wordpress.org/Function_Reference/register_taxonomy
     *
     * @return array
     */
    public static function getArgs() : array;
}