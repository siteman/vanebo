<?php

namespace PP\Models\Taxonomies;

use PP\Image;
use PP\Models\AbstractTaxonomy;
use PP\Models\Posts\Product;

class ProductCategory extends AbstractTaxonomy
{
    public static $needsRegister = false;

    /**
     * Return slug taxonomy
     *
     * @return string
     */
    public static function getTaxonomy(): string
    {
        return 'product_cat';
    }

    /**
     * List of post types
     *
     * @return array
     */
    public static function getPostTypes(): array
    {
        // TODO: Implement getPostTypes() method.
    }

    /**
     * Args to register taxonomy
     *
     * List of allowed arguments: https://codex.wordpress.org/Function_Reference/register_taxonomy
     *
     * @return array
     */
    public static function getArgs(): array
    {
        // TODO: Implement getArgs() method.
    }

    /**
     * Return uncategorized id
     *
     * @return int
     */
    public static function getDefaultId(): int
    {
        return abs(get_option('default_product_cat', 0));
    }

    public function getProducts(array $args = []): \PP\Collections\AbstractWPCollections
    {
        return Product::getQuery(array_merge([
            'tax_query' => [
                [
                    'taxonomy' => self::getTaxonomy(),
                    'field' => 'term_id',
                    'terms' => $this->getId()
                ]
            ]
        ], $args));
    }

    public function getImage(): ?Image
    {
        $id = $this->getMeta('thumbnail_id');

        if (empty($id)) {
            return null;
        }

        return new Image($id);
    }
}