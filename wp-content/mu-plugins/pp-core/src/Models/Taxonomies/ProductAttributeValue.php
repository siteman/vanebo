<?php

namespace PP\Models\Taxonomies;

use PP\Image;
use PP\Models\AbstractTaxonomy;

class ProductAttributeValue extends AbstractTaxonomy
{
    public static $needsRegister = false;

    /**
     * PostType constructor.
     *
     * @param null $id
     */
    public function __construct(int $id, string $attribute)
    {
        $this->setId($id);
        $this->setTerm(get_term($id, $attribute));
    }

    /**
     * Return slug taxonomy
     *
     * @return string
     */
    public static function getTaxonomy(): string
    {
        // TODO: Implement getPostTypes() method.
    }

    /**
     * List of post types
     *
     * @return array
     */
    public static function getPostTypes(): array
    {
        // TODO: Implement getPostTypes() method.
    }

    /**
     * Args to register taxonomy
     *
     * List of allowed arguments: https://codex.wordpress.org/Function_Reference/register_taxonomy
     *
     * @return array
     */
    public static function getArgs(): array
    {
        // TODO: Implement getArgs() method.
    }

    public function getAttributeName(): string
    {
        return get_taxonomy($this->getTerm()->taxonomy)->labels->singular_name;
    }

    public function getColor(): ?string
    {
        return $this->getMeta('attribute_color');
    }

    public function getIcon(): ?string
    {
        return $this->getMeta('attribute_icon');
    }

    public function getImage(): ?Image
    {
        $id = $this->getMeta('thumbnail_id');

        if (empty($id)) {
            return null;
        }

        return new Image($id);
    }

    public function getShopUrl(): ?string
    {
        return wc_get_page_permalink('shop') . '?tax_'.$this->getTerm()->taxonomy.'=' . $this->getId();
    }
}