<?php

namespace PP\Models\Taxonomies;

use PP\Models\AbstractTaxonomy;

class Category extends AbstractTaxonomy
{
    static $needsRegister = false;

    /**
     * Return slug taxonomy
     *
     * @return string
     */
    public static function getTaxonomy(): string
    {
        return 'category';
    }

    /**
     * List of post types
     *
     * @return array
     */
    public static function getPostTypes(): array
    {
        // TODO: Implement getPostTypes() method.
    }

    /**
     * Args to register taxonomy
     *
     * List of allowed arguments: https://codex.wordpress.org/Function_Reference/register_taxonomy
     *
     * @return array
     */
    public static function getArgs(): array
    {
        // TODO: Implement getArgs() method.
    }
}