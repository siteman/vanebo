<?php

namespace PP\Models;

use PP\Collections\WPTermCollections;
use Illuminate\Support\Arr;
use PP\Facades\Taxonomy;

abstract class AbstractTaxonomy implements ITaxonomy
{
    /**
     * Post id
     *
     * @var int|null
     */
    private $id = null;

    /**
     * WP_Post object
     *
     * @var \WP_Term|null
     */
    private $term = null;

    /**
     * List of registered taxonomies
     *
     * @var array
     */
    private static $taxonomies = [];

    /**
     * List of data in meta
     *
     * @var array
     */
    private $meta = [];

    /**
     * In order to don't register post type set false in your class
     *
     * @var bool
     */
    public static $needsRegister = true;

    /**
     * PostType constructor.
     *
     * @param null $id
     */
    public function __construct($id = null)
    {

        if ($id instanceof \WP_Term) {
            $this->setId($id->term_id);
        } else {
            $this->setId($id === null ? get_queried_object()->term_id : $id);
        }

        if ($this->getId()) {
            $this->setTerm(get_term($this->getId(), static::getTaxonomy()));
        }
    }

    /**
     * Setter term
     *
     * @param \WP_Term $term
     */
    public function setTerm(\WP_Term $term)
    {

        $this->term = $term;
    }

    /**
     * Setter post id
     *
     * @param int $id
     */
    public function setId(int $id)
    {

        $this->id = intval($id);
    }

    /**
     * Return post id
     *
     * @return false|int
     */
    public function getId()
    {

        return empty($this->id) ? null : $this->id;
    }

    /**
     * Return term title
     *
     * @return string
     */
    public function getTitle()
    {

        return $this->getTerm()->name;
    }

    /**
     * Return term description
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->getTerm()->description;
    }

    /**
     * Getter post
     *
     * @return \WP_Term
     */
    public function getTerm() : \WP_Term
    {

        return $this->term;
    }

    /**
     * @return string
     */
    public function getSlug()
    {

        return $this->getTerm()->slug;
    }

    /**
     * @return string|\WP_Error
     */
    public function getPermalink()
    {

        return \get_term_link($this->getTerm());
    }

    /**
     * Return WP_Query object with all posts
     *
     * @return mixed
     */
    public static function getAll($args = []) : WPTermCollections
    {

        return self::getTerms(array_merge([
            'hide_empty' => false
        ], $args));
    }

    /**
     * @param array $args
     *
     * @return WPTermCollections
     */
    public static function getTerms($args = []) : WPTermCollections
    {
        $args['taxonomy'] = static::getTaxonomy();

        return new WPTermCollections(get_terms($args));
    }

    /**
     * Return meta value
     *
     * @param $key
     *
     * @return mixed
     */
    public function getMeta($key)
    {
        $driver = \PP\Models\AcfPostMeta::class;

        if (!$this->meta instanceof $driver) {
            $this->meta = new $driver('term_' . $this->getId());
        }

        return $this->meta->getField($key);
    }

    /**
     * Return term children
     *
     * @return \PP\Collections\WPTermCollections|null
     */
    public function getChildren(array $args =[]): ?WPTermCollections
    {
        return self::getAll(array_merge([
            'parent' => $this->getId()
        ], $args));
    }

    /**
     * Check if term is child
     *
     * @return bool
     */
    public function isChild()
    {
        return $this->getTerm()->parent !== 0;
    }

    /**
     * Check if term is main
     *
     * @return bool
     */
    public function isMain()
    {
        return !$this->isChild();
    }

    /**
     * Return parent id
     *
     * @return int
     */
    public function getParentId(): int
    {
        return $this->getTerm()->parent;
    }

    /**
     * Return parent
     *
     * @return \PP\Models\AbstractTaxonomy
     * @throws \Exception
     */
    public function getParent(): ?AbstractTaxonomy
    {
        if ($this->isMain()) {
            return null;
        }

        return Taxonomy::factory(static::getTaxonomy(), $this->getTerm()->parent);
    }
}