<?php

namespace PP\Models;

/**
 * Interface IPostType
 * @package PPTheme\Core\Models
 */
interface IPostType
{

    /**
     * Return slug for post type
     *
     * @return string
     */
    public static function getPostType(): string;

    /**
     * Have to return post type args
     *
     * List of allowed arguments: https://codex.wordpress.org/Function_Reference/register_post_type
     * List dashboard icons: https://developer.wordpress.org/resource/dashicons
     *
     * @return array
     */
    public static function getArgs(): array;
}