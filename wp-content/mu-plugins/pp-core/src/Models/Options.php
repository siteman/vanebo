<?php

namespace PP\Models;

class Options
{
    private $acf = null;

    public function __construct()
    {
        $this->acf = new AcfPostMeta('options', true);
    }

    public function get(string $key)
    {
        return $this->acf->getField($key);
    }
}