<?php

namespace PP\Models;

use PP\Collections\AbstractWPCollections;
use PP\Collections\WPQueryCollections;
use PP\Collections\WPQueryCommentCollections;
use PP\Facades\Posts;
use PP\Image;
use PP\Models\Posts\Page;
use PPTheme\Core\Application;

abstract class AbstractPostType implements IPostType
{

    /**
     * Post id
     *
     * @var int|null
     */
    private $id = null;

    /**
     * WP_Post object
     *
     * @var \WP_Post|null
     */
    private $post = null;

    /**
     * List of data in meta
     *
     * @var IPostMeta
     */
    private ?IPostMeta $meta = null;

    /**
     * In order to don't register post type set false in your class
     *
     * @var bool
     */
    public static $needsRegister = true;

    /**
     * PostType constructor.
     *
     * @param null $id
     *
     * @throws \Exception
     */
    public function __construct($id = null)
    {
        if ($id instanceof \WP_Post) {
            $this->setId($id->ID);
        } else {
            $this->setId($id === null ? get_the_ID() : $id);
        }

        if ($this->getId()) {
            $this->setPost(get_post($this->getId()));
        }
    }

    /**
     * Setter post id
     *
     * @param int $id
     */
    public function setId(int $id)
    {

        $this->id = intval($id);
    }

    /**
     * Return post id
     *
     * @return false|int
     */
    public function getId()
    {

        return (int) empty($this->id) ? get_the_ID() : $this->id;
    }

    /**
     * Setter post
     *
     * @param \WP_Post $post
     *
     * @throws \Exception
     */
    public function setPost(\WP_Post $post)
    {
        if ($post->post_type !== static::getPostType()) {
            throw new \Exception('Invalid post type ' . static::getPostType() . ', excepted ' . $post->post_type);
        }

        $this->post = $post;
    }

    /**
     * Getter post
     *
     * @return \WP_Post
     */
    public function getPost() : \WP_Post
    {

        return $this->post;
    }

    /**
     * Return slug
     *
     * @return string
     */
    public function getSlug()
    {

        return $this->getPost()->post_name;
    }

    /**
     * Return post title
     *
     * @return string
     */
    public function getTitle()
    {

        return \get_the_title($this->getId());
    }

    /**
     * Return post excerpt
     *
     * @return string
     */
    public function getExcerpt()
    {

        return \get_the_excerpt($this->getId());
    }

    /**
     * Check that post has thumbnail
     *
     * @return bool
     */
    public function hasFeatureThumbnail()
    {

        return has_post_thumbnail($this->getId());
    }

    /**
     * Retrieve the post thumbnail.
     *
     * @param string $size
     *
     * @return Image
     */
    public function getFeatureThumbnail($size = 'thumbnail'): ?Image
    {
        return !empty($this->getFeatureThumbnailId()) ? new Image($this->getFeatureThumbnailId(), $size) : null;
    }

    /**
     * Return thumbnail id
     *
     * @return int|string
     */
    public function getFeatureThumbnailId()
    {

        return (int) get_post_thumbnail_id($this->getId());
    }

    /**
     * Return feature thumbnail id
     *
     * @param string $size
     *
     * @return false|string
     */
    public function getFeatureThumbnailUrl($size = 'thumbnail')
    {
        return wp_get_attachment_image_url($this->getFeatureThumbnailId(), $size);
    }

    /**
     * Return post content
     *
     * @param bool $filter Apply filter "the_content".
     * @param string  $moreLinkText Optional. Content for when there is more text.
     * @param bool $stripTeaser
     *
     * @return string
     */
    public function getContent($filter = false, $moreLinkText = null, $stripTeaser = false)
    {

        $content = \get_the_content($moreLinkText, $stripTeaser, $this->getId());

        if ($filter === true) {
            $content = apply_filters('the_content', $content);
        }

        return $content;
    }

    /**
     * Return permalink for single page
     *
     * @return false|string
     */
    public function getPermalink()
    {

        return get_the_permalink($this->getId());
    }

    /**
     * Return archive url
     *
     * @return false|string
     */
    public static function getArchivePageUrl()
    {

        return \get_post_type_archive_link(static::getPostType());
    }

    /**
     * Return meta value
     *
     * @param $key
     *
     * @return mixed
     */
    public function getMeta($key)
    {
        $driver = \PP\Models\AcfPostMeta::class;

        if (!$this->meta instanceof $driver) {
            $this->meta = new $driver($this->getId());
        }

        return $this->meta->getField($key);
    }

    /**
     * Return post type
     *
     * @return false|string
     */
    public function getType()
    {

        return get_post_type($this->getId());
    }

    /**
     * Return publish date of post
     *
     * @param $format
     *
     * @return false|string
     */
    public function getPublishDate($format = '')
    {

        return \get_the_date($format, $this->getId());
    }

    /**
     * Return last update date of post
     *
     * @param string $format
     *
     * @return false|string
     */
    public function getUpdateDate($format = '')
    {

        return \get_the_modified_date($format, $this->getId());
    }

    /**
     * Return id
     *
     * @return int
     */
    public function getThumbnailId()
    {
        return get_post_thumbnail_id($this->getId());
    }

    /**
     * Return page id
     *
     * @return int
     */
    public function getParentId(): ?int
    {
        return $this->post->post_parent;
    }

    /**
     * Return parent
     *
     * @return \PP\Models\AbstractPostType
     */
    public function getParent(): ?AbstractPostType
    {
        if (!$this->getParentId()) {
            return null;
        }

        return Posts::factory($this->getParentId());
    }

    /**
     * Return path to image
     *
     * @param string $size
     * @return bool|string
     */
    public function getThumbnailPath($size = 'full')
    {
        $attachment_id = $this->getThumbnailId();

        $file = get_attached_file($attachment_id, true);
        if (empty($size) || $size === 'full') {
            // for the original size get_attached_file is fine
            return realpath($file);
        }
        if (! wp_attachment_is_image($attachment_id) ) {
            return false; // the id is not referring to a media
        }
        $info = image_get_intermediate_size($attachment_id, $size);
        if (!is_array($info) || ! isset($info['file'])) {
            return false; // probably a bad size argument
        }

        return realpath(str_replace(wp_basename($file), $info['file'], $file));
    }

    /**
     * Set post as global $post
     */
    public function setupPostData()
    {

        global $post;

        $post = $this->getPost();
        setup_postdata($this->getPost());
    }

    /**
     * Create query for custom post type
     *
     * @param array $args
     *
     * @return AbstractWPCollections
     */
    public static function getQuery($args = []): AbstractWPCollections
    {

        $default = [
            'post_status' => 'publish',
            'post_type' => [static::getPostType()],
            'posts_per_page' => -1
        ];

        $args = array_merge($default, $args);

        return new WPQueryCollections(new \WP_Query($args));
    }

    /**
     * Return WP_Query object with all posts
     *
     * @return mixed
     */
    public static function getAllPublished($args = []) : AbstractWPCollections
    {

        return self::getQuery(array_merge([
            'post_status' => 'publish',
            'posts_per_page' => -1
        ], $args));
    }

    /**
     * @param array $args
     * @return \PP\Collections\WPQueryCommentCollections
     */
    public function getComments(array $args = []): WPQueryCommentCollections
    {
        return new WPQueryCommentCollections(new \WP_Comment_Query(array_merge([
            'status' =>  'approve',
            'number' => 3,
            'no_found_rows' => false,
            'post_id' => $this->getId()
        ], $args)));
    }

    public function isPublish(): bool
    {
        return $this->post->post_status === 'publish';
    }
}