<?php

namespace PP\Models;

interface IPostMeta
{

    /**
     * Return post meta by key
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getField(string $key);
}