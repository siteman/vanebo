<?php

namespace PP\Models;

use Illuminate\Support\Arr;

class AcfPostMeta implements IPostMeta
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var array
     */
    private $fields = [];

    /**
     * AcfPostMeta constructor.
     *
     * @param int $id
     */
    public function __construct($id, bool $flushAll = false)
    {
        if (!class_exists('ACF')) {
            throw new \Exception('Plugin Advanced Custom Fields Pro is required');
        }

        $this->id = $id;
        if ($flushAll) {
            $this->flushFields();
        }
    }

    /**
     * Get all fields for id
     */
    public function flushFields()
    {

        if (empty($this->fields)) {
            $this->fields = get_fields($this->id);
        }
    }

    /**
     * Return field
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getField(string $key)
    {

        if (!Arr::has($this->fields, $key)) {
            $field = get_field($key, $this->id);
            $this->fields[$key] = $field;
        }

        return Arr::get($this->fields, $key);
    }
}