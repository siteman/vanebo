<?php

namespace PP\Models;

class Comment
{
    /**
     * @var array|\WP_Comment|null
     */
    private $comment = null;

    /**
     * Comment constructor.
     *
     * @param \WP_Comment|int $id
     */
    public function __construct($id = null)
    {
        if ($id instanceof \WP_Comment) {
            $this->comment = $id;
        } else {
            $this->comment = get_comment($id);
        }
    }

    public function getAuthor(): string
    {
        return $this->comment->comment_author;
    }

    public function getAuthorEmail(): string
    {
        return $this->comment->comment_author_email;
    }

    public function getContent(): string
    {
        return $this->comment->comment_content;
    }

    public function getDate(string $format = 'Y-m-d H:i:s'): string
    {
        return date($format, strtotime($this->comment->comment_date));
    }

    public function getId(): int
    {
        return $this->comment->comment_ID;
    }

    public function getRate(): int
    {
        return intval(get_comment_meta($this->getId(), 'rating', true));
    }
}