<?php

namespace PP\Shortcodes;

interface IShortcode
{
    public function getName(): string;

    public function render($params = []): string;
}