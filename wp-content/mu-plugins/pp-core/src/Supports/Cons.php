<?php

namespace PP\Supports;

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

class Cons
{
	/**
	 * Check defined const exists
	 *
	 * @param $const
	 * @return bool
	 */
	public static function exist($const)
	{

		return defined($const);
	}

	/**
	 * Return value from constant
	 *
	 * @param $const
	 * @param null $default
	 * @return mixed|null
	 */
	public static function get($const, $default = null)
	{

		return self::exist($const) ? constant($const) : $default;
	}

	/**
	 * Define const
	 *
	 * @param $const
	 * @param null $value
	 */
	public static function set($const, $value = null)
	{

		if (!self::exist($const)) {
			define($const, $value);
		}
	}
}