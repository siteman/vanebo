<?php

namespace PP\Supports;

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

class Woocommerce
{
	public static function getShopUrl(): ?string
	{
        $id = wc_get_page_id('shop');

        if (!empty($id)) {
            return get_permalink($id);
        }

        return null;
	}
}