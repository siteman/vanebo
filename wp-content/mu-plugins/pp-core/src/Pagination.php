<?php

namespace PP;

use PP\Collections\AbstractWPCollections;

class Pagination
{
    public $collections;
    public $current;
    public string $baseLink;
    public $linkArgs = [];

    /**
     * Pagination constructor.
     *
     * @param AbstractWPCollections $collections
     */
    public function __construct(AbstractWPCollections $collections)
    {

        $this->setCollection($collections);
    }

    /**
     * @param AbstractWPCollections $collections
     */
    public function setCollection(AbstractWPCollections $collections)
    {

        $this->collections = $collections;
    }

    /**
     * @return AbstractWPCollections
     */
    public function getCollection(): AbstractWPCollections
    {

        return $this->collections;
    }

    /**
     * @param array $args
     *
     * @return bool
     */
    public function display($args = [])
    {
        $defaults = array(
            'range'           => 4,
            'previous_string' => __('Forrige', _THEME_DOMAIN_),
            'next_string'     => __('Neste', _THEME_DOMAIN_),
            'before_output'   => '<nav class="pagination__nav"><ul class="pagination">',
            'after_output'    => '</ul></nav>',
            'base' => str_replace(9999999999, '%#%', get_pagenum_link(9999999999)),
        );

        $args = wp_parse_args($args, apply_filters('wp_bootstrap_pagination_defaults', $defaults));

        $args[ 'range' ] = (int)$args[ 'range' ] - 1;

        $count = (int)$this->getCollection()->getMaxNumPages();
        $page  = intval($this->getCollection()->getPage());
        $ceil  = ceil($args[ 'range' ] / 2);

        if ($count <= 1) {
            echo $args[ 'before_output' ] . $args[ 'after_output' ];
            return;
        }

        if ( ! $page) {
            $page = 1;
        }

        if ($count > $args[ 'range' ]) {
            if ($page <= $args[ 'range' ]) {
                $min = 1;
                $max = $args[ 'range' ] + 1;
            } elseif ($page >= ($count - $ceil)) {
                $min = $count - $args[ 'range' ];
                $max = $count;
            } elseif ($page >= $args[ 'range' ] && $page < ($count - $ceil)) {
                $min = $page - $ceil;
                $max = $page + $ceil;
            }
        } else {
            $min = 1;
            $max = $count;
        }

        $echo     = '';
        $previous = intval($page) - 1;
        $previous = $this->getLink($previous);

        if ($previous && (1 != $page)) {
            $echo .= '<li><a href="' . $previous . '" title="' . $args[ 'previous_string' ] . '">' . $args[ 'previous_string' ] . '</a></li>';
        }

        if ( ! empty($min) && ! empty($max)) {
            for ($i = $min; $i <= $max; $i++) {
                if ($page == $i) {
                    $echo .= '<li class="pagination__item--active"><span>' . $i . '</span></li>';
                } else {
                    $echo .= sprintf('<li><a href="%s">%d</a></li>',
                        $this->getLink($i), $i);
                }
            }
        }

        $next = intval($page) + 1;
        $next = $this->getLink($next);
        if ($next && ($count != $page)) {
            $echo .= '<li class="page-item"><a href="' . $next . '" class="page-link" title="' . $args[ 'next_string' ] . '">' . $args[ 'next_string' ] . '</a></li>';
        }

        if (isset($echo)) {
            echo $args[ 'before_output' ] . $echo . $args[ 'after_output' ];
        }
    }

    public function getLink(int $page): string
    {
        if (isset($this->baseLink)) {
            return esc_attr(str_replace('%#%', $page, $this->baseLink));
        }

        return get_pagenum_link($page);
    }

    /**
     * @param string $baseLink
     */
    public function setBaseLink(string $baseLink): void
    {
        $this->baseLink = $baseLink;
    }
}