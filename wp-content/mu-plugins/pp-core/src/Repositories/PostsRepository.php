<?php

namespace PP\Repositories;

use Illuminate\Support\Arr;
use PP\Facades\Posts;
use PP\Models\AbstractPostType;

class PostsRepository
{
    public function findByMeta(string $key, string $value): ?AbstractPostType
    {
        $post = Arr::first(get_posts([
            'post_status' => ['draft', 'private', 'publish'],
            'post_type' => get_post_types(),
            'meta_key' => $key,
            'meta_value' => $value
        ]));

        if ($post === null) {
            return null;
        }

        return Posts::factory($post->ID);
    }
}