<?php

namespace PP;

use Illuminate\Support\Arr;
use PP\Facades\Theme;

abstract class AbstractBlock
{
    /**
     * @var array
     */
    public $args = [];

    private $meta = [];

    private ?int $postId = null;

    /**
     * Return an args for registering block
     *
     * @return array
     */
    abstract public function getArgs(): array;

    /**
     * Return the block slug
     *
     * @return string
     */
    abstract public function getSlug(): string;

    /**
     * Return list of data for template
     *
     * @return array
     */
    public function with(): array
    {
        return [];
    }

    /**
     * @return int|null
     */
    public function getPostId(): ?int
    {
        return $this->postId;
    }

    /**
     * @param int|null $postId
     */
    public function setPostId(?int $postId): void
    {
        $this->postId = $postId;
    }

    /**
     * Render block
     */
    public function render($blockArgs, $content, $is_preview, $postId): void
    {
        if (method_exists($this, 'getRenderTemplate')) {
            $template = $this->getRenderTemplate();
        } else {
            $template = Arr::get($this->getArgs(), 'render_template');
        }

        if (!$template) {
            throw new \InvalidArgumentException('Template has not been found for this block');
        }

        $block = $this;
        $this->setPostId($postId);
        extract($this->with());

        echo '<div id="'.Arr::get($blockArgs, 'id', '').'" class="align'.Arr::get($blockArgs, 'align', '').'">';

        include Theme::resourcesPath($template);

        echo '</div>';
    }

    /**
     * Set params
     *
     * @param array $args
     * @return \PP\AbstractBlock
     */
    public function setArgs(array $args): self
    {
        $this->args = $args;

        return $this;
    }

    /**
     * Return option value
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        $this->meta = get_fields();

        return Arr::get($this->meta, $this->getSlug() . '_' . $key, $default);
    }
}