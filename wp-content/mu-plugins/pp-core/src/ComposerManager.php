<?php

namespace PP;

use Illuminate\Support\Collection;

class ComposerManager
{
    public $composers = [];

    /**
     * Register a composer class
     *
     * @param string $composerName
     */
    public function register(string $composerName)
    {
        $composer = new $composerName();
        $this->composers[] = $composer;
    }

    /**
     * Return collection of composers adjusting for template
     *
     * @param string $view
     * @return \Illuminate\Support\Collection
     */
    public function getComposers(string $view): Collection
    {
        return collect($this->composers)->filter(function($composer) use ($view) {
            /** @var $composer \PP\AbstractViewComposer */
            foreach ($composer->getViews() as $composerView) {
                if ($composerView === $view) {
                    return true;
                }
            }

            return false;
        });
    }

    /**
     * Return composer params adjusting for template
     *
     * @param string $template
     * @return array
     */
    public function getComposersParams(string $template): array
    {

        $composers = $this->getComposers($template);

        if ($composers->isEmpty()) {
            return [];
        }

        $params = [];

        $this->getComposers($template)->each(function($composer) use (&$params, $template) {
            $params = array_merge($params, $composer->with());
        });

        return $params;
    }

    /**
     * Resolve composers by view
     *
     * @param string $template
     * @return array
     */
    public function resolve(string $template): array
    {
        $this->getComposers($template)->each(function($composer) use ($template) {
            $composer->setView($template)->resolve();
        });

        return $this->getComposersParams($template);
    }
}