<?php

namespace PP\Providers;

use Carbon\Laravel\ServiceProvider;
use PP\ComposerManager;

class ComposerThemeProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->instance('composer_manager', new ComposerManager());
        add_filter('template_include', [$this, 'hookActionTemplateInclude'], PHP_INT_MAX);

        add_action('woocommerce_before_template_part', [$this, 'hookActionBeforeTemplatePart'], PHP_INT_MAX, 4);
        add_action('woocommerce_after_template_part', [$this, 'hookActionAfterTemplatePart'], 0, 4);
    }

    public function hookActionTemplateInclude($template)
    {
        if (!$this->app->has('theme')) {
            return $template;
        }

        extract($this->app->get('composer_manager')->resolve(substr($template, strlen($this->app->get('theme')->resourcesPath()))));

        if ($template) {
            include($template);
        } elseif (current_user_can('switch_themes')) {
            $theme = wp_get_theme();
            if ($theme->errors()) {
                wp_die($theme->errors());
            }
        }
        die();
    }

    public function hookActionBeforeTemplatePart($templatename, $template_path, $located, $args)
    {
        extract($args);

        extract($this->app->get('composer_manager')->resolve(substr($located, strlen($this->app->get('theme')->resourcesPath()))));

        include $located;

        ob_start();
    }

    public function hookActionAfterTemplatePart($templatename, $template_path, $located, $args)
    {
        ob_end_clean();
    }
}