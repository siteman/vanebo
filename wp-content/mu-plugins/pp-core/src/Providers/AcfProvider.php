<?php

namespace PP\Providers;

use Carbon\Laravel\ServiceProvider;
use PP\Facades\Assets;
use PP\Models\Options;
use PP\Supports\Cons;

class AcfProvider extends ServiceProvider
{
    public function register()
    {
        add_action('plugins_loaded', function() {
            if (!class_exists('\ACF')) {
                return;
            }

            if ($this->app->get('env') === 'production') {
                add_filter('acf/settings/show_admin', '__return_false');
            }

            add_filter('acf/settings/load_json', [$this, 'hookAcfSettingsLoadJson']);
            add_filter('acf/settings/save_json', [$this, 'hookAcfSettingsSaveJson']);

            $this->app->singleton('options', function() {
                return new Options();
            });
        });

        if (Cons::get('GOOGLE_MAPS_API_KEY')) {
            add_action('acf/init', function() {
                acf_update_setting('google_api_key', Cons::get('GOOGLE_MAPS_API_KEY'));
                Assets::addJsData(['google_maps_api_key' => Cons::get('GOOGLE_MAPS_API_KEY')]);
            });
        }
    }

    public function hookAcfSettingsLoadJson($paths) : array
    {

        unset($paths[0]);

        $paths[] = $this->app->commonPath('acf');

        return $paths;
    }

    public function hookAcfSettingsSaveJson($path)
    {

        return $this->app->commonPath('acf');
    }
}