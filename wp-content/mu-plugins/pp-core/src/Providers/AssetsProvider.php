<?php

namespace PP\Providers;

use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use PP\Assets;
use PP\Assets\Script;
use PP\Assets\Style;
use PP\Facades\Theme;

class AssetsProvider extends ServiceProvider
{
    public function register()
    {
        $assets = new Assets();
        $this->app->instance('assets', $assets);

        $this->registerHooks();

        add_action('init', function () {
            $this->addGeneralLocalizes();
        });
    }

    public function addGeneralLocalizes()
    {
        $this->app->get('assets')->addJsData([
            'ajaxUrl' => admin_url('admin-ajax.php'),
            'restUrl' => get_rest_url(),
            //'publicPath' => Theme::publicPath(),
            'locale' => get_locale(),
            'lang' => substr(get_locale(), 0, 2)
        ]);
    }

    /**
     * Add action and filters
     */
    public function registerHooks()
    {
        add_action('admin_enqueue_scripts', [$this, 'actionAdminEnqueueScripts'], 10, 0);
        add_action('wp_enqueue_scripts', [$this, 'actionWpEnqueueScripts'], 10, 0);
        add_action('admin_enqueue_scripts', [$this, 'actionAdminEnqueueScripts'], 10, 0);
        add_action('wp_footer', [$this, 'hookActionWpFooter'], 0, 0);
        add_action('enqueue_block_assets', [$this, 'actionGutenbergEnqueueScripts']);
    }

    /**
     * Wordpress core action admin_enqueue_scripts
     *
     * https://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
     */
    public function actionAdminEnqueueScripts()
    {

        collect($this->app->get('assets')->getAdminScripts())->each(function(Script $script) {
            $script->enqueue();
        });

        collect($this->app->get('assets')->getAdminStyles())->each(function(Style $style) {
            $style->enqueue();
        });
    }

    /**
     * Wordpress core action enqueue_block_assets
     */
    public function actionGutenbergEnqueueScripts()
    {
        collect($this->app->get('assets')->getGutenbergScripts())->each(function(Script $script) {
            $script->enqueue();
        });

        collect($this->app->get('assets')->getGutenbergStyles())->each(function(Style $style) {
            $style->enqueue();
        });
    }

    /**
     * Wordpress core action wp_enqueue_scripts
     *
     * https://codex.wordpress.org/Plugin_API/Action_Reference/wp_enqueue_scripts
     */
    public function actionWpEnqueueScripts()
    {
        if (is_single() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }

        collect($this->app->get('assets')->getFrontScripts())->each(function(Script $script) {
            $script->enqueue();
        });

        collect($this->app->get('assets')->getFrontStyles())->each(function(Style $style) {
            $style->enqueue();
        });
    }

    /**
     * Add js the localizes data to footer in json format
     *
     * Action https://codex.wordpress.org/Plugin_API/Action_Reference/wp_footer
     * Append script to the footer https://codex.wordpress.org/Function_Reference/wp_footer
     */
    public function hookActionWpFooter()
    {
        echo '<script type="text/javascript">window.pp = ' . json_encode($this->app->get('assets')->getLocalizeScript()) . '</script>';
    }
}