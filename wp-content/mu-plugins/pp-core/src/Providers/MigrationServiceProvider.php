<?php

namespace PP\Providers;

use Illuminate\Database\Migrations\DatabaseMigrationRepository;
use Illuminate\Database\MigrationServiceProvider as MigrationServiceProviderCore;

class MigrationServiceProvider extends MigrationServiceProviderCore
{
    /**
     * Register the migration repository service.
     *
     * @return void
     */
    protected function registerRepository()
    {
        $this->app->singleton('migration.repository', function ($app) {
            return new DatabaseMigrationRepository($app['db'], 'pp_migrations');
        });
    }
}