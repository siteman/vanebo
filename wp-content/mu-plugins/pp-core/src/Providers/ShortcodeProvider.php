<?php

namespace PP\Providers;

use Carbon\Laravel\ServiceProvider;
use PP\ShortcodeManager;

class ShortcodeProvider extends ServiceProvider
{
    public function register()
    {
        $this->registerManager();
    }

    public function registerManager(): void
    {
        $this->app->instance('shortcodes', new ShortcodeManager());
    }
}