<?php

namespace PP\Providers;

use Carbon\Laravel\ServiceProvider;
use PP\BlocksManager;
use PP\Managers\ControllerManager;
use PP\Models\Posts\Page;
use PP\Models\Posts\Post;
use PP\Models\Posts\Product;
use PP\Models\Taxonomies\Category;
use PP\PostTypeManager;
use PP\TaxonomyManager;

class ContentProvider extends ServiceProvider
{
    public function register()
    {
        $this->registerBindings();
        $this->registerPostTypes();
        $this->registerTaxonomies();
    }

    public function registerBindings()
    {
        $this->app->instance('posts', new PostTypeManager());
        $this->app->instance('taxonomies', new TaxonomyManager());
        $this->app->instance('blocks', new BlocksManager());
        $this->app->instance('controllers', new ControllerManager($this->app));
    }

    public function registerPostTypes()
    {
        collect([
            Page::class,
            Post::class,
            Product::class
        ])->each(function($post) {
            $this->app->get('posts')->register($post);
        });
    }

    public function registerTaxonomies()
    {
        collect([
            Category::class
        ])->each(function($taxonomy) {
            $this->app->get('taxonomies')->register($taxonomy);
        });
    }
}