<?php

namespace PP;

use Illuminate\Support\Arr;
use PP\Models\AbstractPostType;

class PostTypeManager {

    /**
     * Array of registered post types
     *
     * @var array
     */
    private array $registeredPostTypes = [];

    /**
     * Array of post type to register
     *
     * @var array
     */
    private array $postTypes = [];

    /**
     * PostTypeManager constructor.
     */
    public function __construct()
    {
        add_action('init', [$this, 'hookActionInit'], 10, 0);
        add_action('save_post', [$this, 'hookActionSavePost'], 10, 1);
    }

    /**
     * Register post type
     *
     * @param string $postType
     */
    public function register(string $postType): void
    {
        $this->postTypes[] = $postType;
    }

    /**
     * Return class name by post type
     *
     * @param string $postType
     *
     * @return string|null
     */
    public function get(string $postType): ?string
    {
        return Arr::get($this->registeredPostTypes, $postType, null);
    }

    /**
     * Factory post instance
     *
     * @param $id
     * @param null $postType
     * @return mixed
     * @throws \Exception
     */
    public function factory($id, $postType = null): ?AbstractPostType
    {
        if (get_post_type($id) === 'revision') {
            $revisionPost = get_post($id);
            $id = $revisionPost->post_parent;
        }

        $postType = empty($postType) ? get_post_type($id) : $postType;

        $class = $this->get($postType);

        if ($class === null) {
            throw new \Exception('Model for '.$postType.' post type not exists.');
        }

        return new $class($id);
    }

    /**
     * Create instance from global post
     *
     * @return mixed
     * @throws \Exception
     */
    public function factoryByGlobalPost(): ?AbstractPostType
    {
        global $post;

        if (empty($post->ID) || empty($post->post_type)) {
            return null;
        }

        return $this->factory($post->ID, $post->post_type);
    }

    /**
     * After looping through a separate query, this function restores
     * the $post global to the current post in the main query.
     */
    public function resetPostData()
    {
        wp_reset_postdata();
    }

    /**
     * Save information about post type in the post meta
     *
     * @param $postId
     */
    public function hookActionSavePost($postId)
    {
        $postType = get_post_type($postId);

        \update_post_meta($postId,'_post_type', $postType);
    }

    /**
     * Register post type
     */
    public function hookActionInit()
    {
        $manager = $this;

        collect($this->postTypes)->each(function($postType) use ($manager) {

            if ($postType::$needsRegister) {
                register_post_type($postType::getPostType(), $postType::getArgs());
            }

            $manager->registeredPostTypes[$postType::getPostType()] = $postType;
        });
    }
}