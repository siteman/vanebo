<?php

namespace PP\Collections;

use PP\Models\Comment;
use Traversable;

class WPQueryCommentCollections extends AbstractWPCollections
{
    private $commentQuery = null;

    public function __construct(\WP_Comment_Query $commentQuery)
    {
        $this->commentQuery = $commentQuery;
    }

    public function pagination()
    {
        // TODO: Implement pagination() method.
    }

    public function getMaxNumPages()
    {
        return $this->commentQuery->max_num_pages;
    }

    public function getPage()
    {
        // TODO: Implement getPage() method.
    }

    public function havePosts()
    {
        return $this->commentQuery->found_comments !== 0;
    }

    public function hasMore()
    {
        return $this->commentQuery->query_vars['paged'] < $this->getMaxNumPages();
    }

    public function getPosts()
    {
        return array_map(function($comment) {
            return new Comment($comment);
        }, $this->commentQuery->get_comments());
    }

    /**
     * Retrieve an external iterator
     *
     * @link https://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->getPosts());
}}