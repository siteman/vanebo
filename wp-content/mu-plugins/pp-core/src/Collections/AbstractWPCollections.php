<?php

namespace PP\Collections;

abstract class AbstractWPCollections implements \IteratorAggregate
{
    abstract public function pagination();

    abstract public function getMaxNumPages();

    abstract public function getPage();

    abstract public function havePosts();

    abstract public function getPosts();
}