<?php

namespace PP\Collections;

use Illuminate\Support\Arr;
use PP\Foundation\Application;
use PP\Models\AbstractPostType;
use PP\Models\Posts\Product;
use PP\Pagination;

class WPQueryProductCollections extends WPQueryCollections
{
    /**
     * @var string
     */
    private $model = Product::class;

    /**
     * WPQueryCollections constructor.
     *
     * @param \WP_Query $query
     */
    public function __construct(?\WP_Query $query)
    {
        $args = $query->query;

        Arr::set($args, 'post_type', 'product');
        Arr::set($args, 'wc_query', 'product_query');

        $query->query($args);

        parent::__construct($query);
    }

    /**
     * Return array with posts
     *
     * @return AbstractPostType[]
     */
    public function getPosts() : array
    {
        if (empty($this->wpQuery->have_posts())) {
            return [];
        }

        return array_map(function($product) {
            $model = $this->model;
            return new $model($product->ID);
        }, $this->wpQuery->get_posts());
    }

    /**
     * @param string $model
     */
    public function setModel(string $model): void
    {
        $this->model = $model;
    }
}