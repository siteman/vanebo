<?php

namespace PP\Collections;

use Illuminate\Support\Arr;
use PP\Foundation\Application;
use PP\Models\AbstractPostType;
use PP\Pagination;

class WPQueryCollections extends AbstractWPCollections
{

    /**
     * @var \WP_Query
     */
    public $wpQuery;

    /**
     * @var Pagination
     */
    public Pagination $pagination;

    /**
     * WPQueryCollections constructor.
     *
     * @param \WP_Query $query
     */
    public function __construct(\WP_Query $query)
    {
        $this->wpQuery = $query;
        $this->pagination = new Pagination($this);
    }

    /**
     * Return array with posts
     *
     * @return PostType[]
     */
    public function getPosts() : array
    {
        $posts = array_map(function($post) {
            return Application::getInstance()->get('posts')->factory($post->ID, $post->post_type);
        }, $this->wpQuery->get_posts());

        return $posts;
    }

    /**
     * Return information about that query has any posts
     *
     * @return bool
     */
    public function havePosts() : bool
    {
        return $this->wpQuery->have_posts();
    }

    /**
     * Return current active page from collection
     *
     * @return mixed
     */
    public function getPage()
    {
        if (empty($this->wpQuery->get('paged'))) {
            return 1;
        }

        return $this->wpQuery->get('paged');
    }

    /**
     * Return max num pages for query
     *
     * @return int
     */
    public function getMaxNumPages()
    {
        return intval($this->wpQuery->max_num_pages);
    }

    public function getTotal()
    {
        return intval($this->wpQuery->found_posts);
    }

    /**
     * Create pagination object
     *
     * @return Pagination
     */
    public function pagination()
    {
        return $this->pagination;
    }

    public static function factoryByGlobal()
    {
        global $wp_query;
        return new self($wp_query);
    }

    /**
     * Retrieve an external iterator
     * @link https://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     */
    public function getIterator()
    {

        return new \ArrayIterator($this->getPosts());
    }
}