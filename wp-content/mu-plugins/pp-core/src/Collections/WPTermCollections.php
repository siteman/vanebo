<?php

namespace PP\Collections;

use PP\Facades\Taxonomy;
use PP\Models\AbstractTaxonomy;

class WPTermCollections implements \IteratorAggregate
{
    /**
     * @var array
     */
    public $terms;

    /**
     * WPTermCollections constructor.
     *
     * @param $terms array
     */
    public function __construct($terms)
    {

        $this->terms = $terms;
    }

    /**
     * Return array with posts
     *
     * @return Taxonomy[]
     */
    public function getTerms() : array
    {
        $terms = array_map(function($term) {
            return Taxonomy::factory($term->taxonomy, $term->term_id);
        }, $this->terms);

        return $terms;
    }

    public function haveTerms() : bool
    {

        return count($this->terms) > 0;
    }

    /**
     * Retrieve an external iterator
     * @link https://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     */
    public function getIterator()
    {

        return new \ArrayIterator($this->getTerms());
    }
}