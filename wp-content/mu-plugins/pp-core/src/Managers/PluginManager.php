<?php

namespace PP\Managers;

use Illuminate\Support\Arr;
use PP\Foundation\Application;
use PP\Foundation\Plugin;

class PluginManager
{
    /**
     * Plugins to register
     *
     * @var array
     */
    private $plugins = [];

    /**
     * List of registered plugins
     *
     * @var array
     */
    private $registeredPlugins = [];

    /**
     * Application
     *
     * @var \PP\Foundation\Application
     */
    private $app;

    /**
     * PluginManager constructor.
     */
    public function __construct(Application $app)
    {
        $this->app = $app;

        add_action('plugins_loaded', [$this, 'hookPluginLoaded']);
    }

    /**
     * Get a registered plugin
     *
     * @param string $plugin
     * @return mixed
     */
    public function get(string $plugin): ?Plugin
    {
        return Arr::get($this->registeredPlugins, $plugin);
    }

    /**
     * Register a plugin
     *
     * @param string $plugin
     */
    public function register(string $plugin, string $path): void
    {
        $this->plugins[] = [
            'class' => $plugin,
            'path' => $path
        ];
    }

    /**
     * Initialize plugins
     */
    public function hookPluginLoaded(): void
    {
        $manager = $this;

        collect($this->plugins)->each(function($args) use ($manager) {
            $class = Arr::get($args, 'class');

            $instance = new $class($manager->app, Arr::get($args, 'path'));

            $manager->registeredPlugins[$instance->getSlug()] = $instance;

            $instance->boot();
        });
    }
}