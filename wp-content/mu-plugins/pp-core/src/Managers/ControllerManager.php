<?php

namespace PP\Managers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use PP\Foundation\Application;

class ControllerManager
{
    protected Application $app;

    private array $controllers = [];

    protected array $pageRoutes = [];

    protected array $pageCallbacks = [];

    protected array $pageMiddlewares = [];

    protected array $ajaxRoutes = [];

    protected array $registeredControllers = [];

    public function __construct(Application $app)
    {
        $this->app = $app;

        add_action('template_redirect', [$this, 'hookTemplateRedirect']);
    }

    public function hookTemplateRedirect()
    {
        if (intval(get_query_var('is_pp_plugin')) !== 1) {
            return;
        }

        $callback = Arr::get($this->pageCallbacks, get_query_var('callback'));

        echo $callback();
        die();
    }

    private function registerPageCallback($callback): int
    {
        $callback = is_string($callback) ? $callback : function() use ($callback) {
            global $wp_query;

            $request = Request::capture();
            $request->request->add($wp_query->query);

            $instance = $callback[0];
            $controller = $this->app->make($instance);

            return $this->app->call([$controller, $callback[1]], [
                'request' => $request
            ]);
        };

        array_push($this->pageCallbacks, $callback);

        end($this->pageCallbacks);
        return key($this->pageCallbacks);
    }

    public function registerPage($name, $path, $callback)
    {
        $path = is_callable($path) ? call_user_func($path) : $path;

        $queryVars = [
            'is_pp_plugin' => 1,
            'pp_action' => $name,
            'callback' => $this->registerPageCallback($callback)
        ];
        $rewrite = $path;

        if (preg_match_all('/{([a-zA-Z_]+)}/', $path, $matches)) {
            foreach ($matches[0] as $index => $match) {
                $paramName =  $matches[1][$index];
                $rewrite = str_replace($match, '([^/]*)', $rewrite);

                Arr::set($queryVars, $paramName, '$matches['.($index + 1).']');
            }
        }

        add_filter('query_vars', function($vars) use ($queryVars) {
            foreach (array_keys($queryVars) as $queryVar) {
                if (!in_array($queryVar, $vars)) {
                    array_push($vars, $queryVar);
                }
            }

            return $vars;
        }, 10, 1);

        add_action('init', function() use ($rewrite, $queryVars) {
            foreach (array_keys($queryVars) as $queryVar) {
                add_rewrite_tag( '%'.$queryVar.'%', '([^&]+)' );
            }

            add_rewrite_rule('^' . rtrim($rewrite, '/') . '/?', 'index.php?' . urldecode(http_build_query($queryVars)), 'top' );
        }, 999, 0);

        Arr::set($this->pageRoutes, $name, ltrim($path, '/'));
    }

    public function registerAjax($action, $callback)
    {
        $callback = function() use ($action, $callback) {
            if ((defined('WP_DEBUG') && WP_DEBUG === true) ||
                (defined('WP_AJAX_DEBUG') && WP_AJAX_DEBUG === true)) {
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);
            }

            $instance = $this->app->make($callback[0]);

            /** @var Response $response */
            $response = $this->app->call([$instance, $callback[1]], [
                'request' => Request::capture()
            ]);
            $response->send();
            die();
        };

        add_action( "wp_ajax_nopriv_$action", $callback);
        add_action( "wp_ajax_$action", $callback);
    }

    public function url(string $name, $args = []): ?string
    {
        return home_url() . '/' . rtrim(Arr::get($this->pageRoutes, $name), '/') . '/';
    }

    public function ajaxUrl(string $name, $params = [])
    {
        $params = array_merge($params, [
            'action' => $name
        ]);

        return admin_url('admin-ajax.php') . '?' . http_build_query($params);
    }
}