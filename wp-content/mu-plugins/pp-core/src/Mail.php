<?php

namespace PP;

class Mail
{
    protected $body = '';

    protected $subject = '';

    protected $addresses = [];

    protected $headers = [];

    protected $attachments = [];

    protected $from = null;

    protected $replyTo = null;

    public function getHeaders()
    {
        $headers = $this->headers;
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        if ($this->from !== null) {
            $headers[] = 'From: ' . $this->from;
        }

        if ($this->replyTo !== null) {
            $headers[] = 'Reply-To: ' . $this->replyTo;
        }

        return $headers;
    }

    public function getHeadersString(): string
    {
        return implode("\r\n", $this->getHeaders());
    }

    /**
     * @param string $body
     * @return Mail
     */
    public function setBody(string $body): Mail
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @param string $subject
     * @return Mail
     */
    public function setSubject(string $subject): Mail
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @param string $addresses
     * @return Mail
     */
    public function setAddresses(array $addresses): Mail
    {
        $this->addresses = array_merge($this->addresses, $addresses);

        return $this;
    }

    public function setAddress(string $email, ?string $name = null): Mail
    {
        if ($name !== null) {
            $this->addresses[] = $email . '<'.$name.'>';
        } else {
            $this->addresses[] = $email;
        }

        return $this;
    }

    /**
     * @param array $headers
     * @return \PP\Mail
     */
    public function setHeaders(array $headers): Mail
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @param array $attachments
     * @return Mail
     */
    public function setAttachments(array $attachments): Mail
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Send the mail
     *
     * @return bool
     */
    public function send(): bool
    {
        return \wp_mail($this->addresses, $this->subject, $this->getBody(), $this->getHeadersString(), $this->attachments);
    }

    /**
     * @param string $from
     * @return Mail
     */
    public function setFrom(string $email, ?string $name = null): Mail
    {
        if ($name !== null) {
            $this->from = $name . ' <'.$email.'>';
        } else {
            $this->from = $email;
        }

        return $this;
    }

    /**
     * @param string $replyTo
     * @return Mail
     */
    public function setReplyTo(string $email, ?string $name = null): Mail
    {
        if ($name !== null) {
            $this->replyTo = $name . ' <'.$email.'>';
        } else {
            $this->replyTo = $email;
        }

        return $this;
    }
}