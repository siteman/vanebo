<?php

namespace PP;

class ShortcodeManager
{
    private $shortcodes = [];

    private $registeredShortcodes = [];

    public function __construct()
    {
        
        add_action('init', [$this, 'hookShortcode']);
    }

    public function register(string $shortcode): void
    {
        $this->shortcodes[] = $shortcode;
    }

    public function hookShortcode(): void
    {
        $manager = $this;

        collect($this->shortcodes)->each(function($shortcodeClass) use ($manager) {
            $instance = new $shortcodeClass;

            add_shortcode($instance->getName(), [$instance, 'render']);

            $manager->registeredShortcodes[] = $instance;
        });
    }
}