<?php

namespace PP\Assets;

abstract class AbstractAsset
{
    CONST VIEW_TYPE_FRONT = 'front';
    CONST VIEW_TYPE_ADMIN = 'admin';
    CONST VIEW_TYPE_GUTENBERG = 'gutenberg';

    /**
     * @var string
     */
    private $handle = null;

    /**
     * @var string
     */
    private $src = null;

    /**
     * @var array
     */
    private $deps = [];

    /**
     * @var string
     */
    private $ver = '';

    /**
     * @var string
     */
    private $viewType = self::VIEW_TYPE_FRONT;

    /**
     * AbstractAsset constructor.
     *
     * @param string|null $handle
     */
    public function __construct(string $handle = null)
    {
        $this->handle = $handle;
    }

    /**
     * @return string
     */
    public function getHandle(): string
    {
        return $this->handle;
    }

    /**
     * @param string $handle
     * @return Style
     */
    public function setHandle(string $handle): AbstractAsset
    {
        $this->handle = $handle;

        return $this;
    }

    /**
     * @return string
     */
    public function getSrc(): string
    {
        return $this->src;
    }

    /**
     * @param string $src
     * @return Style
     */
    public function setSrc(string $src): AbstractAsset
    {
        $this->src = $src;

        return $this;
    }

    /**
     * @return array
     */
    public function getDeps(): array
    {
        return $this->deps;
    }

    /**
     * @param array $deps
     * @return Style
     */
    public function setDeps(array $deps): AbstractAsset
    {
        $this->deps = $deps;

        return $this;
    }

    /**
     * @return string
     */
    public function getVer(): string
    {
        return $this->ver;
    }

    /**
     * @param string $ver
     * @return Style
     */
    public function setVer(string $ver): AbstractAsset
    {
        $this->ver = $ver;

        return $this;
    }

    /**
     * @return string
     */
    public function getViewType(): string
    {
        return $this->viewType;
    }

    /**
     * Set view type
     *
     * @param string $viewType
     */
    public function setViewType(string $viewType): AbstractAsset
    {
        if (!in_array($viewType, [self::VIEW_TYPE_FRONT, self::VIEW_TYPE_ADMIN, self::VIEW_TYPE_GUTENBERG])) {
            throw new \Exception('Invalid type');
        }

        $this->viewType = $viewType;

        return $this;
    }

    /**
     * Enqueue asset
     */
    abstract public function enqueue(): void ;
}