<?php

namespace PP\Assets;

class Script extends AbstractAsset
{
    /**
     * @var bool
     */
    private $inFooter = true;

    /**
     * @return bool
     */
    public function getInFooter(): bool
    {
        return $this->inFooter;
    }

    /**
     * @param bool $inFooter
     * @return Script
     */
    public function setInFooter(bool $inFooter): Script
    {
        $this->inFooter = $inFooter;

        return $this;
    }

    /**
     * Execute function wp_enqueue_script with properties
     */
    public function enqueue(): void
    {
        wp_enqueue_script($this->getHandle(), $this->getSrc(), $this->getDeps(), $this->getVer(), $this->getInFooter());
    }
}