<?php

namespace PP\Assets;

class Style extends AbstractAsset
{
    /**
     * @var string
     */
    private $media = 'all';

    /**
     * @return string
     */
    public function getMedia(): string
    {
        return $this->media;
    }

    /**
     * @param string $media
     * @return Style
     */
    public function setMedia(string $media): Style
    {
        $this->media = $media;

        return $this;
    }

    public function enqueue(): void
    {
        wp_enqueue_style($this->getHandle(), $this->getSrc(), $this->getDeps(), $this->getVer(), $this->getMedia());
    }
}