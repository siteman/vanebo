<?php

namespace PP;

use Illuminate\Support\Arr;

class Image
{
    private int $id;

    private bool $svgRenderSource = true;

    private array $attachment = [];

    private string $size = 'full';

    private array $meta = [];

    public function __construct(int $id, string $size = 'full')
    {
        $this->id = $id;
        $this->setSize($size);
        $this->meta = get_post_meta($this->id);
    }

    public function setSize(string $size): self
    {
        $this->size = $size;
        $attachment = wp_get_attachment_image_src($this->id, $this->size);

        if (is_array($attachment)) {
            $this->attachment = $attachment;
        }

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getWidth(): int
    {
        return Arr::get($this->attachment, 1, 1);
    }

    public function getHeight(): int
    {
        return Arr::get($this->attachment, 2, 1);
    }

    public function getUrl(): string
    {
        return Arr::get($this->attachment, 0, '');
    }

    public function render(): string
    {
        if ($this->svgRenderSource === true && get_post_mime_type($this->id) === 'image/svg+xml' && file_exists($this->getPath())) {
            return file_get_contents($this->getPath());
        }

        return wp_get_attachment_image($this->id, $this->size);
    }

    public function display(): void
    {
        echo $this->render();
    }

    public function getPath(): string
    {
        $file = get_attached_file($this->id, true);

        if (empty($this->size) || $this->size === 'full') {
            return realpath($file);
        }

        if (! wp_attachment_is_image($this->id) ) {
            return false;
        }

        $info = image_get_intermediate_size($this->id, $this->size);

        if (!is_array($info) || ! isset($info['file'])) {
            return false;
        }

        return realpath(str_replace(wp_basename($file), $info['file'], $file));
    }

    public function __toString(): string
    {
        return $this->render();
    }

    public function getAlt(): string
    {
        return Arr::first(Arr::get($this->meta, '_wp_attachment_image_alt', [])) ?? '';
    }
}