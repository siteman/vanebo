<?php

namespace PP\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Options
 *
 * @package PP\Facades
 *
 * @see \PP\Managers\ControllerManager;
 */
class Router extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'controllers';
    }
}