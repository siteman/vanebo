<?php

namespace PP\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Theme
 *
 * @package PP\Facades
 *
 * @see \PP\TaxonomyManager
 */
class Taxonomy extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'taxonomies';
    }
}