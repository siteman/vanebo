<?php

namespace PP\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Options
 *
 * @package PP\Facades
 *
 * @see \PP\Models\Options
 */
class Options extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'options';
    }
}