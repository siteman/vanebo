<?php

namespace PP\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Theme
 *
 * @package PP\Facades
 *
 * @see \PP\Foundation\Theme
 */
class Theme extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'theme';
    }
}