<?php

namespace PP\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Theme
 *
 * @package PP\Facades
 *
 * @see \PP\PostTypeManager
 */
class Posts extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'posts';
    }
}