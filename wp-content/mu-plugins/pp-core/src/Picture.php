<?php

namespace PP;

use Illuminate\Support\Arr;
use PP\Facades\Theme;

class Picture
{
    const DEFAULT_MEDIA = 'default';

    /**
     * @var \PP\Image[]
     */
    private $images = [];

    /**
     * @var null
     */
    private $alt = null;

    /**
     * @var bool
     */
    private $lazyload = true;

    /**
     * @var array string[]
     */
    private $sizes = [];

    /**
     * @var array string[]
     */
    private array $srcsets = [];

    /**
     * @var array string[]
     */
    private array $imagesSize = [];

    /**
     * @var array
     */
    private array $classes = [];

    /**
     * Picture constructor.
     *
     * @param array $images
     */
    public function __construct(array $images = [], array $sizes = [])
    {
        $this->setSizes($sizes);
        $this->setImages(array_filter($images));
        $this->srcsets = $this->getDefaultSrcSet();
    }

    /**
     * Set tag html classes
     *
     * @param $classes
     * @return \PP\Picture
     */
    public function setClasses($classes): self
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * Set srcsets for picture
     *
     * @param array $sizes
     * @return \PP\Picture
     */
    public function setSrcSets(array $srcsets): self
    {
        $this->srcsets = $srcsets;

        return $this;
    }

    /**
     * Set sizes for thumbnails
     *
     * @param array $sizes
     * @return \PP\Picture
     */
    public function setSizes(array $sizes): self
    {
        $this->sizes = $sizes;

        return $this;
    }

    /**
     * Set wordpress image thumbs sizes
     *
     * @param array $sizes
     * @return \PP\Picture
     */
    public function setImagesSize(array $sizes): self
    {
        $this->imagesSize = $sizes;

        foreach ($this->images as $media => $image) {

            if (Arr::has($this->imagesSize, $media)) {
                $image->setSize(Arr::get($this->imagesSize, $media));
            }
        }

        return $this;
    }

    /**
     * Return list of sizes from config
     *
     * @return array
     */
    public function getDefaultSrcSet(): array
    {
        return Theme::get('config')->get('thumbnails.srcset');
    }

    /**
     * Set alt for the picture
     *
     * @param string $alt
     * @return \PP\Picture
     */
    public function setAlt(string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Set images by array
     *
     * @param array $images
     * @return \PP\Picture
     */
    public function setImages(array $images): self
    {
        foreach ($images as $media => $image) {
            if ($image instanceof Image) {
                $this->setImage($image, $media);
            } else {
                $this->setImageId($image, $media);
            }
        }

        return $this;
    }

    /**
     * Get image by media
     *
     * @param $media
     * @return \PP\Image
     */
    public function getImage(string $media): Image
    {
        return Arr::get($this->images, $media, null);
    }

    /**
     * Set source by id
     *
     * @param int $id
     * @param string $media
     * @return \PP\Picture
     */
    public function setImageId(int $id, $media = self::DEFAULT_MEDIA): self
    {
        $this->setImage(new Image($id), $media);

        return $this;
    }

    /**
     * Set source by Image object
     *
     * @param \PP\Image $image
     * @param string $media
     * @return \PP\Picture
     */
    public function setImage(Image $image, $media = self::DEFAULT_MEDIA): self
    {
        Arr::set($this->images, $media, $image);

        return $this;
    }

    /**
     * Render picture
     *
     * @return string
     */
    public function render(): string
    {
        $classes = $this->classes;

        $html = '<picture class="'.implode(' ', $classes).'">';

        foreach ($this->images as $media => $image) {
            $srcset = Arr::get($this->srcsets, $media, self::DEFAULT_MEDIA);
            $size = Arr::get($this->sizes, $media, null);

            if ($size !== null) {
                $image->setSize($size);
            }

            if ($this->lazyload === true) {
                if ($srcset === self::DEFAULT_MEDIA) {
                    $html .= sprintf('<img data-srcset="%s" alt="%s" class="lazy">', $image->getUrl(), $this->alt ?? $image->getAlt());
                } else {
                    $html .= sprintf('<source data-srcset="%s" media="%s">', $image->getUrl(), $srcset);
                }
            } else {
                if ($srcset === self::DEFAULT_MEDIA) {
                    $html .= sprintf('<img src="%s" alt="%s">', $image->getUrl(), $this->alt ?? $image->getAlt());
                } else {
                    $html .= sprintf('<source srcset="%s %sw" media="%s">', $image->getWidth(), $image->getUrl(), $srcset);
                }
            }
        }

        $html .= '</picture>';

        return $html;
    }

    /**
     * Display a rendered picture
     */
    public function display()
    {
        echo $this->render();
    }

    /**
     * @param bool $lazyload
     * @return Picture
     */
    public function setLazyload(bool $lazyload): Picture
    {
        $this->lazyload = $lazyload;

        return $this;
    }

    public function __toString(): string
    {
        return $this->render();
    }
}