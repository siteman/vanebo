<?php

namespace PP;

use PP\Foundation\Application;

class Template
{

    /**
     * Path to file template
     *
     * @var string
     */
    private $template;

    /**
     * List of params for template, those params will be available as variables in template
     *
     * @var string
     */
    private $params = [];

    /**
     * Js languages will be injected to global pp object
     *
     * @var string
     */
    public static $jsLanguages = [];

    /**
     * Js data will be injected to global pp object
     *
     * @var array
     */
    public static $jsVariableData = [];

    /**
     * Path to theme template
     *
     * @param string $template
     * @return \PP\Template
     */
    public function setThemePath(string $template): self
    {
        $this->template = Application::getInstance()->get('theme')
            ->resourcesPath($template);

        return $this;
    }

    /**
     * Set plugin path
     *
     * @param string $plugin
     * @param string $template
     * @return \PP\Template
     */
    public function setPluginPath(string $plugin, string $template): self
    {
        $this->template = Application::getInstance()->get('plugins')
            ->get($plugin)
            ->basePath($template);

        return $this;
    }

    /**
     * Set the core plugin path
     *
     * @param string $template
     * @return \PP\Template
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function setPath(string $template): self
    {
        $this->template = Application::getInstance()->basePath($template);
    }

    /**
     * @param $key
     * @param $value
     *
     * @return $this
     */
    public function setParam($key, $value)
    {

        $this->params[$key] = $value;

        return $this;
    }

    /**
     * Setter params
     *
     * @param array $params
     *
     * @return $this
     */
    public function setParams(array $params)
    {

        $this->params = array_merge($this->params, $params);

        return $this;
    }

    /**
     * Adding javascript params
     *
     * @param array $params
     */
    public function setJsLanguages(array $params)
    {

        Application::getInstance()->get('assets')->addJsTranslation($params);

        return $this;
    }

    /**
     * Adding javascript params
     *
     * @param array $params
     */
    public function setJsVariables(array $params)
    {

        Application::getInstance()->get('assets')->addJsData($params);

        return $this;
    }

    /**
     * Render template
     *
     * @return string
     */
    public function render()
    {

        if (!empty($this->params) && is_array($this->params)) {
            extract($this->params);
        }

        ob_start();
        require $this->template;
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    /**
     * Print template
     */
    public function display()
    {

        echo $this->render();
    }

    public function __toString()
    {
        return $this->render();
    }
}