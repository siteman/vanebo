<?php

namespace PP;

use Illuminate\Support\Arr;
use PP\Foundation\Application;

class BlocksManager
{
    private $blocks = [];

    private $registeredBlocks = [];

    public function __construct()
    {

        add_action('acf/init', [$this, 'hookInit']);
    }

    /**
     * Register block
     *
     * @param string $block
     */
    public function register(string $block): void
    {

        $this->blocks[] = $block;
    }

    public function hookInit(): void
    {

        if (!function_exists('acf_register_block_type')) {
            return;
        }

        collect($this->blocks)->each(function($block) {
            $instance = new $block;

            $args = $instance->getArgs();

            $args['title'] = sprintf(__('Theme - %s'), Arr::get($args, 'title'));
            $args['name'] = $instance->getSlug();
            $args['render_callback'] = [$instance, 'render'];

            acf_register_block_type($args);

            $this->registeredBlocks[] = $instance;
        });
    }
}