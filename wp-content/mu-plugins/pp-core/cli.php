#!/usr/bin/env php
<?php

$_SERVER['HTTP_HOST'] = '';
$_SERVER['REQUEST_URI'] = '';

ob_start();
require __DIR__.'/../../../wp-load.php';
ob_clean();

$app = \PP\Foundation\Application::getInstance();

$app->register(
    \PP\Providers\ComposerServiceProvider::class
);
$app->register(
    \PP\Providers\DatabaseServiceProvider::class
);
$app->register(
    \PP\Providers\MigrationServiceProvider::class
);

$artisan = new \Illuminate\Console\Application($app, $app['events'], '1.0.0');

$artisan->run(
    $input = new Symfony\Component\Console\Input\ArgvInput,
    new Symfony\Component\Console\Output\ConsoleOutput
);