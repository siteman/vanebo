<?php

namespace Deployer;

require 'recipe/wordpress.php';
require 'recipe/cachetool.php';

// Prepare constants
define('_SSH_HOST_', getenv('SSH_HOST'));
define('_SSH_USERNAME_', getenv('SSH_USERNAME'));
define('_SSH_PASSWORD_', getenv('SSH_PASSWORD'));
define('_SSH_PORT_', (int) getenv('SSH_PORT') ?: 22);
define('_ENVIRONMENT_', getenv('BITBUCKET_DEPLOYMENT_ENVIRONMENT'));
define('_PROJECT_PATH_', getenv('PROJECT_PATH'));
define('_LOCAL_PROJECT_PATH_', getenv('BITBUCKET_CLONE_DIR'));
define('_THEME_SLUG_', getenv('THEME_SLUG'));

set('default_stage', _ENVIRONMENT_);

set('bin/wp', function() {
    if (_ENVIRONMENT_ === 'production') {
        return '/usr/bin/wp';
    } else {
        return '/usr/local/bin/wp';
    }
});

// Set socket for cachetool
set('cachetool', '/run/php/php7.4-fpm.sock');

// Use ssh multiplexing to speedup the native ssh client.
set('ssh_type', 'native');

// List of shared files.
set('shared_files', [
    '.htaccess',
    'wp-config.php',
    'wp-content/db.php',
    'wp-content/object-cache.php',
    'wp-content/wp-cache-config.php',
    'wp-config/environments/env.php'
]);

set('shared_dirs', [
    'wp-content/languages',
    'wp-content/uploads',
    'wp-content/w3tc-config',
]);

// List of dirs which must be writable for web server.
set('writable_dirs', [
    'wp-content/cache',
    'wp-content/uploads',
    'wp-content/languages',
    'wp-content/languages/loco',
    'wp-content/languages/loco/plugins',
    'wp-content/languages/loco/themes',
]);

// List of paths which need to be deleted in release after updating code.
set('clear_paths', [
    'wp-content/cache',
]);

// Defining a host in Deployer.
host(_SSH_HOST_)
    ->user(_SSH_USERNAME_)
    ->port(_SSH_PORT_)
    ->forwardAgent()
    ->set('deploy_path', _PROJECT_PATH_.'/'._ENVIRONMENT_)
    ->stage(_ENVIRONMENT_)
    ->set('http_user', 'www-data')
    ->multiplexing(true);

/** Tasks */
desc('Execute startup commands');
task('deploy:prepare_code_locally', function () {
    if (_ENVIRONMENT_ === 'production') {
        runLocally("npm --prefix wp-content/themes/gs-starter-theme run prod > /dev/null");
    } else {
        runLocally("npm --prefix wp-content/themes/gs-starter-theme run dev");
        //runLocally("ln -s wp-content/themes/gs-starter-theme/node_modules node_modules");
    }

    // Pack entire the prepared project to the tar file
    runLocally("tar cvzf /project.tar.gz -X '.deploy-ignore' .");
})
    ->once();

desc('Creating symlink to release');
task('deploy:symlink', function () {
    // Create symlink
    run("cd {{deploy_path}} && {{bin/symlink}} {{release_path}} current");
    // Remove release link.
    run("cd {{deploy_path}} && rm release");
});

desc('Update code');
task('deploy:update_code', function () {
    // Upload the packed project on server
    upload('/project.tar.gz', '{{release_path}}');

    // Unpack the project on the server to release's path
    run('tar -xvzf {{release_path}}/project.tar.gz -C {{release_path}}');

    // Remove the packed file of project
    run('rm {{release_path}}/project.tar.gz');
});

desc('Rewrite flush, clean w3 total cache');
task('deploy:wp', function () {
    // Refresh all rewrites
    run('{{bin/wp}} rewrite flush --path={{release_path}}');

    // Clean W3 total cache (attention: plugin w3-total-cache have to activate)
    if (_ENVIRONMENT_ === 'production') {
        run('{{bin/wp}} w3-total-cache flush all --path={{release_path}}');
    }

})
    ->once();

task('deploy:completed', function () {
    // Add file to .dep directory, the cron on the server will setup new permissions
    run('touch {{deploy_path}}/.dep/.dep-completed');
})->once();

task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:prepare_code_locally',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:symlink',
    'deploy:wp',
    'cachetool:clear:opcache',
    'deploy:completed',
    'deploy:unlock',
    'cleanup'
]);

// If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');